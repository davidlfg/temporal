<?php

namespace Drupal\content_sync\Normalizer;

use Drupal\content_sync\Plugin\SyncNormalizerDecoratorManager;
use Drupal\content_sync\Plugin\SyncNormalizerDecoratorTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\serialization\Normalizer\ContentEntityNormalizer as BaseContentEntityNormalizer;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

/**
 * Adds the file URI to embedded file entities.
 */
class ContentEntityNormalizer extends BaseContentEntityNormalizer {

  use SyncNormalizerDecoratorTrait;

  /**
   * @var SyncNormalizerDecoratorManager
   */
  protected $decoratorManager;

  /**
   * Constructs an EntityNormalizer object.
   *
   * @param EntityTypeManagerInterface $entity_manager
   *
   * @param SyncNormalizerDecoratorManager $decorator_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, SyncNormalizerDecoratorManager $decorator_manager) {
    parent::__construct($entity_manager);
    $this->decoratorManager = $decorator_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = []) {
    // Get the entity type ID while letting context override the $class param.
    $entity_type_id = !empty($context['entity_type']) ? $context['entity_type'] : $this->entityManager->getEntityTypeFromClass($class);

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type_definition */
    // Get the entity type definition.
    $entity_type_definition = $this->entityManager->getDefinition($entity_type_id, FALSE);

    // Don't try to create an entity without an entity type id.
    if (!$entity_type_definition) {
      throw new UnexpectedValueException(sprintf('The specified entity type "%s" does not exist. A valid entity type is required for denormalization', $entity_type_id));
    }

    // Decorate data before denormalizing it.
    $this->decorateDenormalization($data, $entity_type_id, $format, $context);

    // The bundle property will be required to denormalize a bundleable entity.
    if ($entity_type_definition->hasKey('bundle')) {
      $bundle_key = $entity_type_definition->getKey('bundle');
      // Get the base field definitions for this entity type.
      $base_field_definitions = $this->entityManager->getBaseFieldDefinitions($entity_type_id);

      // Get the ID key from the base field definition for the bundle key or
      // default to 'value'.
      $key_id = isset($base_field_definitions[$bundle_key]) ? $base_field_definitions[$bundle_key]->getFieldStorageDefinition()->getMainPropertyName() : 'value';

      // Normalize the bundle if it is not explicitly set.
      $data[$bundle_key] = isset($data[$bundle_key][0][$key_id]) ? $data[$bundle_key][0][$key_id] : (isset($data[$bundle_key]) ? $data[$bundle_key] : NULL);

      // Get the bundle entity type from the entity type definition.
      $bundle_type_id = $entity_type_definition->getBundleEntityType();
      $bundle_types = $bundle_type_id ? $this->entityManager->getStorage($bundle_type_id)->getQuery()->execute() : [];

      // Make sure a bundle has been provided.
      if (!is_string($data[$bundle_key])) {
        throw new UnexpectedValueException('A string must be provided as a bundle value.');
      }

      // Make sure the submitted bundle is a valid bundle for the entity type.
      if ($bundle_types && !in_array($data[$bundle_key], $bundle_types)) {
        throw new UnexpectedValueException(sprintf('"%s" is not a valid bundle type for denormalization.', $data[$bundle_key]));
      }

      // Resolve uuids.
      if (!empty($context['resolve_uuid']) || !empty($data['_resolve_uuid'])) {
        $field_definitions = $this->entityManager->getFieldDefinitions($entity_type_id, $data[$bundle_key]);
        foreach ($field_definitions as $field_name => $field_definition) {
          // We are only interested in importing content entities.
          if (!is_a($field_definition->getClass(), '\Drupal\Core\Field\EntityReferenceFieldItemList', TRUE)) {
            continue;
          }
          if (is_array($data[$field_name]) && !empty($data[$field_name])) {
            $key = $field_definition->getFieldStorageDefinition()
              ->getMainPropertyName();
            foreach ($data[$field_name] as &$item) {
              if (!empty($item[$key]) && !empty($item['target_uuid'])) {
                $reference = $this->entityManager->loadEntityByUuid($item['target_type'], $item['target_uuid']);
                if ($reference) {
                  $item[$key] = $reference->id();
                }
              }
            }
          }
        }
      }
    }

    // Get entity alias if exists.
    if (!empty($data['_alias'])) {
      $alias = $data['_alias'];
      // Remove the alias from the data.
      unset($data['_alias']);
    }
    // Get entity alias if exists.
    if (!empty($data['_resolve_uuid'])) {
      // Remove the alias from the data.
      unset($data['_resolve_uuid']);
    }
    // Create the entity from data.
    $entity = $this->entityManager->getStorage($entity_type_id)->create($data);

    // Pass the names of the fields whose values can be merged.
    // @todo https://www.drupal.org/node/2456257 remove this.
    $entity->_restSubmittedFields = array_keys($data);

    if (isset($alias)) {
      $entity->_alias = $alias;
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = array()) {
    $normalized_data = parent::normalize($object, $format, $context);
    try {
      /** @var \Drupal\Core\Url $url */
      $url = $object->toUrl();
      if (!empty($url)) {
        $system_path = '/' . $url->getInternalPath();
        $langcode    = $object->language()->getId();
        $path_alias = \Drupal::service('path.alias_manager')
          ->getAliasByPath($system_path, $langcode);
        if (!empty($path_alias) && $path_alias != $system_path) {
          $normalized_data['_alias'] = $path_alias;
          $normalized_data['path'] = $path_alias;
        }
      }
    }
    catch (UndefinedLinkTemplateException $e) {
      \Drupal::logger('content_sync')->warning($e->getMessage());
    }
    $normalized_data['_resolve_uuid'] = TRUE;

    // Decorate normalized entity before retuning it.
    if (is_a($object, '\Drupal\Core\Entity\ContentEntityInterface', true)) {
      $this->decorateNormalization($normalized_data, $object, $format, $context);
    }
    return $normalized_data;
  }

  /**
   * @inheritdoc
   */
  protected function getDecoratorManager() {
    return $this->decoratorManager;
  }

}
