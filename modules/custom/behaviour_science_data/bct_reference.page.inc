<?php

/**
 * @file
 * Contains bct_reference.page.inc.
 *
 * Page callback for BCT Reference entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for BCT Reference templates.
 *
 * Default template: bct_reference.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_bct_reference(array &$variables) {
  // Fetch BCTReference Entity Object.
  $bct_reference = $variables['elements']['#bct_reference'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
