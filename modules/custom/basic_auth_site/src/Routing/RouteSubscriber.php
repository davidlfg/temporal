<?php

namespace Drupal\basic_auth_site\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\basic_auth_site\Routing
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection as $route) {
      $auth = $route->getOption('_auth') ? $route->getOption('_auth') : [];
      $auth = ['basic_auth'];
      $route->setOption('_auth', $auth);
    }
  }
}
