<?php

namespace Drupal\remote_content_sync\Filter;

/**
 * Remote Filter Manager class.
 */
class RemoteFilterManager implements RemoteFilterManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function createInstance($id, array $configuration) {
    $operator = $configuration['operator'];
    $filter = NULL;
    switch ($id) {
      case 'node_type':
        $configuration = [
          'types' => $configuration['data'],
        ];
        $filter = new NodeTypeRemoteFilter($configuration, $operator);
        break;

      case 'taxonomy_term':
        $configuration = [
          'terms' => $configuration['data'],
        ];
        $filter = new TaxonomyTermRemoteFilter($configuration, $operator);
        break;

      case 'entity_type':
        $configuration = [
          'types' => $configuration['data'],
        ];
        $filter = new EntityTypeRemoteFilter($configuration, $operator);
        break;

      default:
        break;
    }
    return $filter;
  }

}
