<?php

namespace Drupal\rest_content_sync;

use Drupal\rest\RequestHandler as RequestHandlerBase;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\rest\ResourceResponseInterface;
use Drupal\rest\RestResourceConfigInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Acts as intermediate request forwarder for resource plugins.
 */
class RequestHandler extends RequestHandlerBase {

  /**
   * {@inheritdoc}
   */
  protected function renderResponse(Request $request, ResourceResponseInterface $response, SerializerInterface $serializer, $format, RestResourceConfigInterface $resource_config) {
    $data = $response->getResponseData();

    if ($response instanceof CacheableResponseInterface) {
      // Add rest config's cache tags.
      $response->addCacheableDependency($resource_config);
    }

    // If there is data to send, serialize and set it as the response body.
    if ($data !== NULL) {
      $serializer_context = [
        'request_method' => strtolower($request->getMethod()),
      ];
      if ($response instanceof CacheableResponseInterface) {
        $context = new RenderContext();
        $output = $this->container->get('renderer')
          ->executeInRenderContext($context, function () use ($serializer, $data, $format, $serializer_context) {
            return $serializer->serialize($data, $format, $serializer_context);
          });

        if (!$context->isEmpty()) {
          $response->addCacheableDependency($context->pop());
        }
      }
      else {
        $output = $serializer->serialize($data, $format, $serializer_context);
      }

      $response->setContent($output);
      $response->headers->set('Content-Type', $request->getMimeType($format));
    }

    return $response;
  }

}
