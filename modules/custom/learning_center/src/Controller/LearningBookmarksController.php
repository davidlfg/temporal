<?php

namespace Drupal\learning_center\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Manager for bookmarking learning assets.
 */
class LearningBookmarksController {

  /**
   * IsBookmark.
   *
   * @Return int bookmark ID in case it exists or FALSE otherwise
   */
  public function isBookmark(Request $request) {
    
    $headers = getallheaders();

    if (!empty($headers['nid'])&&!empty($headers['UUID'])) {

      $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);
      // Service isBookmark.
      $bookmark = \Drupal::service('learning_center.bookmarks')->isBookmark($uid, $headers['nid']);

      return new JsonResponse($bookmark, 200);

    }
    else {
      $response = array(
        'errorMessage' => t('Error: there are missing parameters'),
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * Get bookmark list for a certain UID.
   *
   * @Return array of bookmark data or FALSE in case no bookmarks are found
   */
  public function getBookmarks(Request $request) {
    $headers = getallheaders();

    if (!empty($headers['UUID'])) {

      $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);
      // Service getBookmarks.
      $bookmarks = \Drupal::service('learning_center.bookmarks')->getBookmarks($uid);

      if (!empty($bookmarks)) {
        return new JsonResponse($bookmarks, 200);
      }
      else {
        $response = array();
        return new JsonResponse($response, 400);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error: there are missing parameters'),
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * Creates a bookmark from a set of user and node identifiers.
   *
   * @Return Boolean
   */
  public function addBookmark(Request $request) {

    $headers = getallheaders();

    if (!empty($headers['nid'])&&!empty($headers['UUID'])) {

      $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);

      if ($uid !== FALSE) {
        // Service addBookmark.
        $bookmark = \Drupal::service('learning_center.bookmarks')->addBookmark($headers['UUID'], $uid, $headers['nid']);
        if ($bookmark !== FALSE) {
          return new JsonResponse($bookmark, 200);
        }
        else {
          $response = array(
            'errorMessage' => t('Error: could not save the bookmark.'),
          );
          return new JsonResponse($response, 400);
        }
      }
      else {
        $response = array(
          'errorMessage' => t('Error: The user does not exist.'),
        );
        return new JsonResponse($response, 400);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error: there are missing parameters.'),
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * Removes a bookmark from a pair of user and node identifiers.
   *
   * @Return the number of bookmarks affected or FALSE
   */
  public function deleteBookmark(Request $request) {

    $headers = getallheaders();

    if (!empty($headers['nid'])&&!empty($headers['UUID'])) {

      $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);
      // Service deleteBookmark.
      $bookmark = \Drupal::service('learning_center.bookmarks')->deleteBookmark($uid, $headers['nid']);
      if ($bookmark !== FALSE) {
        return new JsonResponse($bookmark, 200);
      }
      else {
        $response = array(
          'errorMessage' => t('Error: could not delete the bookmark'),
        );
        return new JsonResponse($response, 400);
      }

    }
    else {

      $response = array(
        'errorMessage' => t('Error: there are missing parameters'),
      );
      return new JsonResponse($response, 400);
    }
  }

}
