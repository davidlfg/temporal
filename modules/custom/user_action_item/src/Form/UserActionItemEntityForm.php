<?php

namespace Drupal\user_action_item\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for User action item entity edit forms.
 *
 * @ingroup user_action_item
 */
class UserActionItemEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\user_action_item\Entity\UserActionItemEntity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label User action item entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label User action item entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.user_action_item_entity.canonical', ['user_action_item_entity' => $entity->id()]);
  }

}
