<?php

/**
 * @file
 * Include file for drush commands.
 */

use Drupal\Core\Site\Settings;

/**
 * Implements hook_drush_command().
 */
function content_sync_drush_command() {
  $items['content-sync-export'] = [
    'description' => dt('Exports a single entity'),
    'arguments' => [
      'entity_type' => dt('The entity type to export.'),
      'entity_id' => dt('The ID(s) of the entity(ies) to export. Use comma to separate each ID.'),
    ],
    'options' => [
      'directory' => dt('Folder to export to.'),
      'format' => dt('The desired format do export.'),
    ],
    'aliases' => ['cse'],
    'required-arguments' => 2,
  ];
  $items['content-sync-import'] = [
    'description' => dt('Import content to the current application'),
    'arguments' => [],
    'options' => [
      'directory' => dt('An alternative directory from where to read the contents.'),
      'format' => dt('The format that will be used to import the content.'),
      'no-update' => dt('Only imports the new entities, and skips the existing entities.'),
    ],
    'aliases' => ['csi'],
    'required-arguments' => 0,
  ];

  return $items;
}

/**
 * Exports a piece of content and all its referenced entities.
 *
 * @param string $entity_type
 *   The entity type ID.
 * @param mixed $entity_id
 *   The entity ID to export.
 */
function drush_content_sync_export($entity_type, $entity_id) {
  $directory = drush_get_option('directory', '');
  if (empty($directory)) {
    $directory = Settings::get('content_sync_directory', '');
  }

  $format = drush_get_option('format', '');
  if (empty($format)) {
    $format = Settings::get('content_sync_format', '');
  }

  drush_print('Exporting entities ' . $entity_type . ':' . $entity_id . ' to ' . $directory);

  $manager = \Drupal::service('content_sync.manager');
  if ($format) {
    $manager->setFormat($format);
  }
  $context = [
    'content_sync_directory' => $directory,
    'field_map' => [
      [
        'local_name' => 'field_bariatrics_weight',
        'remote_name' => 'field_weight',
      ],
    ],
  ];
  if (strpos($entity_id, 'bundle:') === 0) {
    $bundle = str_replace('bundle:', '', $entity_id);
    $entities_by_type = $manager->exportContentByBundle($entity_type, $bundle, $context);
  }
  else {
    if ($entity_id === 'all') {
      $entity_id = NULL;
    }
    else {
      $entity_id = explode(',', $entity_id);
    }
    $entities_by_type = $manager->exportContent($entity_type, $entity_id, $context);
  }
  $extension = _content_sync_extension($format);

  foreach ($entities_by_type as $uuid => $entity) {
    $entity_type_folder = "{$directory}/{$entity['entity_type_id']}";
    file_prepare_directory($entity_type_folder, FILE_CREATE_DIRECTORY);
    file_put_contents("{$entity_type_folder}/{$uuid}.{$extension}", $entity['entity']);
    drush_print("Created file: {$entity_type_folder}/{$uuid}.{$extension}");
    // @todo it should be discussed whether this is a good fix for the newline
    // issue.
    // foreach ($entities as $id => $entity) {
    // $entity = str_replace('\r\n', '\n', $entity);
    // file_put_contents("{$entity_type_folder}/{$id}.{$extension}", $entity);
    // drush_print("Created file: {$entity_type_folder}/{$id}.{$extension}");
    // }
  }

  drush_print('Entities exported!');
}

/**
 * Import the content placed in the import directory.
 */
function drush_content_sync_import() {
  $directory = drush_get_option('directory', '');
  if (empty($directory)) {
    $directory = Settings::get('content_sync_directory', '');
  }

  $format = drush_get_option('format', '');
  if (empty($format)) {
    $format = Settings::get('content_sync_format', '');
  }

  drush_print('Importing from ' . $directory);

  $manager = \Drupal::service('content_sync.manager');
  if (!empty($format)) {
    $manager->setFormat($format);
  }
  if (drush_get_option('no-update', FALSE)) {
    $manager->setUpdateEntities(FALSE);
  }
  $manager->importContent($directory);

  drush_print('Entities imported!');
}
