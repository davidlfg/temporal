<?php
/**
 * checkinquestionsController file to create check-in question report
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\field_collection\Entity\FieldCollectionItem;

class checkinquestionsController extends ControllerBase {
  public function checkInQuestions() {
    global $base_path;
    $view = views_embed_view('check_in_question_report', 'page_1');
    $view_result = \Drupal::service('renderer')->render($view);
    return array(
      '#type' => 'markup',
      '#markup' => '<div class="download-report"> <b>Download:</b> <a href="/check-in-questions-report"><img src="' . $base_path . drupal_get_path('module', 'reports') . '/src/icon/Excel-xls.ico" > </a></div><div>' . $view_result . '</div>',
    );
  }


  public function downloadCheckInQuestions() {
    $view = views_embed_view('check_in_question_report', 'rest_export_1');
    $view_result = \Drupal::service('renderer')->render($view);
    $output_json = (string) $view_result;
    $data = json_decode($output_json, TRUE);
    $item = array();
    foreach ($data as $id => $data) {
      $item['CMS Id'] = $data['nid'];
      $item['CMS Title'] = $data['title'];
      $question = str_replace(array(
        "<br />",
        "\t",
        "\r",
        chr(194) . chr(160)
      ), "                   ", $data['field_text_content_new']);
      $question = str_replace(array(chr(194) . chr(160)), " ", $question);
      $item['CheckIn Question'] = $question;
      $item['CMS Last Update Time'] = $data['changed'];
      $action = $data['field_prompt_action'];
      $option = self::getActionType($action);
      $items[$id] = $item + $option;
    }
//print_r($items);exit;

    // filename for download
    $filename = "checkin_questions_" . date('Ymd') . ".xls";
    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");

    $flag = FALSE;
    foreach ($items as $row) {
      if (!$flag) {
        // display field/column names as first row
        echo implode("\t", array_keys($row)) . "\r\n";
        $flag = TRUE;
      }
      array_walk($row, array('self', 'cleanData'));
      echo implode("\t", array_values($row)) . "\r\n";
    }

    exit;
  }

  /*
   *
   */
  public function cleanData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", " ", $str);
    if (strstr($str, '"')) {
      $str = '"' . str_replace('"', '""', $str) . '"';
    }
  }

  /*
   *
   */
  public function getActionType($nid) {
    $view = views_embed_view('action', 'rest_export_1', $nid);
    $view_result2 = \Drupal::service('renderer')->render($view);
    $json = (string) $view_result2;
    $chain_array2 = json_decode($json);

    $buttons_array = isset($chain_array2['0']->field_buttons) ? $chain_array2['0']->field_buttons : '0';
    $checkboxes_array = isset($chain_array2['0']->field_checkboxes) ? $chain_array2['0']->field_checkboxes : '0';
    $free_form_text_array = isset($chain_array2['0']->field_free_form_text) ? $chain_array2['0']->field_free_form_text : '0';
    $slider_array = isset($chain_array2['0']->field_slider) ? $chain_array2['0']->field_slider : '0';
    $toggle_array = isset($chain_array2['0']->field_toggle) ? $chain_array2['0']->field_toggle : '0';
    $options = NULL;
    //Button options
    $items = array();
    if (count($buttons_array) > 0) {
      $items['Question Type'] = 'button';
      foreach ($buttons_array as $key => $value) {
        $field_collection_id = $value->value;
        $fieldcollectionitem = FieldCollectionItem::load($field_collection_id);
        $label = $fieldcollectionitem->field_button_label->value;
        if ($options) {
          $options = isset($label) ? $options . ',' . $label : NULL;
        }
        else {
          $options = isset($label) ? $label : NULL;
        }
      }
      $items['Options / Label'] = $options;
      $items['Slider Range'] = '';
    }
    //Checkbox options
    if (count($checkboxes_array) > 0) {
      $items['Question Type'] = 'checkbox';
      foreach ($checkboxes_array as $key => $value) {
        $field_collection_id = $value->value;
        $fieldcollectionitem = FieldCollectionItem::load($field_collection_id);
        $label = $fieldcollectionitem->field_checkbox_label->value;
        if ($options) {
          $options = isset($label) ? $options . ',' . $label : NULL;
        }
        else {
          $options = isset($label) ? $label : NULL;
        }
      }
      $items['Options / Label'] = $options;
      $items['Slider Range'] = '';
    }
    //Freeform text options
    if (count($free_form_text_array) > 0) {
      $items['Question Type'] = 'freeFormText';
      foreach ($free_form_text_array as $key => $value) {
        $field_collection_id = $value->value;
        $fieldcollectionitem = FieldCollectionItem::load($field_collection_id);
        $label = $fieldcollectionitem->field_free_form_text_label->value;
        if ($options) {
          $options = isset($label) ? $options . ',' . $label : NULL;
        }
        else {
          $options = isset($label) ? $label : NULL;
        }
      }
      $items['Options / Label'] = $options;
      $items['Slider Range'] = '';
    }
    //Slider options
    if (count($slider_array) > 0) {
      $items['Question Type'] = 'Slider';
      //$items['action']['sliderIncrement'] =  $chain_array2['0']->field_slider_increment['0']->value;
      //$items['action']['sliderMinimum'] =  $chain_array2['0']->field_slider_minimum['0']->value;
      //$items['action']['sliderMaximum'] =  $chain_array2['0']->field_slider_maximum['0']->value;
      foreach ($slider_array as $key => $value) {
        $field_collection_id = $value->value;
        $fieldcollectionitem = FieldCollectionItem::load($field_collection_id);
        $label = $fieldcollectionitem->field_slide_label->value;
        if ($options) {
          $options = isset($label) ? $options . ',' . $label : NULL;
        }
        else {
          $options = isset($label) ? $label : NULL;
        }
      }
      $items['Options / Label'] = $options;
      $items['Slider Range'] = $chain_array2['0']->field_slider_minimum['0']->value . ' To ' . $chain_array2['0']->field_slider_maximum['0']->value;
    }
    //Toggle options
    if (count($toggle_array) > 0) {
      $items['Question Type'] = 'Toggle';
      foreach ($toggle_array as $key => $value) {
        $field_collection_id = $value->value;
        $fieldcollectionitem = FieldCollectionItem::load($field_collection_id);
        $label = $fieldcollectionitem->field_toggle_label->value;
        if ($options) {
          $options = isset($label) ? $options . ',' . $label : NULL;
        }
        else {
          $options = isset($label) ? $label : NULL;
        }
      }
      $items['Options / Label'] = $options;
      $items['Slider Range'] = '';
    }
    return $items;
  }
}