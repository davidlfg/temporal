<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for BCT Metadata entities.
 */
class BCTMetadataViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bct_metadata']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('BCT Metadata'),
      'help' => $this->t('The BCT Metadata ID.'),
    );

    return $data;
  }

}
