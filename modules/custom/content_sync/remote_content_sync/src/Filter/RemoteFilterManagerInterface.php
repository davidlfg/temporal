<?php

namespace Drupal\remote_content_sync\Filter;

/**
 * Remote Filter Manager Interface.
 */
interface RemoteFilterManagerInterface {

  /**
   * Creates an instance of the specified filter id.
   *
   * @param string $id
   *   The filter id.
   * @param array $configuration
   *   Configuration array.
   *
   * @return \Drupal\remote_content_sync\Filter\RemoteFilter
   *   An instance of the specified filter id.
   */
  public function createInstance($id, array $configuration);

}
