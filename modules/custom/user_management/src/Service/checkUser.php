<?php
/**
 * Created by PhpStorm.
 * User: rabithk
 * Date: 8/2/16
 * Time: 11:48 AM
 */

namespace Drupal\user_management\Service;

use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user_management\Controller\userregisterController;

/**
 * Class checkUser
 * @package Drupal\user_management\Service
 */
class checkUser {
  /**
   * Function for checking user id shared is valid or not
   * @return array|\Drupal\user\Entity\User|\Symfony\Component\HttpFoundation\JsonResponse
   */
  public function isValidUser() {
    if (filter_input(INPUT_SERVER, 'HTTP_USERID')) {
      $user = User::load(filter_input(INPUT_SERVER, 'HTTP_USERID'));
    }
    else {
      $response = array(
        'errorMessage' => t('User does not exit.'),
      );
      return new JsonResponse($response, 400);
    }
    if (!$user->uid->value) {
      $response = array(
        'errorMessage' => t('User does not exit.'),
      );
      return new JsonResponse($response, 400);
    }
    else {
      $userProfile = new userregisterController();
      $userObject = $userProfile->return_user_profile($user);
      return $userObject;
    }
  }

}