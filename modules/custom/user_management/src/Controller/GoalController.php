<?php

namespace Drupal\user_management\Controller;

use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityStorageException;

/**
 * Class GoalController.
 *
 * @package Drupal\user_management\Controller
 */
class GoalController {

  /**
   * Function to get the list of all default goals.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A list of all available goals.
   */
  public function getGoals() {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'goal', '=');

    $nids = $query->execute();

    if (!empty($nids)) {
      $goals = Node::loadMultiple($nids);

      $items = array();

      /**
       * @var $goal \Drupal\Core\Entity\ContentEntityBase
       */
      foreach ($goals as $goal) {
        if ((bool) $goal->get('field_default_goal')->value) {
          $item['id']    = $goal->get('nid')->value;
          $item['title'] = $goal->get('title')->value;

          $messages = $goal->get('field_motivational_messages')->getValue();
          foreach ($messages as $message) {
            $item['messages'][] = $message['value'];
          }

          $images = $goal->get('field_motivational_images')->getValue();
          foreach ($images as $image) {
            $file             = File::load($image['target_id'])->url();
            $item['images'][] = $file;
          }
          $items[] = $item;
        }
      }
      return new JsonResponse($items, 200);

    }
    else {
      $response = array();
      return new JsonResponse($response, 200);
    }
  }

  /**
   * Function to get the list of goals for a user.
   *
   * @param $request \Symfony\Component\HttpFoundation\Request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A list with all goals for the specified user.
   */
  public function getGoalsPerUser(Request $request) {

    $headers = getallheaders();
    if (!empty($headers['UUID'])) {
      $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);

      $query = \Drupal::entityQuery('node')
        ->condition('uid', $uid, '=')
        ->condition('type', 'goal', '=');

      $nids = $query->execute();

      if (!empty($nids)) {
        $goals = Node::loadMultiple($nids);

        $i = 0;
        $items = array();

        /**
         * @var $goal \Drupal\Core\Entity\ContentEntityBase
         */
        foreach ($goals as $goal) {
          $items[$i]['title'] = $goal->get('title')->getValue()[0]['value'];
          $items[$i]['id'] = $goal->get('nid')->getValue()[0]['value'];
          $i++;
        }
        return new JsonResponse($items, 200);

      }
      else {
        $response = array();
        return new JsonResponse($response, 404);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error: There are missing parameters.'),
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * Function to get the list of messages for a goal.
   *
   * @param $request \Symfony\Component\HttpFoundation\Request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A list with the motivational messages for the goal.
   */
  public function getGoalData(Request $request) {
    $headers = getallheaders();
    if (!empty($headers['id'])) {
      $goal = Node::load($headers['id']);

      if (!empty($goal)) {
        $response = array();
        $response['title'] = $goal->get('title')->getValue()[0]['value'];
        $messages = $goal->get('field_motivational_messages')->getValue();
        foreach ($messages as $message) {
          $response['messages'][] = $message['value'];
        }
        $images = $goal->get('field_motivational_images')->getValue();
        foreach ($images as $image) {
          $file = File::load($image['target_id']);
          $path = file_create_url($file->getFileUri());
          $response['images'][] = $path;
        }
        return new JsonResponse($response, 200);

      }
      else {
        $response = array();
        return new JsonResponse($response, 200);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error: There are missing parameters.'),
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * Function to create goals.
   *
   * @param $request \Symfony\Component\HttpFoundation\Request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   empty 200 response or error message with number.
   */
  public function addGoal(Request $request) {
    $headers = getallheaders();
    $content = json_decode($request->getContent(), TRUE);
    if (!empty($headers['UUID'])&&(!empty($content['title']))) {
      $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);
      $title = $content['title'];
      if (!empty($uid)) {
        $node = Node::create([
          'type'                    => 'goal',
          'title'                   => $title,
          'uid'                     => $uid,
        ]);

        $saved = TRUE;

        try {
          $node->save();
        }
        catch (EntityStorageException $e) {
          $saved = FALSE;
        }
        if ($saved) {
          $response = array(
            'title' => $node->get('title')->getValue()[0]['value'],
            'id' => $node->get('nid')->getValue()[0]['value'],
            'UUID' => $headers['UUID'],
          );
          return new JsonResponse($response, 200);
        }
        else {
          $response = array(
            'errorMessage' => 'Error ' . $e,
          );
          return new JsonResponse($response, 400);
        }
      }
      else {
        $response = array(
          'errorMessage' => t('Error: User not found.'),
        );
        return new JsonResponse($response, 403);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error: There are missing parameters.'),
      );
      return new JsonResponse($response, 400);
    }
  }

}
