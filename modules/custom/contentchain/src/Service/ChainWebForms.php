<?php
/**
 * ChainWebForms file to handle all forms submit
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Service;

use \DrewM\MailChimp\MailChimp;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;
Use Drupal\user_management\Service\getTaxonomyTerm;


class ChainWebForms {
  /*
   * Function to retrieve form values inside a chain
   * @param @array
   * FormId : care-taker-subscription : For caretaker registration
   */
  public function submitChainWebForms($content) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $userId = $headlessAccount->id();

    $config = \Drupal::configFactory()->getEditable('user_management.settings');
    if ($content['formId']) {
      $careTaker = $content['selected'];

      switch ($content['formId']) {
        case  'care-taker-subscription':
          $firstName = $careTaker['field_caretaker_first_name'];
          $lastName = $careTaker['field_caretaker_last_name'];
          $email = $careTaker['field_caretaker_email'];
          $referenceDate = $careTaker['ReferenceDate'];
          $jointReplacementType = $user['field_joint_replacement_type'];
          $oldCaretakerEmail = isset($user['field_caretaker_email']) ? $user['field_caretaker_email'] : '';
          if ($oldCaretakerEmail != '') {//Update
            if ($oldCaretakerEmail != $email) {
              //TODO ;1st check new email is already used or not
              $res = self::CareTakerSubscription(array(
                'caretakerEmail' => $email,
                'jointReplacementType' => $jointReplacementType
              ), 'Status');
              if ($res['status'] != 404) {
                //Email is already in use by other user
                $response = array(
                  'ErrorMessage' => t('Caretaker Email is already used by other user.'),
                  'code' => 400
                );
                return $response;
              }
              //unSubscribe old caretaker
              $oldCareTaker = array(
                'userId' => $userId,
                'caretakerFirstName' => $user['field_caretaker_first_name'],
                'caretakerLastName' => $user['field_caretaker_last_name'],
                'caretakerEmail' => $user['field_caretaker_email'],
                'ReferenceDate' => $referenceDate,
                'jointReplacementType' => $jointReplacementType
              );
              $result = self::CareTakerSubscription($oldCareTaker, 'Status');
              if ($result['status'] != 404) {
                self::CareTakerSubscription($oldCareTaker, 'Remove');
              }
            }
          }
          else {//New
            //TODO ;1st check new email is already used or not
            $res = self::CareTakerSubscription(array(
              'caretakerEmail' => $email,
              'jointReplacementType' => $jointReplacementType
            ), 'Status');
            if ($res['status'] != 404) {
              //Email is already in use by other user
              $response = array(
                'ErrorMessage' => t('Caretaker Email is already used by other user.'),
                'code' => 400
              );
              return $response;
            }
          }
//If no error do the subscription
          $userProfile = array(
            'userId' => $userId,
            'caretakerFirstName' => $firstName,
            'caretakerLastName' => $lastName,
            'caretakerEmail' => $email,
            'ReferenceDate' => $referenceDate,
            'jointReplacementType' => $jointReplacementType
          );
          return self::mailChimpApiCall($userProfile);
          break;
      }
    }
  }

  /*
   * Function to call mailChimpApiCall
   * @param : userProfile
   * @return response array
   */
  public function mailChimpApiCall($userProfile) {
    //check subscription status
    $status = self::CareTakerSubscription($userProfile, 'Status');
    if ($status['status'] == 404) {
      //TO crete :subscribed or pending
      $result = self::CareTakerSubscription($userProfile, 'Subscribe');
    }
    else {
      //TO update :subscribed or pending
      $result = self::CareTakerSubscription($userProfile, 'Update');
    }
    $userProfile['subscriptionStatus'] = $result['status'];
    if ($result['status']) {
      //Update user profile with caretaker details
      $response = self::updateUserProfile($userProfile);
      if ($response == 200) {
        $response = array(
          'Message' => t('Caretaker Email is added.'),
          'code' => 200
        );
        return $response;
      }
      else {
        //Error
        $response = array(
          'ErrorMessage' => t('Error.'),
          'code' => 400
        );
        return $response;
      }
    }
  }

  /*
   * Function to Subscribe/Update/Remove caretaker email
   * status:-
   *  subscribed : Subscription confirmed
   *  pending : waiting for Subscription confirmation
   *  404 : The requested resource could not be found
   * Subscribe someone to a list (with a post to the list/{listID}/members method)
   * Update a list member with more information (using patch to update):
   * Remove a list member using the delete method:
   *
   * @param $userProfile
   * @param $type {status/Subscribe/Update/Remove}
   * @return array
   */
  public function CareTakerSubscription($userProfile, $type) {
    //Check Default JointType
    $taxonomyTerm = new getTaxonomyTerm();
    $application = $taxonomyTerm->listTaxonomyTerm('application');
    $defaultType = array_search('Knee Replacement Surgery', $application);
    //End check
    $jointType = isset($userProfile['jointReplacementType']) ? $userProfile['jointReplacementType'] : $defaultType;
    switch ($jointType) {
      case '9':
        $jType = 'knees';
        break;
      case '10':
        $jType = 'hips';
        break;
      default:
        $jType = 'knees';
        break;
    }
    $config = \Drupal::configFactory()->getEditable('user_management.settings');
    $apiKey = $config->get('mailChimp_apiKey');
    $apiKey = isset($apiKey) ? $apiKey : 'd7219813c8c9b27e14b031e8d8b97bf6-us13';
    $MailChimp = new MailChimp($apiKey);
    $list_id = $config->get($jType . '_mailChimp_listId');
    $list_id = isset($list_id) ? $list_id : '3983d5fab4';

    $firstName = isset($userProfile['caretakerFirstName']) ? $userProfile['caretakerFirstName'] : '';
    $lastName = isset($userProfile['caretakerLastName']) ? $userProfile['caretakerLastName'] : '';
    $email = isset($userProfile['caretakerEmail']) ? $userProfile['caretakerEmail'] : '';
    $referenceDate = isset($userProfile['ReferenceDate']) ? $userProfile['ReferenceDate'] : '';

    $subscriber_hash = $MailChimp->subscriberHash($email);

    switch ($type) {
      case 'Status':
        $result = $MailChimp->get("lists/$list_id/members/$subscriber_hash", []);
        \Drupal::logger('CareTaker Status')->notice(print_r($result, TRUE));
        break;
      case 'Subscribe':
        $result = $MailChimp->post("lists/$list_id/members", [
          'email_address' => $email,
          'merge_fields' => [
            'FNAME' => $firstName,
            'LNAME' => $lastName,
            'MMERGE3' => $referenceDate
          ],
          'status' => 'pending',
        ]);
        \Drupal::logger('CareTaker Subscribe')->notice(print_r($result, TRUE));
        if (isset($result['title']) == 'Member Exists') {
          $result = self::CareTakerSubscription($userProfile, 'Update');
        }
        break;
      case 'Update':
        $result = $MailChimp->put("lists/$list_id/members/$subscriber_hash", [
          'merge_fields' => [
            'FNAME' => $firstName,
            'LNAME' => $lastName,
            'MMERGE3' => $referenceDate
          ],
          'status' => 'pending',
        ]);
        \Drupal::logger('CareTaker Update')->notice(print_r($result, TRUE));
        break;
      case 'Remove':
        $result = $MailChimp->patch("lists/$list_id/members/$subscriber_hash", [
          'status' => 'unsubscribed',
        ]);
        \Drupal::logger('CareTaker Remove')->notice(print_r($result, TRUE));
        break;
    }
    return $result;
  }

  /*
   * Function to Check caretaker email is already used by any other user
   * @param $careTakerEmail ,$userId
   * @return caretakerEmail or NULL
   */
  public function checkCareTakerEmail($careTakerEmail, $userId) {
    $response = \Drupal::database()->select('user__field_caretaker_email', 'ce')
      ->fields('ce', array('field_caretaker_email_value'))
      ->condition('ce.entity_id', $userId, '!=')
      ->condition('ce.field_caretaker_email_value', $careTakerEmail, '=')
      ->execute()
      ->fetchField();
    return $response;
  }


  /**
   * Update user profile to add caretaker information
   * @param array $userProfile
   * @return status 200 is success , 400 if failure
   */
  public function updateUserProfile($userProfile) {
    $headers = getallheaders();
    global $base_url;
    $registerUserUrl = $base_url . '/user_management/update-profile';
    try {
      $request = \Drupal::httpClient()->post($registerUserUrl,
        array(
          'body' => Json::encode(array(
            'field_caretaker_first_name' => $userProfile['caretakerFirstName'],
            'field_caretaker_last_name' => $userProfile['caretakerLastName'],
            'field_caretaker_email' => $userProfile['caretakerEmail'],
            'field_caretaker_subscription_sta' => $userProfile['subscriptionStatus'],
          )),
          'headers' => array(
            'uuid' => $headers['uuid'],
            'content-type' => 'application/json'
          )
        ));

    } catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode() == 400) {
        \Drupal::logger('Auth User Flow')
          ->notice('Error while updating profile: ' .
            Json::decode($e->getResponse()->getBody()->getContents()));
      }
      else {
        \Drupal::logger('Auth User Flow')
          ->error('An error occurred updating profile "' .
            $userProfile['userId'] . '"');
      }
    }
    return $request->getStatusCode();
  }
}