<?php
/**
 * FileUploadService file to handle file upload service.
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\user_management\Service;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class FileUploadService {
  private $BASE_URL;

  /**
   * FileUploadService constructor.
   */
  public function __construct()
  {
    $BASE_URL = \Drupal::config('user_management.settings')->get("apigee_url");
  }

  public function postDataBundle($email, $file)
  { 
    $config = \Drupal::configFactory()->getEditable('user_management.settings');
    /** @var  Client client */
    $client   = \Drupal::service('http_client');
    $url       = $config->get('apigee_fileupload_uri');
	$data = array(
				'multipart' => array(
					array('name' => 'email', 'contents' => $email),
					array('name' => 'bundle','contents' => fopen($file, 'r')),
				),
				'headers' => array('apikey' =>'Sa2AxY0r1bxGfxlk76wjRLER9jiUEJfB')
			);	
    try
    {
      $request  = $client->request('POST',$url, $data);
	  $headers  = $request->getHeaders();
	  $response = array(
		  'StatusCode' => $request->getStatusCode(),
		  'ResponseBody' => $request->getBody()->getContents(),
		  'ResponseTime' => $headers['Date'][0],
		  'Email' => $email
	  );
	  \Drupal::logger('FileUpload')->notice('status :' .print_r($response ,TRUE)  .'for the file'. $file );
	  //Check uplod success or not
	  self::checkUplaodStatus($email, $response['ResponseBody']);
    }
    catch(ClientException $e)
    {
		if ($e->getResponse()->getStatusCode()) {
			$request = $e->getResponse();	 
			$headers  = $request->getHeaders();
			$response = array(
				'StatusCode' => $request->getStatusCode(),
				'ResponseBody' => $request->getBody()->getContents(),
				'ResponseTime' => $headers['Date'][0],
		        'Email' => $email				
			);	  
			\Drupal::logger('FileUpload')->error('status :' .print_r($response ,TRUE)  .'for the file'. $file );		 
		}
    }
    return $response;
  }
  
  public function checkUplaodStatus($email, $ResponseBody)
  { 
    $ResponseBody = json_decode($ResponseBody);
	$fileId = $ResponseBody->fileId;
    $config = \Drupal::configFactory()->getEditable('user_management.settings');
    /** @var  Client client */
    $client1   = \Drupal::service('http_client');
    $url1      = 'https://jnj-dev.apigee.net/rwb/v1/status';//$config->get('apigee_fileupload_status_uri');
	$data1 = array(
				'multipart' => array(
					array('name' => 'email', 'contents' => $email),
					array('name' => 'fileId','contents' => $fileId),
				),
				'headers' => array('apikey' =>'Sa2AxY0r1bxGfxlk76wjRLER9jiUEJfB')
			);	
			
    try
    {
      $request1  = $client1->request('POST',$url1, $data1);
	  $headers  = $request1->getHeaders();
	  $upresponse = array(
		  'StatusCode' => $request1->getStatusCode(),
		  'ResponseBody' => $request1->getBody()->getContents(),
		  'ResponseTime' => $headers['Date'][0],
		  'Email' => $email
	  );  
	  \Drupal::logger('FileUploadStatus')->notice('status :' .print_r($upresponse ,TRUE));
    }
    catch(ClientException $e)
    {  
		if ($e->getResponse()->getStatusCode()) {
			$request1 = $e->getResponse();	
			$headers  = $request1->getHeaders();
			$upresponse = array(
				'StatusCode' => $request1->getStatusCode(),
				'ResponseBody' => $request1->getBody()->getContents(),
				'ResponseTime' => $headers['Date'][0],
		        'Email' => $email				
			);	  
			\Drupal::logger('FileUploadStatus')->error('status :' .print_r($upresponse ,TRUE) );		 
		}
    }
    //return $response;
  }
}