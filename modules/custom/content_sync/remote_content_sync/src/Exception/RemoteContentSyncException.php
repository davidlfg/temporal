<?php

namespace Drupal\remote_content_sync\Exception;

/**
 * Remote content sync exception.
 */
class RemoteContentSyncException extends \RuntimeException {}
