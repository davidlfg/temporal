<?php
/**
 * contentchainController file to list content chain for a week
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\contentchain\Service\chainContent;
Use Drupal\user_management\Service\getTaxonomyTerm;

/**
 * Class contentchainController
 * @package Drupal\contentchain\Controller
 */
class contentchainController extends ControllerBase {

  /**
   * Function to list all activities for a week
   * Depend on before or after surgery date.
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Exception
   */
  public function weekContentChain() {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $user['uid'] = $headlessAccount->id();

    $type = isset($headers['type']) ? $headers['type'] : 'home'; //home ,pre-surgery or post-surgery
    $chainContent = new chainContent();
    $date = DateTimePlus::createFromFormat('m-d-Y', $user['field_surgery_date']);
    $surgery_date = $date->format('Y-m-d');

    //Start: Adding application type selection
    $taxonomyTerm = new getTaxonomyTerm();
    $application = $taxonomyTerm->listTaxonomyTerm('application');
    $defaultType = array_search('Knee Replacement Surgery', $application);
    $jointReplacementType = isset($user['field_joint_replacement_type']) ? $user['field_joint_replacement_type'] : $defaultType;
    if ($jointReplacementType == '' or $jointReplacementType == 0) {
      $jointReplacementType = $defaultType;
    }
    //End: Adding application type selection
    //User time stamp
    $requestDateTime = isset($headers['UserTimeStamp']) ? $headers['UserTimeStamp'] : \date('m-d-Y H:i:s');
    $date = DateTimePlus::createFromFormat('m-d-Y H:i:s', $requestDateTime);
    $now = $date->format('Y-m-d');
    $week_diff = $chainContent->weekDifference($surgery_date, $now);

    $items = [];
    if (strtotime($surgery_date) > strtotime($now)) {
      #$date occurs in the future
      $before_surgery = 1;//'Before surgery';
      $chain_info = $chainContent->get_chainCount($before_surgery, $jointReplacementType);
    }
    else {
      #$date occurs now or in the past
      $before_surgery = 0; //'After surgery';
      //For after surgery set a longer week to get all chain information
      $chain_info = $chainContent->get_chainCount($before_surgery, $jointReplacementType);
    }

    $items['totalchainCount'] = $chain_info['totalchainCount'];
    $items['totalRequired'] = $chain_info['totalRequired'];
    $items['totalNotRequired'] = $chain_info['totalNotRequired'];
    switch ($type) {
      case 'home':
        if ($week_diff > 4) { //If surgery date is more than 4 week set the week to 4 week before surgery.
          $week_diff = 4;
        }
        $view = views_embed_view('content_chain', 'rest_export_1', $week_diff, $before_surgery, $jointReplacementType);
        break;
      case 'pre-surgery':
        $view = views_embed_view('content_chain_pre_surgery', 'rest_export_1', 1, $jointReplacementType);
        break;
      case 'post-surgery':
        $view = views_embed_view('content_chain_pre_surgery', 'rest_export_1', 0, $jointReplacementType);
        break;
      default:
        $view = views_embed_view('content_chain', 'rest_export_1', $week_diff, $before_surgery, $jointReplacementType);
    }

    $view_result = \Drupal::service('renderer')->render($view);
    $chain_json = (string) $view_result;
    $chain_array = json_decode($chain_json);

    if (count($chain_array) > 0) {
      $item = $chainContent->getChainContent($items, $chain_array, $user, 0, $before_surgery, $week_diff);
    }
    else {
      $item = array();
    }

    if (count($item) > 0) {
      return new JsonResponse($item, 200);
    }
    else {
      $response = array(
        'errorMessage' => 'No Content Available',
      );
      return new JsonResponse($response, 400);
    }
  }


}