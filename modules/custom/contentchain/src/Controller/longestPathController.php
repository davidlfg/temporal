<?php
/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release x.x.x
 */

namespace Drupal\contentchain\Controller;

use Drupal\field_collection\Entity\FieldCollectionItem;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class longestPathController
 * @package Drupal\contentchain\Controller
 */
class longestPathController {

  /**
   * @param $nodeId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function longestPath($nodeId) {
    $view = views_embed_view('individual_chain', 'rest_export_1', $nodeId);
    $view_result = \Drupal::service('renderer')->render($view);
    $chain_json = (string) $view_result;
    $chain_array = json_decode($chain_json);
    $chain_link_array = explode(",", $chain_array['0']->field_content_chain);
    $items = array();
    $i = 0;
    foreach ($chain_link_array as $chainLinkId) {
      $items = self::longestPathRecursive($items, $chainLinkId, $i, $nodeId);
      $i++;
    }

    $pathArray = self::array_value_recursive('TotalLinkCount', $items);
    $longPath['longPathCount'] = isset($pathArray) ? max($pathArray) : $i;
    return new JsonResponse($longPath, 200);
  }

  /**
   * @param $key
   * @param array $arr
   * @return array|mixed
   */
  function array_value_recursive($key, array $arr) {
    $val = array();
    array_walk_recursive($arr, function ($v, $k) use ($key, &$val) {
      if ($k == $key) {
        array_push($val, $v);
      }
    });
    return count($val) > 1 ? $val : array_pop($val);
  }

  /**
   * @param $items
   * @param $chainLinkId
   * @param $i
   * @param $path
   * @return mixed
   */
  function longestPathRecursive($items, $chainLinkId, $i, $path) {
    $view = views_embed_view('chain_link_view', 'rest_export_1', $chainLinkId);
    $view_result1 = \Drupal::service('renderer')->render($view);
    $chain_json1 = (string) $view_result1;
    $chain_array1 = json_decode($chain_json1);

    $view = views_embed_view('action', 'rest_export_1', $chain_array1['0']->field_prompt_action);
    $view_result2 = \Drupal::service('renderer')->render($view);
    $chain_json2 = (string) $view_result2;
    $chain_array2 = json_decode($chain_json2);
    $buttons_array = isset($chain_array2['0']->field_buttons) ? $chain_array2['0']->field_buttons : array();
    //Button options
    if (count($buttons_array) > 0) {
      $j = 0;
      foreach ($buttons_array as $key => $value) {
        $fcId = $value->value;
        $fc = FieldCollectionItem::load($fcId);
        foreach ($fc as $fcKey => $field) {
          if ($fcKey == 'field_next_chain_id') {
            $chainId = \Drupal::database()
              ->select('field_collection_item__field_next_chain_id', 'nc')
              ->fields('nc', array('field_next_chain_id_target_id'))
              ->condition('nc.entity_id', $fc->item_id->value)
              ->execute()
              ->fetchField();
            if ($chainId != '') {
              $subChainListCount = \Drupal::database()
                ->select('node__field_content_chain', 'cc')
                ->fields('cc', array('field_content_chain_target_id'))
                ->condition('cc.entity_id', $chainId)
                ->countQuery()->execute()->fetchField();
              $subChainPromptId = \Drupal::database()
                ->queryRange("SELECT cc.field_content_chain_target_id FROM {node__field_content_chain} cc  WHERE cc.entity_id = :nid ", $subChainListCount - 1, $subChainListCount, array(':nid' => $chainId))
                ->fetchField();

              $updatedPath = $path . '/' . $chainId;
              //\Drupal::logger('$updatedPath')->notice(print_r($updatedPath, TRUE));
              $exp = explode("/", $updatedPath);
              switch (count($exp)) {
                case 1:
                  $items[$exp[0]]['TotalLinkCount'] = $i + 1;
                  break;
                case 2:
                  $items[$exp[0]]['TotalLinkCount'] = $i + 1;
                  $items[$exp[0]][$exp[1]]['TotalLinkCount'] = $items[$exp[0]]['TotalLinkCount'] + $subChainListCount;
                  break;
                case 3:
                  $items[$exp[0]][$exp[1]][$exp[2]]['TotalLinkCount'] = $items[$exp[0]][$exp[1]]['TotalLinkCount'] + $subChainListCount;
                  break;
                case 4:
                  $items[$exp[0]][$exp[1]][$exp[2]][$exp[3]]['TotalLinkCount'] = $items[$exp[0]][$exp[1]][$exp[2]]['TotalLinkCount'] + $subChainListCount;
                  break;
                case 5:
                  $items[$exp[0]][$exp[1]][$exp[2]][$exp[3]][$exp[4]]['TotalLinkCount'] = $items[$exp[0]][$exp[1]][$exp[2]][$exp[3]]['TotalLinkCount'] + $subChainListCount;
                  break;
                case 6:
                  $items[$exp[0]][$exp[1]][$exp[2]][$exp[3]][$exp[4]][$exp[5]]['TotalLinkCount'] = $items[$exp[0]][$exp[1]][$exp[2]][$exp[3]][$exp[4]]['TotalLinkCount'] + $subChainListCount;
                  break;
              }

              $items = self::longestPathRecursive($items, $subChainPromptId, $i, $updatedPath);
            }
          }
          $j++;
        }
      }
    }
    // \Drupal::logger('1')->notice(print_r($longPath, TRUE));
    return $items;
  }

}