Colorful List Module
====================

This module is used to whitelist and blacklist a list of IP address from URLs

#### Developers:

* [Chris Boffa](mailto:cboffa@its.jnj.com)
* [Pablo Andermann](mailto:pablo.andermann@globant.com)
* [David Nova](mailto:david.nova@globant.com)