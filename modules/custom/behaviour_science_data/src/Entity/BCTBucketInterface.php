<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining BCT Bucket entities.
 *
 * @ingroup behaviour_science_data
 */
interface BCTBucketInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the BCT Bucket name.
   *
   * @return string
   *   Name of the BCT Bucket.
   */
  public function getName();

  /**
   * Sets the BCT Bucket name.
   *
   * @param string $name
   *   The BCT Bucket name.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTBucketInterface
   *   The called BCT Bucket entity.
   */
  public function setName($name);

  /**
   * Gets the BCT Bucket creation timestamp.
   *
   * @return int
   *   Creation timestamp of the BCT Bucket.
   */
  public function getCreatedTime();

  /**
   * Sets the BCT Bucket creation timestamp.
   *
   * @param int $timestamp
   *   The BCT Bucket creation timestamp.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTBucketInterface
   *   The called BCT Bucket entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the BCT Bucket published status indicator.
   *
   * Unpublished BCT Bucket are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the BCT Bucket is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a BCT Bucket.
   *
   * @param bool $published
   *   TRUE to set this BCT Bucket to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTBucketInterface
   *   The called BCT Bucket entity.
   */
  public function setPublished($published);

}
