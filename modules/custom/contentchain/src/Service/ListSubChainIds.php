<?php
/**
 *  This file will list all sub-chain id of a giving chain
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 *  This file contains trade secrets of Johnson & Johnson, Inc.
 *  No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 *  permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 *
 */

namespace Drupal\contentchain\Service;

use Drupal\field_collection\Entity\FieldCollectionItem;


class ListSubChainIds {
  /*
   * Function to list all sub-chain of a giving chain id
   * @param int chainId
   * @return array;
   */

  public function getSubChainId($nodeId) {
    $SubChainIds = array();
    $view = views_embed_view('individual_chain', 'rest_export_1', $nodeId);
    $view_result = \Drupal::service('renderer')->render($view);
    $chain_json = (string) $view_result;
    $chain_array = json_decode($chain_json);
    $chain_link_array = isset($chain_array['0']->field_content_chain) ? explode(",", $chain_array['0']->field_content_chain) : array();
    if (count($chain_link_array) > 0) {
      foreach ($chain_link_array as $nid) {
        $result = \Drupal::database()
          ->query("SELECT type FROM {node} WHERE nid = :nid;", array(':nid' => $nid))
          ->fetchField();
        $nodeType = array(
          'prompt_text_only',
          'prompt_text_image',
          'prompt_text_image_audio',
          'prompt_text_audio'
        );
        if (in_array($result, $nodeType) == TRUE) {
          $view = views_embed_view('chain_link_view', 'rest_export_1', $nid);
          $view_result1 = \Drupal::service('renderer')->render($view);
          $chain_json1 = (string) $view_result1;
          $chain_array1 = json_decode($chain_json1);

          $view = views_embed_view('action', 'rest_export_1', $chain_array1['0']->field_prompt_action);
          $view_result2 = \Drupal::service('renderer')->render($view);
          $chain_json2 = (string) $view_result2;
          $chain_array2 = json_decode($chain_json2);
          $buttons_array = isset($chain_array2['0']->field_buttons) ? $chain_array2['0']->field_buttons : array();
          //\Drupal::logger('Sub Chain')->notice(print_r($listSubChain, TRUE));
          //Only for button options
          if (count($buttons_array) > 0) {
            foreach ($buttons_array as $key => $value) {
              $fcId = $value->value;
              $chainId = \Drupal::database()
                ->select('field_collection_item__field_next_chain_id', 'nc')
                ->fields('nc', array('field_next_chain_id_target_id'))
                ->condition('nc.entity_id', $fcId)
                ->execute()
                ->fetchField();
              if ($chainId) {
                $SubChainIds[] = (int) $chainId;
              }
            }
          }
        }
      }
      return $SubChainIds;
    }
  }
}