<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for BCT Temporary Array entities.
 */
class BCTTemporaryArrayViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bct_temporary_array']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('BCT Temporary Array'),
      'help' => $this->t('The BCT Temporary Array ID.'),
    );

    return $data;
  }

}
