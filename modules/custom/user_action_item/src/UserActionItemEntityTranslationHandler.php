<?php

namespace Drupal\user_action_item;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for user_action_item_entity.
 */
class UserActionItemEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
