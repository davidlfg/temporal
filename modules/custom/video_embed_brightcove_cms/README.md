**Video Embed Brightcove CMS**

A video_embed_field integration with Brightcove.

**NOTES**

* The Brightcove CMS API is used to get the video images, so a valid Brightcove account is required to generate thumbnails.
* If you have not installed this module with composer you will need to run `composer require brightcove/api:dev-master` in your Drupal root directory, or use Composer Manager, to make sure all dependencies are added.