<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for BCT Reference entities.
 */
class BCTReferenceViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bct_reference']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('BCT Reference'),
      'help' => $this->t('The BCT Reference ID.'),
    );

    return $data;
  }

}
