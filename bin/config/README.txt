Per Environment Config Folder:

Drupal 8 and drush don't allow you to import the 2 config sets at the same time, so we need to run 2 drush commands to import our config.
 drush config-import --source=/path/to/project/config/base && drush config-import --partial --source=/path/to/project/config/dev.
 The first command imports the base config and the second applies any per environment overrides.


Usage as per current folder structure :

option 1 :

drush config-import --source=bin/config-sync && drush config-import --partial --source=bin/config/local

--------
option 2:
f you add the source to the settings.php file you will not have to declare the path every time you run the command.
For example:

$config_directories = array();
$config_directories[CONFIG_SYNC_DIRECTORY] = 'config/sync';
$config_directories['test'] = 'config/test';
$config_directories['prod'] = 'config/prod';


In which can you can run drush config-import test --partial -y


