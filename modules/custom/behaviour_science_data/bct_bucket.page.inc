<?php

/**
 * @file
 * Contains bct_bucket.page.inc.
 *
 * Page callback for BCT Bucket entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for BCT Bucket templates.
 *
 * Default template: bct_bucket.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_bct_bucket(array &$variables) {
  // Fetch BCTBucket Entity Object.
  $bct_bucket = $variables['elements']['#bct_bucket'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
