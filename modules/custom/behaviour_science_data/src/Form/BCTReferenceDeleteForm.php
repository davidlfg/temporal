<?php

namespace Drupal\behaviour_science_data\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting BCT Reference entities.
 *
 * @ingroup behaviour_science_data
 */
class BCTReferenceDeleteForm extends ContentEntityDeleteForm {


}
