<?php
/**
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\disruptive_checkin\DateTime;

use Drupal\Core\Datetime\DrupalDateTime;

class DateTime extends DrupalDateTime
{
  
  /**
   * Constructs a new instance based on a DrupalDateTime object.
   * 
   * @param DrupalDateTime $drupalDateTime
   * @return DateTime
   */
  public static function createFromDrupalDateTime($drupalDateTime)
  {
    return new self($drupalDateTime->format(static::FORMAT));
  }
  
  /**
   * Adds/substracts days to defined date, depending on the sign of the parameter.
   * 
   * @param int $days
   * @return DateTime
   */
  public function addDays($days)
  {
    $this->dateTimeObject->modify($days . ' day');
    return $this;
  }

 /**
   * Adds/substracts weeks to defined date, depending on the sign of the parameter.
   * 
   * @param int $weeks
   * @return DateTime
   */
  public function addWeeks($weeks)
  {
    $this->addDays($weeks * 7);
    return $this;
  }
  
  /**
   * Checks if current object is after the given date (orEqual).
   * 
   * @param DateTime $date
   * @param boolean $orEqual
   * @return boolean
   */
  public function isAfter($date, $orEqual = TRUE)
  {
    $diff = $date->getDateTimeInstance()->diff($this->dateTimeObject);
    if ($orEqual) {
      return !$diff->invert;
    } else {
      return !$diff->invert && $diff->days > 0;
    }
  }
  
  /**
   * Checks if current object is before the given date (orEqual).
   * 
   * @param DateTime $date
   * @param boolean $orEqual
   * @return boolean
   */
  public function isBefore($date, $orEqual = TRUE)
  {
    return !$this->isAfter($date, !$orEqual);
  }
  
  /**
   * Gets the related object.
   * @return \DateTime
   */
  public function getDateTimeInstance()
  {
    return clone $this->dateTimeObject;
  }
}
