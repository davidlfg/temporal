<?php

namespace Drupal\remote_content_sync\Filter;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Remote Filter interface.
 */
interface RemoteFilterInterface {

  const OR_OPERATOR = 'or';
  const AND_OPERATOR = 'and';

  /**
   * The filter function.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return bool
   *   True if the entity matches the filter.
   */
  public function filter(ContentEntityInterface $entity);

}
