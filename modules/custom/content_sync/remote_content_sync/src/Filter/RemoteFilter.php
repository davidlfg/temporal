<?php

namespace Drupal\remote_content_sync\Filter;

/**
 * Remote Filter parent class.
 */
abstract class RemoteFilter implements RemoteFilterInterface {

  /**
   * The filter configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The filter operator.
   *
   * @var string
   */
  protected $operator;

  /**
   * RemoteFilter constructor.
   */
  public function __construct($configuration, $operator) {
    $this->configuration = $configuration;
    $this->operator = $operator;
  }

}
