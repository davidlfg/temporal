<?php

namespace Drupal\behaviour_science_data\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for BCT Bucket edit forms.
 *
 * @ingroup behaviour_science_data
 */
class BCTBucketForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\behaviour_science_data\Entity\BCTBucket */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label BCT Bucket.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label BCT Bucket.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.bct_bucket.canonical', ['bct_bucket' => $entity->id()]);
  }

}
