<?php

namespace Drupal\remote_content_sync\Filter;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Entity Type Remote Filter class.
 */
class EntityTypeRemoteFilter extends RemoteFilter {

  /**
   * {@inheritdoc}
   */
  public function filter(ContentEntityInterface $entity) {
    return in_array($entity->getEntityTypeId(), $this->configuration['types']);
  }

}
