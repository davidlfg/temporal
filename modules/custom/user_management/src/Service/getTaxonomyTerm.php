<?php
/**
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\user_management\Service;

/**
 * Class getTaxonomyTerm
 * @package Drupal\user_management\Service
 */
class getTaxonomyTerm {

  /**
   * @param $vid
   * @return array
   */
  public function listTaxonomyTerm($vid) {
    $appVersion = array();
    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree($vid);
    foreach ($terms as $key => $termObj) {
      $appVersion[$termObj->tid] = $termObj->name;
    }
    return $appVersion;
  }
}