<?php
/**
 * @file
 * Contains Drupal\deploy_hpb\DeployHpbServiceProvider
 */

namespace Drupal\deploy_hpb;


use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class DeployHpbServiceProvider extends ServiceProviderBase {
  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('replication.changes_factory');

    $definition->setClass('Drupal\deploy_hpb\ChangesFactory');
  }
}