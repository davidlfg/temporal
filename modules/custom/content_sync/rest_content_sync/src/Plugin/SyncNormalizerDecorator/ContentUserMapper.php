<?php

namespace Drupal\rest_content_sync\Plugin\SyncNormalizerDecorator;

use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a decorator to map field names.
 *
 * @SyncNormalizerDecorator(
 *   id = "content_user_mapper",
 *   name = @Translation("Content User Mapper"),
 * )
 */
class ContentUserMapper extends SyncNormalizerDecoratorBase implements ContainerFactoryPluginInterface {

  /**
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, AccountProxyInterface $current_user, EntityTypeManagerInterface $manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function decorateNormalization(array &$normalized_entity, ContentEntityInterface $entity, $format, array $context = []) {
  }

  /**
   * {@inheritdoc}
   */
  public function decorateDenormalization(array &$normalized_entity, $type, $format, array $context = []) {
    if (!empty($normalized_entity['uid'])) {
      /* @var UserInterface $user */
      $user = $this->manager->getStorage('user')
                            ->load($this->currentUser->id());
      if ($user && !$user->isAnonymous()) {
        $normalized_entity['uid'] = [
          'target_id' => $user->id(),
          'target_uuid' => $user->uuid(),
          'target_type' => $user->getEntityTypeId(),
          'url' => $user->toUrl()->toString(),
        ];
      }
    }
  }
}
