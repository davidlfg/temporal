<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining BCT Reference entities.
 *
 * @ingroup behaviour_science_data
 */
interface BCTReferenceInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the BCT Reference name.
   *
   * @return string
   *   Name of the BCT Reference.
   */
  public function getName();

  /**
   * Sets the BCT Reference name.
   *
   * @param string $name
   *   The BCT Reference name.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTReferenceInterface
   *   The called BCT Reference entity.
   */
  public function setName($name);

  /**
   * Gets the BCT Reference creation timestamp.
   *
   * @return int
   *   Creation timestamp of the BCT Reference.
   */
  public function getCreatedTime();

  /**
   * Sets the BCT Reference creation timestamp.
   *
   * @param int $timestamp
   *   The BCT Reference creation timestamp.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTReferenceInterface
   *   The called BCT Reference entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the BCT Reference published status indicator.
   *
   * Unpublished BCT Reference are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the BCT Reference is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a BCT Reference.
   *
   * @param bool $published
   *   TRUE to set this BCT Reference to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTReferenceInterface
   *   The called BCT Reference entity.
   */
  public function setPublished($published);

}
