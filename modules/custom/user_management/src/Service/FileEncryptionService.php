<?php
/**
 * FileEncryptionService file to handle file encryption
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\user_management\Service;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class FileEncryptionService {

  public function __construct()
  {
  }

	public static function zipDirectory($rootPath , $type)
	{
		if($type == 'onboarding'){
			$info = drupal_realpath($rootPath .'/info.json');
			$onboarding = drupal_realpath($rootPath .'/onboarding-data.json');
			\Drupal::logger('zipDirectory')->notice('zip -j '. $rootPath .'.zip ' . $info .' '. $onboarding);
			exec('zip -j '. $rootPath .'.zip ' . $info .' '. $onboarding);			
		}else{
			$info = drupal_realpath($rootPath .'/info.json');
			$checkin = drupal_realpath($rootPath .'/checkin-data.json');
			\Drupal::logger('zipDirectory')->notice('zip '. $rootPath .'.zip '. $info .' '. $checkin);			
			exec('zip -j '. $rootPath .'.zip '. $info .' '. $checkin);	
		}
		
        self::encryptBundleArchive($rootPath .'.zip', $rootPath .'-1.zip');		
	}


  private function encryptBundleArchive($inFilePath, $outFilePath)
  {
    $CERT_DIRECTORY = drupal_realpath(drupal_get_path('module', 'user_management'). '/src/config/jnjstudy.cert');
    exec('openssl cms -encrypt -aes256 -binary -in ' . $inFilePath . ' -out ' . $outFilePath . ' -outform DER ' . $CERT_DIRECTORY);
  }

}