var Drupal = Drupal || {};

(function ($, Drupal) {

  Drupal.behaviors.brigtcove_GA = {
    attach: function (context) {
      if (context.nodeName !== '#document') {
        return;
      }
      var data_player = drupalSettings.brightcove_GA.data_player;
      var data_video_id = drupalSettings.brightcove_GA.data_video_id;
      var playlist_id = drupalSettings.brightcove_GA.data_playlist_id;
      var myPlayer;

      if (typeof videojs == 'function') {
        videojs(data_player).ready(function () {
          myPlayer = this;
          if (playlist_id) {
            myPlayer.catalog.getPlaylist(playlist_id, function (error, playlist) {
              myPlayer.catalog.load(playlist);
            });
          }
        });

        videojs(data_player).one('loadedmetadata', function () {
          myPlayer = this;
          var category;
          var action = 'click';
          var label;
          // duration is rounded down to nearest second, video can be milliseconds longer
          var duration = myPlayer.duration();
          var currentTime;
          var videoTitle = myPlayer.mediainfo.name;
          var nextPercentile = 0;

          function getVideoLocation() {
            category = (window.location.pathname.split('/'))[1];
            if (category === '') {
              category = 'homepage';
            }
          }

          getVideoLocation();
          // send onClick event when play is clicked for video
          myPlayer.on('play', function () {
            currentTime = myPlayer.currentTime();
            label = 'video_play_' + data_video_id + '_' + videoTitle + '_' + duration + '_' + currentTime;
            ga('send', 'event', category, action, label);
          });

          // send onClick event when pause is clicked for video
          myPlayer.on('pause', function () {
            if (!myPlayer.ended()) {
              currentTime = myPlayer.currentTime();
              label = 'video_pause_' + data_video_id + '_' + videoTitle + '_' + duration + '_' + currentTime;
              ga('send', 'event', category, action, label);
            }
          });

          // send onClick event when video is seeked
          myPlayer.on('seeked', function () {
            currentTime = myPlayer.currentTime();
            var percentWatched = Math.round(currentTime / duration * 100);
            nextPercentile = Math.ceil(percentWatched / 10) * 10;
            label = 'video_force_elapse_' + data_video_id + '_' + videoTitle + '_' + duration + '_' + currentTime;
            ga('send', 'event', category, action, label);
          });

          // send onClick event when fullscreen is clicked for video
          myPlayer.on('fullscreenchange', function () {
            if (myPlayer.isFullscreen()) {
              currentTime = myPlayer.currentTime();
              label = 'video_fullscreen_true_' + data_video_id + '_' + videoTitle + '_' + duration + '_' + currentTime;
            } else {
              label = 'video_fullscreen_false_' + data_video_id + '_ ' + videoTitle + '_' + duration + '_' + currentTime;
            }
            ga('send', 'event', category, action, label);
          });

          // send onClick event when video automatically elapses
          myPlayer.on('timeupdate', function () {
            currentTime = Math.floor(myPlayer.currentTime());
            var percent = currentTime / duration * 100;
            if (percent > nextPercentile) {
              label = 'video_auto_elapse_' + data_video_id + '_ ' + videoTitle + '_' + duration + '_' + currentTime + '_' + nextPercentile + '%';
              ga('send', 'event', category, 'time_elapse', label);
              nextPercentile += 10;
            }
          });

          myPlayer.on('ended', function () {
            nextPercentile = 0;
            label = 'video_auto_elapse_' + data_video_id + '_ ' + videoTitle + '_' + duration + '_' + currentTime + '_' + '100%';
            ga('send', 'event', category, 'time_elapse', label);
          });
        });
      }
    }
  };
}(jQuery, Drupal));

