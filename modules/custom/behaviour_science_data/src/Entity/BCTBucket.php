<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the BCT Bucket entity.
 *
 * @ingroup behaviour_science_data
 *
 * @ContentEntityType(
 *   id = "bct_bucket",
 *   label = @Translation("BCT Bucket"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\behaviour_science_data\BCTBucketListBuilder",
 *     "views_data" = "Drupal\behaviour_science_data\Entity\BCTBucketViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\behaviour_science_data\Form\BCTBucketForm",
 *       "add" = "Drupal\behaviour_science_data\Form\BCTBucketForm",
 *       "edit" = "Drupal\behaviour_science_data\Form\BCTBucketForm",
 *       "delete" = "Drupal\behaviour_science_data\Form\BCTBucketDeleteForm",
 *     },
 *     "access" = "Drupal\behaviour_science_data\BCTBucketAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\behaviour_science_data\BCTBucketHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "bct_bucket",
 *   admin_permission = "administer bct bucket entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/bct_bucket/{bct_bucket}",
 *     "add-form" = "/admin/structure/bct_bucket/add",
 *     "edit-form" = "/admin/structure/bct_bucket/{bct_bucket}/edit",
 *     "delete-form" = "/admin/structure/bct_bucket/{bct_bucket}/delete",
 *     "collection" = "/admin/structure/bct_bucket",
 *   },
 *   field_ui_base_route = "bct_bucket.settings"
 * )
 */
class BCTBucket extends ContentEntityBase implements BCTBucketInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the BCT Bucket entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the BCT Bucket entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    //New Fields
    $fields['bct_Bucket'] = BaseFieldDefinition::create('float')
      ->setLabel(t('BCT Bucket'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'float',
        'weight' => 2,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'float',
        'weight' => 2,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['content_chain_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Content Chain Id'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default')
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 3,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 3,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['link_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Content Chain Link Id'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default')
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $option = array(
      'Active' => 'Active',
      'Inactive' => 'InActive',
    );

    $fields['bct_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('BCT Status'))
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' => $option
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'weight' => 5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    //End Fields

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the BCT Bucket is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
