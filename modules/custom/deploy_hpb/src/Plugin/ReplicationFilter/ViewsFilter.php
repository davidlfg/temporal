<?php

namespace Drupal\deploy_hpb\Plugin\ReplicationFilter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\replication\Plugin\ReplicationFilter\ReplicationFilterBase;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Provides a filter based on a view.
 *
 * Supported parameters:
 *   view_id: The ID of the view used to filter the entities
 *
 * @ReplicationFilter(
 *   id = "views",
 *   label = @Translation("Filter by a View"),
 *   description = @Translation("Replicate only entities that belong to a given view.")
 * )
 */
class ViewsFilter extends ReplicationFilterBase {

  /**
   * {@inheritdoc}
   */
  public function filter(EntityInterface $entity, ParameterBag $parameters) {
    if ($parameters->has('view_id')) {
      $view_id = $parameters->get('view_id');
      $results = views_get_view_result($view_id, null, $entity->id());
      foreach ($results as $result) {
        if (!empty($result->_entity) && $result->_entity->uuid() == $entity->uuid()) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
