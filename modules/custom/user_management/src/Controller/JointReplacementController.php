<?php
/**
 * JointReplacementController file to get Joint Replacement Type
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\user_management\Controller;

Use \Drupal\taxonomy\Entity\Vocabulary;
Use Symfony\Component\HttpFoundation\JsonResponse;
Use Drupal\user_management\Service\getTaxonomyTerm;


/**
 * Class JointReplacementController
 * @package Drupal\user_management\Controller
 */
class JointReplacementController {
  /**
   * Function to get different joint replacement type
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getJointReplacementType() {
    $taxonomyTerm = new getTaxonomyTerm();
    $application = $taxonomyTerm->listTaxonomyTerm('application');
    $result = [];
    $i = 0;
    foreach ($application as $key => $value) {
      $result[$i]['jointId'] = $key;
      $result[$i]['jointName'] = $value;
      $i++;
    }
    return new JsonResponse($result, 200);
  }
}