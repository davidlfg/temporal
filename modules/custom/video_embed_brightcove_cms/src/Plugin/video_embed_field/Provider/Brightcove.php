<?php

/**
 * @file
 * Contains \Drupal\video_embed_brightcove\Plugin\video_embed_field\Provider\Brightcove.
 */

namespace Drupal\video_embed_brightcove_cms\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * @VideoEmbedProvider(
 *   id = "brightcove",
 *   title = @Translation("Brightcove")
 * )
 */
class Brightcove extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $data_account = $this->getAccountId();
    $data_player = $this->getPlayerId();
    $data_embed = 'default';

    if ($this->isPlaylist()) {
      $data_playlist_id = $this->getPlaylistId();
      return array(
        '#theme' => 'brightcove_html5_player',
        '#width' => $width,
        '#height' => $height,
        '#autoplay' => $autoplay,
        '#data_account' => $data_account,
        '#data_player' => $data_player,
        '#data_embed' => $data_embed,
        '#data_playlist_id' => $data_playlist_id,
        '#attached' => [
          'library' => [
            'video_embed_brightcove_cms/brightcove_ga_analytics',
          ],
          'drupalSettings' => [
            'brightcove_GA' => ([
              'data_account' => $data_account,
              'data_player' => $data_player,
              'data_embed' => $data_embed,
              'data_playlist_id' => $data_playlist_id,
            ]),
          ]
        ]
      );
    } else {
      $data_video_id = $this->getVideoId();
      return array(
        '#theme' => 'brightcove_html5_player',
        '#width' => $width,
        '#height' => $height,
        '#autoplay' => $autoplay,
        '#data_account' => $data_account,
        '#data_player' => $data_player,
        '#data_embed' => $data_embed,
        '#data_video_id' => $data_video_id,
        '#attached' => [
          'library' => [
            'video_embed_brightcove_cms/brightcove_ga_analytics',
          ],
          'drupalSettings' => [
            'brightcove_GA' => ([
              'data_account' => $data_account,
              'data_player' => $data_player,
              'data_embed' => $data_embed,
              'data_video_id' => $data_video_id,
            ]),
          ]
        ]
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $br = \Drupal::service('video_embed_brightcove_cms.brightcove_reader');
    return $br->getVideoImage($this->getVideoId());
  }

  /**
   * Get the player ID from the input URL.
   *
   * @return string
   *   The video player ID.
   */
  protected function getPlayerId() {
    return static::getUrlMetadata($this->getInput(), 'player');
  }

  /**
   * Get the account ID from the input URL.
   *
   * @return string
   *   The video account ID.
   */
  protected function getAccountId() {
    return static::getUrlMetadata($this->getInput(), 'account');
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    return static::getUrlMetadata($input, 'id');
  }

  /**
   * Checks if the URL given is for a playlist.
   *
   * @return bool
   *   True if URL is for playlist, else false.
   */
  public function isPlaylist() {
    return strpos($this->getInput(), 'playlistId');
  }

  /**
   * Get the playlist ID from the input URL.
   *
   * @return bool|string
   *   False if no id found, else the playlist id.
   */
  public function getPlaylistId() {
    return static::getUrlMetadata($this->getInput(), 'id');
  }

  /**
   * Extract metadata from the input URL.
   *
   * @param string $metadata
   *   The metadata matching the regex capture group to get from the URL
   *
   * @return string|bool
   *   The metadata or FALSE on failure.
   */
  protected static function getUrlMetadata($input, $metadata) {
    $matches = [];
    if (strpos($input, 'playlist')) {
      preg_match('/^(http:|https:)?\/\/players\.brightcove\.net\/(?<account>[0-9]*)\/(?<player>[a-zA-Z0-9]*)_default\/index\.html\?playlistId\=(?<id>[0-9]*)?$/', $input, $matches);
    } else {
      preg_match('/^(http:|https:)?\/\/players\.brightcove\.net\/(?<account>[0-9]*)\/(?<player>[a-zA-Z0-9]*)_default\/index\.html\?videoId\=(?<id>[0-9]*)?$/', $input, $matches);
    }
    return isset($matches[$metadata]) ? $matches[$metadata] : FALSE;
  }

}
