<?php

namespace Drupal\contentchain\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentChainConfigForm.
 *
 * @package Drupal\contentchain\Form
 */
class ContentChainConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'contentchain.ContentChainConfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_chain_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contentchain.ContentChainConfig');

    $form['onboarding'] = [
      '#type' => 'details',
      '#title' => $this->t('Patient Onboarding / Re-Onboarding '),
      '#open' => FALSE,
    ];
    //Knees
    $form['onboarding']['knees'] = [
      '#type' => 'details',
      '#title' => $this->t('Knees'),
      '#open' => FALSE,
    ];

    $form['onboarding']['knees']['knees_onboardingId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Onboarding Content chain Id'),
      '#description' => $this->t('Coma Separated content id for separate slides. Example :1,2,3'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('knees_onboardingId'),
    ];
    $form['onboarding']['knees']['knees_onboarding_feedbackId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Onboarding Content Feedback chain Id'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('knees_onboarding_feedbackId'),
    ];
    $form['onboarding']['knees']['knees_reonboardingId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reboarding Content chain Id'),
      '#description' => $this->t('Coma Separated content id for separate slides. Example :1,2,3'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('knees_reonboardingId'),
    ];
    $form['onboarding']['knees']['knees_accountId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Account page Content Chain Id'),
      '#description' => $this->t('Coma Separated content id for separate slides. Example :1,2,3'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('knees_accountId'),
    ];

    //Hips
    $form['onboarding']['hips'] = [
      '#type' => 'details',
      '#title' => $this->t('Hips'),
      '#open' => FALSE,
    ];

    $form['onboarding']['hips']['hips_onboardingId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Onboarding Content chain Id'),
      '#description' => $this->t('Coma Separated content id for separate slides. Example :1,2,3'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('hips_onboardingId'),
    ];
    $form['onboarding']['hips']['hips_onboarding_feedbackId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Onboarding Content Feedback chain Id'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('hips_onboarding_feedbackId'),
    ];
    $form['onboarding']['hips']['hips_reonboardingId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reboarding Content chain Id'),
      '#description' => $this->t('Coma Separated content id for separate slides. Example :1,2,3'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('hips_reonboardingId'),
    ];
    $form['onboarding']['hips']['hips_accountId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Account page Content Chain Id'),
      '#description' => $this->t('Coma Separated content id for separate slides. Example :1,2,3'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('hips_accountId'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('contentchain.ContentChainConfig')
      ->set('knees_onboardingId', $form_state->getValue('knees_onboardingId'))
      ->set('knees_onboarding_feedbackId', $form_state->getValue('knees_onboarding_feedbackId'))
      ->set('knees_reonboardingId', $form_state->getValue('knees_reonboardingId'))
      ->set('knees_accountId', $form_state->getValue('knees_accountId'))
      ->set('hips_onboardingId', $form_state->getValue('hips_onboardingId'))
      ->set('hips_onboarding_feedbackId', $form_state->getValue('hips_onboarding_feedbackId'))
      ->set('hips_reonboardingId', $form_state->getValue('hips_reonboardingId'))
      ->set('hips_accountId', $form_state->getValue('hips_accountId'))
      ->save();
  }

}
