<?php
/**
 * FileFields file to get file's metadata used in content cahin
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Service;


class FileFields {
  /*
   * function to retrieve files array
   */
  public function getFilesFields($file, $items, $i, $type, $fid = NULL) {
    global $base_url;
    $fileInfoArray = explode('?', $file);
    $fileBasename = basename($fileInfoArray[0]);
    $fileArray = explode('.', $fileBasename);
    if ($type == 'backgroundImage') {
      $items['backgroundImage']['url'] = $fileInfoArray[0];
      $items['backgroundImage']['fileName'] = $fileArray[0];
      $items['backgroundImage']['format'] = trim($fileArray[1]);
      if ($fid != NULL) {
        $items['backgroundImage']['fileId'] = $fid;
      }
    }
    else {
      if (preg_match('#^http#', $fileInfoArray[0]) === 1) {
        $url = $fileInfoArray[0];
      }
      else {
        $url = $base_url . $fileInfoArray[0];
      }
      $items['contentElements'][$i][$type]['url'] = $url;
      $items['contentElements'][$i][$type]['fileName'] = $fileArray[0];
      $items['contentElements'][$i][$type]['format'] = $fileArray[1];
      if ($fid != NULL) {
        $items['contentElements'][$i][$type]['fileId'] = $fid;
      }
    }
    return $items;
  }

}