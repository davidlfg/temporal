<?php
/**
 * Includes some logic related to PromptAction Node Entity
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\disruptive_checkin\Entity;

use Drupal\node\Entity\Node;
use Drupal\Component\Serialization\Json;

class PromptAction {
  protected $node; // Node related action
  
  /**
   * @param Node $node
   * @throws Exception if the node type is not prompt_action
   */
  public function __construct(Node $node)
  {
    if ($node->getType() != 'prompt_action') {
      throw new \Exception('PromptAction Constructor expects a node from type "prompt_action".');
    }
    $this->node = $node;
  }

  /**
   * Creates an instance with the node id
   * 
   * @param int $nid
   * @return \self
   */
  public static function createFromNid($nid)
  {
    if (!$nid || !$node = Node::load($nid)) {
      throw new \Exception('PromptAction Constructor expects a node from type "prompt_action".');
    }
    return new self($node);
  }
  
  /**
   * Exports the Action as Json
   * @return string
   */
  public function toJson()
  {
    return Json::encode($this->toArray());
  }
  
  /**
   * Exports the Action as array
   * @return array
   */
  public function toArray()
  {
    return array(
        'id' => (int)$this->node->nid->value,
        'title' => $this->node->getTitle(),
        'optionType' => (string)$this->getOptionType(),
        'options' => $this->getOptions(),
    ) + ($this->getType() == 'Slider' ? array(
        'sliderIncrement' => (float)$this->node->field_slider_increment->value,
        'sliderMaximum' => (float)$this->node->field_slider_maximum->value,
        'sliderMinimum' => (float)$this->node->field_slider_minimum->value
    ) : array());
  }
  
  /**
   * Translates the option type
   * @return string
   */
  public function getOptionType()
  {
    return t($this->getType());
  }
  
  /**
   * Gets the field names for the options type
   * @return array
   */
  protected function getFieldsByType()
  {
    switch ($this->getType()) {
      case 'button':
        return array(
            'main' => 'field_buttons',
            'label' => 'field_button_label',
            'value' => 'field_button_value',
            'feedback' => 'field_button_feedback_new'
        );
        
      case 'checkbox':
        return array(
            'main' => 'field_checkboxes',
            'label' => 'field_checkbox_label',
            'value' => 'field_checkbox_value',
            'feedback' => 'field_checkbox_feedback_new'
        );

      case 'Slider':
        return array(
            'main' => 'field_slider',
            'label' => 'field_slide_label',
            'feedback' => 'field_slide_feedback_new'
        );
        
      case 'freeFormText':
        return array(
            'main' => 'field_free_form_text',
            'label' => 'field_free_form_text_label',
            'value' => 'field_free_form_text_value',
            'feedback' => 'field_freeform_text_feedback_new'
        );
        
      default:
        return NULL;
    }
  }
  
  /**
   * Gets the actual options type
   * @return string
   */
  protected function getType()
  {
    if (count($this->node->field_buttons)) {
      return 'button';
    }
    elseif (count($this->node->field_checkboxes)) {
      return 'checkbox';
    }
    elseif (count($this->node->field_slider)) {
      return 'Slider';
    }
    elseif (count($this->node->field_free_form_text)) {
      return 'freeFormText';
    }
    else {
      \Drupal::logger('Disruptive Checkins')->warning('Action ' .
              $this->node->nid->value . ' doesn\'t have options.');
    }
  }
  
  /**
   * Gets the options as an array
   * @return array
   */
  public function getOptions()
  {
    $options = array();
    if (!$field = $this->getFieldsByType()) {
      return $options;
    }
    foreach ($this->node->{$field['main']} as $order => $value) {
      $option = \Drupal\field_collection\Entity\FieldCollectionItem::load($value->value);
      $options[] = array(
          'order' => (int)$order,
          'label' => $option->{$field['label']}->value,
          'feedback' => $option->{$field['feedback']}->value
      ) + (isset($field['value']) ? array(
          'value' => $option->{$field['value']}->value,
      ) : array());
    }
    return $options;
  }
}
