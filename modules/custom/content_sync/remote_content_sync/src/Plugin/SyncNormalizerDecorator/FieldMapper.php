<?php

namespace Drupal\remote_content_sync\Plugin\SyncNormalizerDecorator;

use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides a decorator to map field names.
 *
 * @SyncNormalizerDecorator(
 *   id = "field_mapper",
 *   name = @Translation("Field Mapper"),
 * )
 */
class FieldMapper extends SyncNormalizerDecoratorBase {

  /**
   * Implements decorator for normalization.
   */
  public function decorateNormalization(array &$normalized_entity, ContentEntityInterface $entity, $format, array $context = []) {
    if (!empty($context['field_map'])) {
      foreach ($context['field_map'] as $mapping) {
        if (!empty($normalized_entity[$mapping['local_name']])) {
          $normalized_entity[$mapping['remote_name']] = $normalized_entity[$mapping['local_name']];
        }
      }
    }
  }

  /**
   * Implements decorator for denormalization.
   */
  public function decorateDenormalization(array &$normalized_entity, $type, $format, array $context = []) {
    if (!empty($context['field_map'])) {
      foreach ($context['field_map'] as $mapping) {
        if (!empty($normalized_entity[$mapping['remote_name']])) {
          $normalized_entity[$mapping['local_name']] = $normalized_entity[$mapping['remote_name']];
        }
      }
    }
  }

}
