<?php

namespace Drupal\deploy_hpb\Normalizer;

use Drupal\replication\Normalizer\ContentEntityNormalizer as ContentEntityNormalizerBase;

class ContentEntityNormalizer extends ContentEntityNormalizerBase {

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = array()) {
    $data = parent::normalize($entity, $format, $context);
    if (!empty($data) && !empty($entity->id())) {
      $entity_languages = $entity->getTranslationLanguages();
      foreach ($entity_languages as $entity_language) {
        try {
          /** @var \Drupal\Core\Url $url */
          $url = $entity->toUrl();
          if (!empty($url)) {
            $system_path = '/' . $url->getInternalPath();
            $langcode = $entity_language->getId();
            $path_alias = \Drupal::service('path.alias_manager')
                                 ->getAliasByPath($system_path, $langcode);
            if (!empty($path_alias) && $path_alias != $system_path) {
              $data[$entity_language->getId()]['path'] = $path_alias;
            }
          }
        } catch (UndefinedLinkTemplateException $e) {
          \Drupal::logger('deploy_hpb')->error($e->getMessage());
        }
      }
    }
    return $data;
  }

}
