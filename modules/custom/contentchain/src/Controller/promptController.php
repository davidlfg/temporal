<?php
/**
 * promptController file
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\user\Entity\User;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\contentchain\Service\ChainWebForms;

/**
 * Class promptController
 * @package Drupal\contentchain\Controller
 */
class promptController {
  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Exception
   */
  public function prompt_action(Request $request) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    $uuid = $user['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $account = \Drupal::service('user_management.user')
      ->getUserProfile($uuid);
    $userId = $headlessAccount->id();

    $content = json_decode($request->getContent(), TRUE);
    //User time stamp
    $requestDateTime = isset($headers['UserTimeStamp']) ? $headers['UserTimeStamp'] : \date('m-d-Y H:i:s');
    $date = DateTimePlus::createFromFormat('m-d-Y H:i:s', $requestDateTime);
    $datetime = $date->format('Y-m-d H:i:s');

    $questionTaggedToProfile = isset($content['questionTaggedToProfile']) ? $content['questionTaggedToProfile'] : 0;
    $chainId = isset($content['chainId']) ? $content['chainId'] : 0;
    $linkId = $content['linkId'];
    $actionId = $content['actionId'];
    $selected = serialize($content['selected']);

    if (count($content['selected']) == 1) {
      foreach ($content['selected'] as $key => $value) {
        $yes = $value;
      }
    }

    $surgeryDate = $user['field_surgery_date'];
    $registrationDate = $user['created'];
    $items = [];
    if ($userId && $actionId) {
      $responses_id = \Drupal::database()->select('responses', 'rs')
        ->fields('rs', array('response'))
        ->condition('rs.uid', $userId)
        ->condition('rs.link_id', $actionId)
        ->execute()
        ->fetchField();
      if ($responses_id != FALSE) {
        $result = \Drupal::database()->update('responses')
          ->fields(array(
            'key_value_array' => $selected,
            'timestamp' => $datetime
          ))
          ->condition('link_id', $actionId, '=')
          ->condition('uid', $userId, '=')
          ->execute();
      }
      else {
        //Create a new action
        $fields = array(
          'uid' => $userId,
          'link_id' => $actionId,
          'key_value_array' => $selected,
          'timestamp' => $datetime
        );
        $result = \Drupal::database()->insert('responses')
          ->fields($fields)
          ->execute();
      }
      //If prompt contain webForm
      if (isset($content['formId'])) {
        $chainForm = new ChainWebForms();
        $date = DateTimePlus::createFromFormat('m-d-Y', $surgeryDate);
        $content['selected']['ReferenceDate'] = $date->format('m/d/Y');
        $response = $chainForm->submitChainWebForms($content);
        if ($response) {
          if (isset($response['ErrorMessage'])) {
            \Drupal::logger('CareTakerSubscription')
              ->notice($response['ErrorMessage']);
            return new JsonResponse(array('errorMessage' => $response['ErrorMessage']), 400);
          }
          else {
            \Drupal::logger('CareTakerSubscription')
              ->notice($response['Message']);
          }
        }

      }
      //End form submit

      //response 	 if action item
      if ($result) {
        $view = views_embed_view('chain_link_view', 'rest_export_1', $linkId);
        $view_result1 = \Drupal::service('renderer')->render($view);
        $chain_json1 = (string) $view_result1;
        $chain_array1 = json_decode($chain_json1);
        if (count($chain_array1) == 0) {
          //If not an action item return success
          $message = array(
            'Message' => 'Success'
          );
          return new JsonResponse($message, 200);
        }
        $type_of_prompt = $chain_array1['0']->field_type_of_prompt;
        if (isset($yes) == 'Yes') {
          $optionCheck = $yes;
        }
        else {
          $optionCheck = 'No';
        }
        if ($type_of_prompt == 'action item' AND $optionCheck == 'Yes') {
          //User can create multiple action item from a potential action item( 1 to n combination)
          $actionItemId = $chain_array1['0']->field_action_item_id;
          $items['actionItem']['type'] = isset($type_of_prompt) ? t('Action item') : '';
          $view = views_embed_view('action_item', 'rest_export_1', $chain_array1['0']->field_action_item_id);
          $view_result2 = \Drupal::service('renderer')->render($view);
          $chain_json2 = (string) $view_result2;
          $chain_array3 = json_decode($chain_json2);
          $contains_token = $chain_array3['0']->field_contains_token['0']->value;
          $title = htmlspecialchars_decode($chain_array3['0']->field_display_title['0']->value, ENT_QUOTES);
          //Start: If any token in action item title or feedback. Replace with actual text
          if ($contains_token) {
            $pattern = "/\[(.*?)]/";
            preg_match_all($pattern, $title, $title_match);
            if ($title_match[1]) {
              $ul = new updatelinkController();
              $title = $ul->tokenReplace($title, $userId);
            }
          }
          //End token replace code
          $items['actionItem']['title'] = $title;
          $referenceDate = $chain_array3['0']->field_reference_date['0']->value;
          $daysFromreferenceDate = (int) $chain_array3['0']->field_days_from_reference_date['0']->value;
          switch ($referenceDate) {
            case 'Registration Date':
              $notificationDate = date("Y-m-d 12:00:00", strtotime("+" . $daysFromreferenceDate . " days", strtotime($registrationDate)));
              break;

            case 'Current Date':
              $notificationDate = date("Y-m-d 12:00:00", strtotime("+" . $daysFromreferenceDate . " days", strtotime($datetime)));
              break;

            case 'Surgery Date':
              $notificationDate = date("Y-m-d 12:00:00", strtotime("+" . $daysFromreferenceDate . " days", strtotime($surgeryDate)));
              break;

            default:
              $notificationDate = date("Y-m-d 12:00:00", strtotime("+" . $daysFromreferenceDate . " days", strtotime($datetime)));
          }
          $dateFormat = DateTimePlus::createFromFormat('Y-m-d H:i:s', $notificationDate);
          $appNotificationDate = $dateFormat->format('m-d-Y H:i:s');

          $items['actionItem']['notificationDate'] = $appNotificationDate;
          $status = isset($chain_array3['0']->status['0']->value) ? $chain_array3['0']->status['0']->value : 'Active';
          $items['actionItem']['status'] = isset($chain_array3['0']->status['0']->value) ? 0 : 0;
          $items['actionItem']['weight'] = 0;
          $items['actionItem']['completed'] = 0;
          $items['actionItem']['userId'] = (int) $userId;
          //$items['actionItem']['whenCompleted'] = "0000-00-00 00:00:00";
          $items['actionItem']['chainId'] = $chainId;
          $items['actionItem']['is_recurrence'] = isset($chain_array3['0']->field_is_recurrence['0']->value) ? (int) $chain_array3['0']->field_is_recurrence['0']->value : 0;
          $items['actionItem']['recurrence_interval'] = isset($chain_array3['0']->field_recurrence_interval['0']->value) ? $chain_array3['0']->field_recurrence_interval['0']->value : 'None';
          $items['actionItem']['walkingActionItem'] = isset($chain_array3['0']->field_walking_action_item['0']->value) ? (int) $chain_array3['0']->field_walking_action_item['0']->value : 0;
          $fields = array(
            'uid' => $userId,
            'display_title' => $title,
            'status' => $status,
            'weight' => 0,
            'creation_date' => $datetime,
            'reference_date' => $referenceDate,
            'daysfromreference_date' => $daysFromreferenceDate,
            'notification_date' => $notificationDate,
            'completed' => 0,
            'last_update' => $datetime,
            'chainId' => $chainId,
            'linkId' => $actionItemId,
            'is_recurrence' => isset($chain_array3['0']->field_is_recurrence['0']->value) ? (int) $chain_array3['0']->field_is_recurrence['0']->value : 0,
            'recurrence_interval' => isset($chain_array3['0']->field_recurrence_interval['0']->value) ? $chain_array3['0']->field_recurrence_interval['0']->value : 'None'
          );
          $actionItemtid = \Drupal::database()->insert('action_items')
            ->fields($fields)
            ->execute();
          $items['actionItem']['id'] = (int) $actionItemtid;

          //If question tagged to profile , save to profile
          if ($questionTaggedToProfile) {
            unset($content['selected']['ReferenceDate']);
            foreach ($content['selected'] as $key => $value) {
              $formData[$key] = isset($value) ? $value : $account[$key];
              $hsAccountUpdated = \Drupal::service('apigee.connect')
                ->update_health_store_user_profile($formData, $uuid);
            }
            //Updating User profile fields response
          }

          return new JsonResponse($items, 200);
        }

        //If question tagged to profile , save to profile
        if ($questionTaggedToProfile) {
          unset($content['selected']['ReferenceDate']);
          foreach ($content['selected'] as $key => $value) {
            $formData[$key] = isset($value) ? $value : $account[$key];
            $hsAccountUpdated = \Drupal::service('apigee.connect')
              ->update_health_store_user_profile($formData, $uuid);
          }
          //Updating User profile fields response
        }

        //If  not action item
        $message = array(
          'Message' => 'Success'
        );
        return new JsonResponse($message, 200);
      }
      else {
        $error = array(
          'errorMessage' => 'Error'
        );
        return new JsonResponse($error, 400);
      }
    }
    else {
      $response = array(
        'errorMessage' => 'Error',
      );
      return new JsonResponse($response, 400);
    }
  }
}