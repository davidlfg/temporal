<?php

namespace Drupal\video_embed_brightcove_cms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VideoEmbedBrightcoveCmsConfigForm.
 *
 * @package Drupal\video_embed_brightcove_cms\Form
 */
class VideoEmbedBrightcoveCmsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'video_embed_brightcove_cms.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_embed_brightcove_cms_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('video_embed_brightcove_cms.settings');
    $form['api_client'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Brightcove API Client'),
    ];
    $form['api_client']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brightcove API Client ID'),
      '#description' => $this->t('The Client ID of the Brightcove API Authentication credentials, available <a href=":link" target="_blank">here</a>.', [':link' => 'https://studio.brightcove.com/products/videocloud/admin/oauthsettings']),
      '#maxlength' => 255,
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];
    $form['api_client']['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brightcove API Secret Key'),
      '#description' => $this->t('The Secret Key associated with the Client ID above, only visible once when Registering New Application.'),
      '#maxlength' => 255,
      '#default_value' => $config->get('secret_key'),
      '#required' => TRUE,
    ];
    $form['api_client']['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brightcove Account ID'),
      '#description' => $this->t('The number of one of the Brightcove accounts "selected for authorization" with the API Client above.'),
      '#maxlength' => 255,
      '#default_value' => $config->get('account_id'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->cleanValues()->getValues();
    $config = $this->config('video_embed_brightcove_cms.settings');

    foreach ($values as $key => $value) {
      $config->set($key, $form_state->getValue($key))->save();
    }

    $config->save();
  }

}
