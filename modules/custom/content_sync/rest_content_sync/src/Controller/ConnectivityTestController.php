<?php

namespace Drupal\rest_content_sync\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Builds an example page.
 */
class ConnectivityTestController extends ControllerBase {

  /**
   * Route callback for connection and access test.
   */
  public function testConnection() {
    return new JsonResponse((object) []);
  }

  /**
   * Route callback for connection and access test.
   */
  public function testConnectionAuth() {
    return new JsonResponse((object) []);
  }

  /**
   * Checks access for a specific request.
   */
  public function access(AccountInterface $account) {
    return AccessResult::allowedIf($account->isAuthenticated());
  }

}
