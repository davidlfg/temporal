<?php

namespace Drupal\content_sync;

/**
 * Content sync scanner class.
 */
class ContentSyncScanner {

  protected $supportedExtensions;

  /**
   * ContentSyncScanner constructor.
   *
   * @param array $supported_extensions
   *   The supported extensions.
   */
  public function __construct($supported_extensions = array('json')) {
    $this->supportedExtensions = $supported_extensions;
  }

  /**
   * Returns a list of file objects.
   *
   * @param string $directory
   *   Absolute path to the directory to search.
   *
   * @return array
   *   List of stdClass objects with name and uri properties.
   */
  public function scan($directory) {
    // Use Unix paths regardless of platform, skip dot directories, follow
    // symlinks (to allow extensions to be linked from elsewhere), and return
    // the RecursiveDirectoryIterator instance to have access to getSubPath(),
    // since SplFileInfo does not support relative paths.
    $flags = \FilesystemIterator::UNIX_PATHS;
    $flags |= \FilesystemIterator::SKIP_DOTS;
    $flags |= \FilesystemIterator::CURRENT_AS_SELF;
    $directory_iterator = new \RecursiveDirectoryIterator($directory, $flags);
    $iterator           = new \RecursiveIteratorIterator($directory_iterator);

    $files = array();
    foreach ($iterator as $fileinfo) {
      /* @var \SplFileInfo $fileinfo */

      // Skip directories and non-json files.
      if ($fileinfo->isDir() || !in_array($fileinfo->getExtension(), $this->supportedExtensions)) {
        continue;
      }

      $file = new \stdClass();
      $file->name = $fileinfo->getFilename();
      $file->uri = $fileinfo->getPathname();
      $files[$file->uri] = $file;
    }

    return $files;
  }

}
