<?php

namespace Drupal\basic_auth_site\EventSubscriber;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Component\Utility\Crypt;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class AuthenticationSubscriber.
 *
 * @package Drupal\basic_auth_site
 */
class AuthenticationSubscriber implements EventSubscriberInterface {


  /**
   * Constructor.
   */
  public function __construct(AuthenticationProviderInterface $authentication_provider, AccountProxyInterface $account_proxy, SessionManagerInterface $session_manager, ConfigFactoryInterface $config_factory) {
    $this->authenticationProvider = $authentication_provider;
    $this->filter = ($authentication_provider instanceof AuthenticationProviderFilterInterface) ? $authentication_provider : NULL;
    $this->challengeProvider = ($authentication_provider instanceof AuthenticationProviderChallengeInterface) ? $authentication_provider : NULL;
    $this->accountProxy = $account_proxy;
    $this->sessionManager = $session_manager;
    $this->configFactory = $config_factory;
  }

  public function onKernelRequestAuthenticateBefore(GetResponseEvent $event) {
    if ($event->getRequestType() === HttpKernelInterface::MASTER_REQUEST) {
      $credentials = $this->getCredentials();
      $request = $event->getRequest();
      if (!$this->authenticationProvider->applies($request)) {
        $response = new Response();
        $response->headers->add([
          'WWW-Authenticate' => 'Basic realm="Access restricted"',
        ]);
        $response->setStatusCode(401);
        $event->setResponse($response);
        return;
      }
    }
  }

  public function onKernelRequestAuthenticateAfter(GetResponseEvent $event) {
    if ($event->getRequestType() === HttpKernelInterface::MASTER_REQUEST) {
      $credentials = $this->getCredentials();
      if ($this->accountProxy->getAccount()->isAnonymous()) {
        if ($event->getRequest()->headers->has('PHP_AUTH_USER') && $event->getRequest()->headers->has('PHP_AUTH_PW')) {
          $input_user = $event->getRequest()->headers->get('PHP_AUTH_USER');
          $input_pass = $event->getRequest()->headers->get('PHP_AUTH_PW');
        }
        if (isset($input_user) && $input_user === $credentials['username'] && Crypt::hashEquals($credentials['password'], $input_pass)) {
          $event->getRequest()->headers->remove('PHP_AUTH_USER');
          $event->getRequest()->headers->remove('PHP_AUTH_PW');
          return;
        }
        else {
          $response = new Response();
          $response->headers->add([
            'WWW-Authenticate' => 'Basic realm="Access restricted"',
          ]);
          $response->setStatusCode(401);
          $event->setResponse($response);
          return;
        }

      }
      else {

        $event->getRequest()->getSession()->set('basic_auth_user', TRUE);
        $this->sessionManager->start();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {

    $events[KernelEvents::REQUEST][] = [
      'onKernelRequestAuthenticateBefore',
      301
    ];
    $events[KernelEvents::REQUEST][] = [
      'onKernelRequestAuthenticateAfter',
      299
    ];

    return $events;
  }

  protected function getCredentials() {
    $config = $this->configFactory->get('basic_auth_site.settings');
    return [
      'username' => $config->get('username'),
      'password' => $config->get('password'),
    ];
  }

}
