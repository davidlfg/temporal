<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for BCT Bucket entities.
 */
class BCTBucketViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['bct_bucket']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('BCT Bucket'),
      'help' => $this->t('The BCT Bucket ID.'),
    );

    return $data;
  }

}
