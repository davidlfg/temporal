<?php
/**
 * UserManagementForm file to handle custom configurations.
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\user_management\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Form with examples on how to use cron.
 */
class UserManagementForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_management';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('user_management.settings');

    $batch_notification_time = $config->get('batch_notification_time');
    $form['batch_notification_time'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Action Item Batch Notification Time'),
      '#default_value' => isset($batch_notification_time) ? $batch_notification_time : '',
      '#description' => $this->t('Action Item Batch Notification Time is set to : "<b>' . $batch_notification_time . '</b>". Batch notification will trigger at the specified time . We are not considering the date of the notification.'),
    ];

    //App Version configuration
    $form['app_config'] = array(
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Knees & Hips App Version Configuration'),
    );
    //Daily
    $form['app_configuration']['environment1'] = [
      '#type' => 'details',
      '#title' => $this->t('Environment #1'),
      '#open' => TRUE,
      '#group' => 'app_config',
    ];
    $dailyEnvironmentName = $config->get('dailyEnvironmentName');
    $form['app_configuration']['environment1']['dailyEnvironmentName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment Name'),
      '#default_value' => isset($dailyEnvironmentName) ? $dailyEnvironmentName : 'daily',
      '#description' => $this->t('Example :daily.'),
    ];
    $dailyVersion = $config->get('dailyVersion');
    $form['app_configuration']['environment1']['dailyVersion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latest Version'),
      '#default_value' => isset($dailyVersion) ? $dailyVersion : '1.0',
      '#description' => $this->t('Example :0.50.'),
    ];
    $dailyDownloadLink = $config->get('dailyDownloadLink');
    $form['app_configuration']['environment1']['dailyDownloadLink'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Message'),
      '#default_value' => isset($dailyDownloadLink['value']) ? $dailyDownloadLink['value'] : 'http://tsfr.io/rwbday',
      '#description' => $this->t('Example :http://tsfr.io/rwbday.'),
    ];
    //Dev
    $form['app_configuration']['environment2'] = [
      '#type' => 'details',
      '#title' => $this->t('Environment #2'),
      '#open' => FALSE,
      '#group' => 'app_config',
    ];
    $devEnvironmentName = $config->get('devEnvironmentName');
    $form['app_configuration']['environment2']['devEnvironmentName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment Name'),
      '#default_value' => isset($devEnvironmentName) ? $devEnvironmentName : 'dev',
      '#description' => $this->t('Example :dev.'),
    ];
    $devVersion = $config->get('devVersion');
    $form['app_configuration']['environment2']['devVersion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latest Version'),
      '#default_value' => isset($devVersion) ? $devVersion : '1.0',
      '#description' => $this->t('Example :0.50.'),
    ];
    $devDownloadLink = $config->get('devDownloadLink');
    $form['app_configuration']['environment2']['devDownloadLink'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Message'),
      '#default_value' => isset($devDownloadLink['vaue']) ? $devDownloadLink['value'] : 'http://tsfr.io/rwbday',
      '#description' => $this->t('Example :http://tsfr.io/rwbday.'),
    ];
//Stage
    $form['app_configuration']['environment3'] = [
      '#type' => 'details',
      '#title' => $this->t('Environment #3'),
      '#open' => FALSE,
      '#group' => 'app_config',
    ];
    $stageEnvironmentName = $config->get('stageEnvironmentName');
    $form['app_configuration']['environment3']['stageEnvironmentName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment Name'),
      '#default_value' => isset($stageEnvironmentName) ? $stageEnvironmentName : 'stage',
      '#description' => $this->t('Example :stage.'),
    ];
    $stageVersion = $config->get('stageVersion');
    $form['app_configuration']['environment3']['stageVersion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latest Version'),
      '#default_value' => isset($stageVersion) ? $stageVersion : '1.0',
      '#description' => $this->t('Example :0.50.'),
    ];
    $stageDownloadLink = $config->get('stageDownloadLink');
    $form['app_configuration']['environment3']['stageDownloadLink'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Message'),
      '#default_value' => isset($stageDownloadLink['value']) ? $stageDownloadLink['value'] : 'http://tsfr.io/rwbstg',
      '#description' => $this->t('Example :http://tsfr.io/rwbstg.'),
    ];
//Prod
    $form['app_configuration']['environment4'] = [
      '#type' => 'details',
      '#title' => $this->t('Environment #4'),
      '#open' => FALSE,
      '#group' => 'app_config',
    ];
    $prodEnvironmentName = $config->get('prodEnvironmentName');
    $form['app_configuration']['environment4']['prodEnvironmentName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment Name'),
      '#default_value' => isset($prodEnvironmentName) ? $prodEnvironmentName : '',
      '#description' => $this->t('Example :Prod.'),
    ];
    $prodVersion = $config->get('prodVersion');
    $form['app_configuration']['environment4']['prodVersion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latest Version'),
      '#default_value' => isset($prodVersion) ? $prodVersion : '',
      '#description' => $this->t('Example :0.50.'),
    ];
    $prodDownloadLink = $config->get('prodDownloadLink');
    $form['app_configuration']['environment4']['prodDownloadLink'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Message'),
      '#default_value' => isset($prodDownloadLink['value']) ? $prodDownloadLink['value'] : '',
      '#description' => $this->t('Example :'),
    ];
    //MailChimp configuration
    $form['mailChimp'] = array(
      '#type' => 'vertical_tabs',
      '#title' => $this->t('MailChimp Configuration'),
    );

    $url = Url::fromUri('https://us13.admin.mailchimp.com/account/api/');
    $link_options = array(
      'attributes' => array(
        'target' => array(
          '_blank',
        ),
      ),
    );
    $url->setOptions($link_options);
    $link1 = Link::fromTextAndUrl('Click here to reset apikey', $url);

    $url = Url::fromUri('https://us13.admin.mailchimp.com/lists/');
    $url->setOptions($link_options);
    $link2 = Link::fromTextAndUrl('Click here to reset ListId', $url);

    //Knees
    $form['knees_mailChimp_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Knees'),
      '#open' => TRUE,
      '#group' => 'mailChimp',
    ];
    $mailChimp_apiKey = $config->get('mailChimp_apiKey');
    $form['knees_mailChimp_configuration']['mailChimp_apiKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api Key (One API key per account)'),
      '#default_value' => isset($mailChimp_apiKey) ? $mailChimp_apiKey : 'd7219813c8c9b27e14b031e8d8b97bf6-us13',
      '#description' => $this->t('Example :d7219813c8c9b27e14b031e8d8b97bf6-us13. <br />' . $link1->toString()),
    ];
    $knees_mailChimp_listId = $config->get('knees_mailChimp_listId');
    $form['knees_mailChimp_configuration']['knees_mailChimp_listId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List Id'),
      '#default_value' => isset($knees_mailChimp_listId) ? $knees_mailChimp_listId : '3983d5fab4',
      '#description' => $this->t('Example :3983d5fab4. <br />' . $link2->toString()),
    ];
    //Hips
    $form['hips_mailChimp_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Knees'),
      '#open' => TRUE,
      '#group' => 'mailChimp',
    ];
    $hips_mailChimp_listId = $config->get('hips_mailChimp_listId');
    $form['hips_mailChimp_configuration']['hips_mailChimp_listId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List Id'),
      '#default_value' => isset($hips_mailChimp_listId) ? $hips_mailChimp_listId : '6d5ac3365e',
      '#description' => $this->t('Example :6d5ac3365e. <br />' . $link2->toString()),
    ];
//End  MailChimp configuration


    //Start : Collection Statement
    $form['collection_statement'] = [
      '#type' => 'details',
      '#title' => $this->t('Collection Statement'),
      '#open' => FALSE,
    ];
    $checkInStatement = $config->get('checkInStatement');
    $form['collection_statement']['checkInStatement'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Check-in Collection Statement Content Id'),
      '#default_value' => isset($checkInStatement) ? $checkInStatement : '2009,2010',
      '#description' => $this->t('Coma Separated content id for separate slides. Example :1,2,3'),
    ];
    //End

    $form['rwb_email'] = array(
      '#type' => 'vertical_tabs',
      '#title' => $this->t('RWB Emails'),
    );
    // These email tokens are shared for all settings, so just define
    // the list once to help ensure they stay in sync.
    $email_token_help = $this->t('Available variables are: [site:name], [site:url], [user:display-name], [user:account-name], [user:mail],
    [site:login-url], [site:url-brief], [user:edit-url], [user:one-time-login-url], [user:cancel-url].');
    //Knees
    $form['custom_email_no_approval_required'] = array(
      '#type' => 'details',
      '#title' => $this->t('Knees - Welcome (no approval required) email'),
      '#open' => $config->get('register') == USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL,
      '#description' => $this->t('Edit the email notifying the site administrator that there are new members awaiting administrative approval.') . ' ' . $email_token_help,
      '#group' => 'rwb_email',
    );
    $knees_register_no_approval_required_subject = $config->get('knees_register_no_approval_required_subject');
    $form['custom_email_no_approval_required']['knees_register_no_approval_required_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => isset($knees_register_no_approval_required_subject) ? $knees_register_no_approval_required_subject : 'Welcome to the Health Partner for Knees app!',
    ];
    $knees_register_no_approval_required_body_text = $config->get('knees_register_no_approval_required_body_text');
    $form['custom_email_no_approval_required']['knees_register_no_approval_required_body_text'] = array(
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Body'),
      '#default_value' => isset($knees_register_no_approval_required_body_text['value']) ? $knees_register_no_approval_required_body_text['value'] : '<style type="text/css">#outlook a { padding: 0; } body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0; } .ExternalClass { width: 100%; } .ExternalClass, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; } .ExternalClass p { line-height: inherit; } #body-layout { margin: 0; padding: 0; width: 100% !important; line-height: 100% !important; } table td { border-collapse: collapse; } table { border-collapse: collapse; }
</style>
<style type="text/css">@media only screen and (max-width: 599px) { div[class="block"] { width: 100% !important; max-width: none !important; } table[class="full-width"] { width: 100% !important; } td[class="mobile-padding"] { padding: 0 0 20px 0 !important; } td[class="normalize-height"] { height: auto !important; } table[class="center"], td[class="center"] { text-align: center !important; float: none !important; } table[class="left"], td[class="left"] { text-align: left !important; float: left !important; } table[class="right"], td[class="right"] { text-align: right !important; float: right !important; } table[class="remove"], tr[class="remove"], span[class="remove"] { display: none; } }
</style>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;" width="100%">
	<tbody>
		<tr>
			<td align="center" valign="top">
			<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0; background: #f7f7f7;">
				<tbody>
					<tr>
						<td align="center" valign="top" width="680">
						<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;background: #f7f7f7;" width="100%">
							<tbody>
								<tr>
									<td align="center" valign="top"><img alt="" src="http://www.careadvantage.jnj.com/sites/default/files/inline-images/email_logo.png" style="width: 100%; max-width: 100%;" />
									<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;" width="100%">
										<tbody>
											<tr>
												<td align="center" valign="top">
												<table align="center" border="0" cellpadding="0" cellspacing="20" style="margin: 0; padding-bottom: 0; background: #fff; border-bottom: 2px solid #ddd; border-top: 2px solid #ddd;" width="100%">
													<tbody>
														<tr>
															<td align="center" style="padding-left: 20px; padding-right: 20px;" valign="top">
															<table align="left" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 20px;">
																<tbody>
																	<tr>
																		<td align="left" style="color: #000000; font-family:Helvetica,Arial; font-size: 14px; line-height:
                                                                    24px; font-weight: 400;" valign="top" width="500">
																		<p>[user:field_first_name],</p>

																		<p>Thank you for creating an account with the Health Partner for Knees app, your resource for personalized support before and after knee replacement surgery. Each week, you’ll receive the most relevant information for each phase of your knee replacement journey — from four weeks before your surgery to 12 weeks after.&nbsp;We hope these interactive lessons help set you on your path for success!</p>

																		<p>&nbsp;</p>

																		<p>-- The Health Partner for Knees team</p>

																		<p>Technical problems? Suggestions for or improvements to our app? You can call us at 1-888-980-8947, 7 days a week, 24 hours a day.</p>

																		<p>If you have a medical question, contact your health care provider. If this is a medical emergency, call 911.</p>

																		<p>&nbsp;</p>
																		</td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>

												<table align="center" border="0" cellpadding="20" cellspacing="0" style="margin: 0; padding: 0;background: #f7f7f7;">
													<tbody>
														<tr>
															<td align="left" style="padding: 4%;" valign="top">
															<table align="left" border="0" cellpadding="0" cellspacing="0" style="margin: 0;">
																<tbody>
																	<tr>
																		<td align="left" style="line-height: 0px !important; padding-bottom: 3%; width:100%" valign="top"><img alt="jnj-logo" src="https://knees.thehealthpartner.com/sites/default/files/inline-images/Poweredby.png" style="width: 50%; max-width: 100%;" width="360" /></td>
																	</tr>
																</tbody>
															</table>

															<table align="left" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding:20px;">
																<tbody>
																	<tr>
																		<td align="left" style="color: #333; font: 14px Helvetica, sans-serif; line-height: 125%; font-weight: 400;" valign="top" width="350">Johnson &amp; Johnson Health and Wellness Solutions, Inc.<br />
																		One Johnson &amp; Johnson Plaza<br />
																		Atrium 7<br />
																		New Brunswick, New Jersey 08933 USA</td>
																	</tr>
																	<tr>
																		<td height="10" style="height: 31px; line-height: 31px;">&nbsp;</td>
																	</tr>
																	<tr>
																		<td align="left" style="color: #333; font: 14px Helvetica, sans-serif; line-height: 150%; font-weight: 400;" valign="top" width="400">©Johnson &amp; Johnson Health and Wellness Solutions, Inc. 2016 All rights reserved. Please review the sites\'s <a href="http://knees.thehealthpartner.com/privacy-policy">Privacy Policy</a> and <a href="http://knees.thehealthpartner.com/legal-notice">Legal Notice.</a></td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>

						<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;" width="100%">
							<tbody>
								<tr>
									<td height="20" style="height: 20px; line-height: 20px;">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
',
      '#rows' => 15,
    );
    //Hips
    $form['hips_custom_email_no_approval_required'] = array(
      '#type' => 'details',
      '#title' => $this->t('Hips - Welcome (no approval required) email'),
      '#open' => $config->get('register') == USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL,
      '#description' => $this->t('Edit the email notifying the site administrator that there are new members awaiting administrative approval.') . ' ' . $email_token_help,
      '#group' => 'rwb_email',
    );
    $hips_register_no_approval_required_subject = $config->get('hips_register_no_approval_required_subject');
    $form['hips_custom_email_no_approval_required']['hips_register_no_approval_required_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => isset($hips_register_no_approval_required_subject) ? $hips_register_no_approval_required_subject : 'Welcome to the Health Partner for Hips app!',
    ];
    $hips_register_no_approval_required_body_text = $config->get('hips_register_no_approval_required_body_text');
    $form['hips_custom_email_no_approval_required']['hips_register_no_approval_required_body_text'] = array(
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Body'),
      '#default_value' => isset($hips_register_no_approval_required_body_text['value']) ? $hips_register_no_approval_required_body_text['value'] : '<style type="text/css">#outlook a { padding: 0; } body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0; } .ExternalClass { width: 100%; } .ExternalClass, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; } .ExternalClass p { line-height: inherit; } #body-layout { margin: 0; padding: 0; width: 100% !important; line-height: 100% !important; } table td { border-collapse: collapse; } table { border-collapse: collapse; }
</style>
<style type="text/css">@media only screen and (max-width: 599px) { div[class="block"] { width: 100% !important; max-width: none !important; } table[class="full-width"] { width: 100% !important; } td[class="mobile-padding"] { padding: 0 0 20px 0 !important; } td[class="normalize-height"] { height: auto !important; } table[class="center"], td[class="center"] { text-align: center !important; float: none !important; } table[class="left"], td[class="left"] { text-align: left !important; float: left !important; } table[class="right"], td[class="right"] { text-align: right !important; float: right !important; } table[class="remove"], tr[class="remove"], span[class="remove"] { display: none; } }
</style>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;" width="100%">
	<tbody>
		<tr>
			<td align="center" valign="top">
			<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0; background: #f7f7f7;">
				<tbody>
					<tr>
						<td align="center" valign="top" width="680">
						<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;background: #f7f7f7;" width="100%">
							<tbody>
								<tr>
									<td align="center" valign="top"><img alt="" src="http://hips.thehealthpartner.com/sites/hips.careadvantage.jnj.com/files/Hips_email_logo.png" style="width: 100%; max-width: 100%;" />
									<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;" width="100%">
										<tbody>
											<tr>
												<td align="center" valign="top">
												<table align="center" border="0" cellpadding="0" cellspacing="20" style="margin: 0; padding-bottom: 0; background: #fff; border-bottom: 2px solid #ddd; border-top: 2px solid #ddd;" width="100%">
													<tbody>
														<tr>
															<td align="center" style="padding-left: 20px; padding-right: 20px;" valign="top">
															<table align="left" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 20px;">
																<tbody>
																	<tr>
																		<td align="left" style="color: #000000; font-family:Helvetica,Arial; font-size: 14px; line-height:
									24px; font-weight: 400;" valign="top" width="500">
																		<p>[user:field_first_name],</p>

																		<p>Thank you for creating an account with the Health Partner for Hips app, your resource for personalized support before and after hip replacement surgery. Each week, you’ll receive the most relevant information for each phase of your hip replacement journey — from four weeks before your surgery to 12 weeks after.&nbsp;We hope these interactive lessons help set you on your path for success!</p>

																		<p>&nbsp;</p>

																		<p>-- The Health Partner for Hips team</p>

																		<p>Technical problems? Suggestions for or improvements to our app? You can call us at 1-888-980-8947, 7 days a week, 24 hours a day.</p>

																		<p>If you have a medical question, contact your health care provider. If this is a medical emergency, call 911.</p>

																		<p>&nbsp;</p>
																		</td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>

												<table align="center" border="0" cellpadding="20" cellspacing="0" style="margin: 0; padding: 0;background: #f7f7f7;">
													<tbody>
														<tr>
															<td align="left" style="padding: 4%;" valign="top">
															<table align="left" border="0" cellpadding="0" cellspacing="0" style="margin: 0;">
																<tbody>
																	<tr>
																		<td align="left" style="line-height: 0px !important; padding-bottom: 3%; width:100%" valign="top"><img alt="jnj-logo" src="https://hips.thehealthpartner.com/sites/default/files/inline-images/Poweredby.png" style="width: 50%; max-width: 100%;" width="360" /></td>
																	</tr>
																</tbody>
															</table>

															<table align="left" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding:20px;">
																<tbody>
																	<tr>
																		<td align="left" style="color: #333; font: 14px Helvetica, sans-serif; line-height: 125%; font-weight: 400;" valign="top" width="350">Johnson &amp; Johnson Health and Wellness Solutions, Inc.<br />
																		One Johnson &amp; Johnson Plaza<br />
																		Atrium 7<br />
																		New Brunswick, New Jersey 08933 USA</td>
																	</tr>
																	<tr>
																		<td height="10" style="height: 31px; line-height: 31px;">&nbsp;</td>
																	</tr>
																	<tr>
																		<td align="left" style="color: #333; font: 14px Helvetica, sans-serif; line-height: 150%; font-weight: 400;" valign="top" width="400">©Johnson &amp; Johnson Health and Wellness Solutions, Inc. 2016 All rights reserved. Please review the sites\'s <a href="http://dev.hips.careadvantage.jnj.com/privacy-policy">Privacy Policy</a> and <a href="http://dev.hips.careadvantage.jnj.com/legal-notice">Legal Notice.</a></td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
							</tbody>
						</table>

						<table align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0;" width="100%">
							<tbody>
								<tr>
									<td height="20" style="height: 20px; line-height: 20px;">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
',
      '#rows' => 15,
    );

    $form['custom_email_password_reset'] = array(
      '#type' => 'details',
      '#title' => $this->t('Password recovery'),
      '#description' => $this->t('Edit the email messages sent to users who request a new password.') . ' ' . $email_token_help,
      '#group' => 'rwb_email',
      '#weight' => 10,
    );
    $form['custom_email_password_reset'] = array(
      '#type' => 'details',
      '#title' => $this->t('Password recovery'),
      '#description' => $this->t('Edit the email messages sent to users who request a new password.') . ' ' . $email_token_help,
      '#group' => 'rwb_email',
      '#weight' => 10,
    );

    $password_reset_body_text = $config->get('password_reset_body_text');
    $form['custom_email_password_reset']['password_reset_body_text'] = array(
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Password recovery email body'),
      '#default_value' => isset($password_reset_body_text['value']) ? $password_reset_body_text['value'] : '<p>[user:field_patient_first_name],</p>
 <p>A request to reset the password for your account has been made at [site:name].</p><p>New Password: [user:name]</p><p><br />
 Technical problems? Suggestions or improvements for our application? General comments? We’d love to hear from you. You can call us at +1-888-980-8947, 7days a week, 24 hours day. If you’d prefer to reach via email, please contact admin@myhealthyskills.com</p>
 <p>If you have a medical question, contact your health care provider. If this is a medical emergency, call 911.</p>
 <p>&nbsp;</p><p>© Johnson &amp; Johnson Health and Wellness Solutions, Inc. 2015 | Ann Arbor, MI, USA. All rights reserved. This email is sent by Johnson &amp; Johnson Health and Wellness Solutions, Inc., which is solely responsible for its contents. It is intended for visitors from the USA. Please review the site’s <a href="http://careadvantage.jnj.com/privacy-policy">Privacy Policy</a>&nbsp; and <a href="http://careadvantage.jnj.com/legal-notice">Legal Notice</a></p>
 <p>Send from email address: admin@myhealthyskills.com</p>',
      '#rows' => 15,
    );
    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('batch_notification_time')) {
      $date = DateTimePlus::createFromFormat('Y-m-d H:i:s e', $form_state->getValue('batch_notification_time'));
      $dateTime = $date->format('Y-m-d H:i:s');
      $this->configFactory->getEditable('user_management.settings')
        ->set('batch_notification_time', $dateTime)
        ->save();
    }

    $this->configFactory->getEditable('user_management.settings')
      ->set('dailyEnvironmentName', $form_state->getValue('dailyEnvironmentName'))
      ->set('dailyVersion', $form_state->getValue('dailyVersion'))
      ->set('dailyDownloadLink', $form_state->getValue('dailyDownloadLink'))
      ->set('devEnvironmentName', $form_state->getValue('devEnvironmentName'))
      ->set('devVersion', $form_state->getValue('devVersion'))
      ->set('devDownloadLink', $form_state->getValue('devDownloadLink'))
      ->set('stageEnvironmentName', $form_state->getValue('stageEnvironmentName'))
      ->set('stageVersion', $form_state->getValue('stageVersion'))
      ->set('stageDownloadLink', $form_state->getValue('stageDownloadLink'))
      ->set('prodEnvironmentName', $form_state->getValue('prodEnvironmentName'))
      ->set('prodVersion', $form_state->getValue('prodVersion'))
      ->set('prodDownloadLink', $form_state->getValue('prodDownloadLink'))
      ->set('mailChimp_apiKey', $form_state->getValue('mailChimp_apiKey'))
      ->set('knees_mailChimp_listId', $form_state->getValue('knees_mailChimp_listId'))
      ->set('hips_mailChimp_listId', $form_state->getValue('hips_mailChimp_listId'))
      ->set('password_reset_body_text', $form_state->getValue('password_reset_body_text'))
      ->set('preSurgeryNode', $form_state->getValue('preSurgeryNode'))
      ->set('checkInStatement', $form_state->getValue('checkInStatement'))
      ->set('knees_register_no_approval_required_subject', $form_state->getValue('knees_register_no_approval_required_subject'))
      ->set('knees_register_no_approval_required_body_text', $form_state->getValue('knees_register_no_approval_required_body_text'))
      ->set('hips_register_no_approval_required_subject', $form_state->getValue('hips_register_no_approval_required_subject'))
      ->set('hips_register_no_approval_required_body_text', $form_state->getValue('hips_register_no_approval_required_body_text'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['user_management.settings'];
  }

}
