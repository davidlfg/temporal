<?php
/**
 * DCController file to create DC queue content
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\disruptive_checkin\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\disruptive_checkin\DateTime\DateTime as CustomDateTime;
use Drupal\file\Entity\File;
use Drupal\disruptive_checkin\Entity\PromptAction;
Use Drupal\user_management\Service\getTaxonomyTerm;

define('DC_LISTING_LIMIT', 5);
define('DC_MAX_SKIPPED_TIMES', 3);

class DCController {

  private $uid;

  /**
   * Process all programmed DCs for a user and returns the 3 first
   * @return JsonResponse
   */
  public function getByUser() {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    if (!$user['field_surgery_date']) {
      return new JsonResponse(array('errorMessage' => 'Error validating Surgery Date. Please Update your Surgery Date'), 400);
    }
    $uuid = $user['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $uid = $headlessAccount->id();

    //Check Default JointType
    $taxonomyTerm = new getTaxonomyTerm();
    $application = $taxonomyTerm->listTaxonomyTerm('application');
    $defaultType = array_search('Knee Replacement Surgery', $application);
    //End check
    $jointType = isset($user['field_joint_replacement_type']) ? $user['field_joint_replacement_type'] : $defaultType;
    // Get DCs configured on the CMS
    $dcsQuery = \Drupal::entityQuery('node')
      ->condition('status', NODE_PUBLISHED)
      ->condition('field_type_of_prompt', 'disruptive check-in')
      ->condition('type', array(
        'prompt_text_only',
        'prompt_text_image',
        'prompt_text_image_audio',
        'prompt_text_audio'
      ), 'IN')
      ->condition('field_joint_replacement_type.entity', array($jointType), 'IN')
      ->sort('field_weight');
    if (!$dcIds = $dcsQuery->execute()) { //If there's no DC, finish flow
      return new JsonResponse([]);
    }
    $dcs = Node::loadMultiple($dcIds);

    // Get User data to process DCs
    $this->uid = $uid;
    $users = User::load($uid);
    $timezone = $users->timezone->value;
    $surgeryDate = CustomDateTime::createFromFormat('m-d-Y', $user['field_surgery_date'], $timezone);
    $registrationDate = date('Y-m-d H:i:s e', $users->created->value);

    // Get DC instances for the user
    $dcInstances = \Drupal::database()->select('dc_queue')->fields('dc_queue')
      ->condition('uid', $users->id())->execute()
      ->fetchAllAssoc('check_in', \PDO::FETCH_ASSOC);

    $this->disableOldDCs($dcInstances, $surgeryDate, $registrationDate);
    $this->createDCInstances($dcs, $dcInstances, $surgeryDate, $registrationDate);

    // Get DC instances to show
    $dcQueue = \Drupal::database()->select('dc_queue')
      ->fields('dc_queue', array('check_in', 'prompt_id'))
      ->condition('uid', $users->id())->condition('status', 'active')
      ->orderBy('weight', 'ASC')->range(0, DC_LISTING_LIMIT)->execute()
      //@TODO: put limit in CMS
      ->fetchAllAssoc('check_in', \PDO::FETCH_ASSOC);
    $dcPrompts = Node::loadMultiple(array_column($dcQueue, 'prompt_id'));
    $order = 0;
    //Start: Compliance Improvements - check-in collection statement
    $config = $config = \Drupal::configFactory()
      ->getEditable('user_management.settings');
    $checkInStatement = $config->get('checkInStatement');
    $contentIds = explode(",", $checkInStatement);
    if (count($contentIds) > 0) {
      foreach ($contentIds as $akey => $aValue) {
        $node = \Drupal\node\Entity\Node::load($aValue);
        $statement = str_replace(array(
          "\n",
          "\t",
          "\r"
        ), "", htmlspecialchars_decode($node->field_text_content_new['0']->value, ENT_QUOTES));
        $outputArray[] = array(
          'id' => (int) $node->nid['0']->value,
          'prompt_Id' => (int) $node->nid['0']->value,
          'order' => $order,
          'type' => 'content templates - text view',
          'text' => $statement
        );
        $order++;
      }
    }
    //End :Compliance Improvements - check-in collection statement

    foreach ($dcQueue as $dcInstance) {
      $dcPrompt = $dcPrompts[$dcInstance['prompt_id']];
      $audio = $dcPrompt->field_audio_content ?
        File::load($dcPrompt->field_audio_content->target_id) : NULL;
      $image = $dcPrompt->field_image_content ?
        File::load($dcPrompt->field_image_content->target_id) : NULL;
      try {
        $action = PromptAction::createFromNid($dcPrompt->field_prompt_action->target_id)
          ->toArray();
      } catch (\Exception $ex) {
        \Drupal::logger('Disruptive Checkins')->warning('Disruptive Check-in ' .
          $dcInstance['prompt_id'] . 'doesn\'t have an action defined.');
        $action = array();
      }
      $field_text_content = str_replace(array(
        "\n",
        "\t",
        "\r"
      ), "", htmlspecialchars_decode($dcPrompt->field_text_content_new->value, ENT_QUOTES));
      $outputArray[] = array(
          'id' => (int) $dcInstance['check_in'],
          'prompt_Id' => (int) $dcInstance['prompt_id'],
          'isActionItem' => FALSE,
          'order' => (int) $order,
          'type' => $dcPrompt->getType(),
          'text' => $field_text_content,
          'title' => $dcPrompt->getTitle(),
          'isMandatory' => isset($dcPrompt->field_is_mandatory->value) ? (int) $dcPrompt->field_is_mandatory->value : 0,
          'action' => $action,
        ) + ($image ? array(
          'image' => array(
            'url' => $image->url(),
            'fileName' => $image->getFilename(),
            'format' => $image->getMimeType()
          )
        ) : array()) + ($audio ? array(
          'audio' => array(
            'url' => $audio->url(),
            'fileName' => $audio->getFilename(),
            'format' => $audio->getMimeType()
          )
        ) : array());
      $order++;
    }
    if (count($outputArray) == count($contentIds)) {
      return new JsonResponse(array(
        'disruptiveCheckIn' => 1,
        'contentElements' => array()
      ));
    }
    else {
      return new JsonResponse(array(
        'disruptiveCheckIn' => 1,
        'contentElements' => $outputArray
      ));
    }
  }

  /**
   * Verify all the active DCs and disable the old ones.
   *
   * @param array $dcInstances
   * @param CustomDateTime $surgeryDate
   * @param CustomDateTime $registrationDate
   */
  private function disableOldDCs($dcInstances, $surgeryDate, $registrationDate) {
    foreach ($dcInstances as $dcInstance) {
      if ($dcInstance['status'] == 'inactive') {
        continue;
      }

      $disable = FALSE;
      switch ($dcInstance['reference_date']) {
        case 'Registration Date':
          $disable = !$this->checkInDateRange($registrationDate,
            $dcInstance['dc_active_begin'], $dcInstance['dc_active_end'],
            new CustomDateTime());
          break;

        case 'Current Date':
          $disable = !$this->checkInDateRange(new CustomDateTime($dcInstance['creation_date']),
            $dcInstance['dc_active_begin'], $dcInstance['dc_active_end'],
            new CustomDateTime());
          break;

        case 'Surgery Date':
          $disable = !$this->checkInDateRange($surgeryDate,
            $dcInstance['dc_active_begin'], $dcInstance['dc_active_end'],
            new CustomDateTime());
          break;

        default: //If no reference date matching, disable DC
          \Drupal::logger('Disruptive Checkins')
            ->error('DC Instance doesn\'t ' .
              'have a valid reference date for check-in "' .
              $dcInstance['check_in'] . '"');
          $disable = TRUE;
          break;
      }

      if ($disable) {
        $this->disableDC($dcInstance['check_in']);
      }
    }
  }

  /**
   * Checks if the date is between reference + begin/end
   *
   * @param CustomDateTime $referenceDate
   * @param int $rangeBegin
   * @param int $rangeEnd
   * @param CustomDateTime $date
   */
  private function checkInDateRange($referenceDate, $rangeBegin, $rangeEnd, $date) {
    return (CustomDateTime::createFromDrupalDateTime($referenceDate)
        ->addDays($rangeBegin)->isBefore($date) &&
      CustomDateTime::createFromDrupalDateTime($referenceDate)
        ->addDays($rangeEnd)->isAfter($date));
  }

  /**
   * Sets the dc as inactive in the DB
   * @param int $dcId
   * @param boolean $updateCount
   */
  private function disableDC($dcId, $updateCount = FALSE) {
    $dbQuery = \Drupal::database()->update('dc_queue')
      ->fields(array('status' => 'inactive'));
    if ($updateCount) {
      $dbQuery->expression('ask_count', 'ask_count + 1');
    }
    $dbQuery->condition('check_in', $dcId)->execute();
  }

  /**
   * Create DC instances into the queue based on the rules for each.
   *
   * @param array $dcs
   * @param array $dcInstances
   * @param CustomDateTime $surgeryDate
   * @param CustomDateTime $registrationDate
   */
  private function createDCInstances($dcs, $dcInstances, $surgeryDate, $registrationDate) {
    $promptIds = array_column($dcInstances, 'prompt_id');
    foreach ($dcs as $dc) {
      if (in_array($dc->nid->value, $promptIds)) { //It's already created
        continue;
      }

      $creationDate = CustomDateTime::createFromTimestamp($dc->created->value);
      switch ($dc->field_reference_date->value) {
        case 'Registration Date':
          $referenceDate = $registrationDate;
          break;

        case 'Current Date':
          $referenceDate = $creationDate;
          break;

        case 'Surgery Date':
          $referenceDate = $surgeryDate;
          break;

        default: //If no reference date matching, don't generate
          \Drupal::logger('Disruptive Checkins')->error('DC Prompt doesn\'t ' .
            'have a valid reference date for nid "' . $dc->nid->value . '"');
          continue;
          break;
      }

      if ($this->checkInDateRange($referenceDate,
        $dc->field_dc_active_range_begin->value,
        $dc->field_dc_active_range_end->value, new CustomDateTime())
      ) {
        $dcData = array(
          'uid' => $this->uid,
          'prompt_id' => $dc->nid->value,
          'completed' => 'N',
          'weight' => (int) $dc->field_weight->value,
          'status' => 'active',
          'ask_count' => 0,
          'reference_date' => $dc->field_reference_date->value,
          'dc_active_begin' => $dc->field_dc_active_range_begin->value,
          'dc_active_end' => $dc->field_dc_active_range_end->value,
          'creation_date' => $creationDate->format(CustomDateTime::FORMAT)
        );
        \Drupal::database()->insert('dc_queue')->fields($dcData)->execute();
      }
    }
  }

  /**
   * Skips current DC adding one to the ask_count field
   * @param int $dcid
   * @return JsonResponse
   */
  public function skip($dcid) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }

    $uuid = $user['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $uid = $headlessAccount->id();

    $askCount = 0;
    $askCount = \Drupal::database()
      ->select('dc_queue', 'dc')
      ->fields('dc', array('ask_count'))
      ->condition('dc.uid', $uid)
      ->condition('dc.check_in', $dcid)
      ->execute()
      ->fetchField();
    \Drupal::logger($dcid . 'Dc0')->error($askCount);
    $askCount++;
    if ($askCount >= DC_MAX_SKIPPED_TIMES) {
      //\Drupal::logger($dcid . 'Dc1')->error($askCount);
      \Drupal::database()->update('dc_queue')->fields(
        array(
          'complete_time' => (new CustomDateTime())->format(CustomDateTime::FORMAT),
          'updated_date' => (new CustomDateTime())->format(CustomDateTime::FORMAT),
          'ask_count' => $askCount,
          'status' => 'inactive',
          'completed' => 'Y',
        ))
        ->condition('check_in', $dcid)->execute();
      $message = array(
        'status' => 'ok'
      );
      return new JsonResponse($message, 200);
    }
    else {
      //\Drupal::logger($dcid . 'Dc2')->error($askCount);
      \Drupal::database()->update('dc_queue')->fields(
        array(
          'updated_date' => (new CustomDateTime())->format(CustomDateTime::FORMAT),
          'ask_count' => $askCount,
          'status' => 'active',
          'completed' => 'N',
          'complete_time' => NULL
        ))
        ->condition('check_in', $dcid)->execute();
    }
    $message = array(
      'status' => 'ok'
    );
    return new JsonResponse($message, 200);
  }

  /**
   * Answer a particular DC, updating all its information.
   * @param int $dcid
   * @return JsonResponse
   */
  public function answer($dcid) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }

    $uuid = $user['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $uid = $headlessAccount->id();

    $dcInstance = \Drupal::database()->select('dc_queue')
      ->fields('dc_queue', array('ask_count'))
      ->condition('status', 'active')->condition('uid', $uid)
      ->condition('check_in', $dcid)->execute()->fetchAssoc();
    if (!$dcInstance) {
      \Drupal::logger('Disruptive Checkins')->warning('User ' . $uid .
        ' is trying to answer DC ' . $dcid . '.');
      return JsonResponse::create(
        array('errorMessage' => 'Invalid DC reference.'), 400);
    }
    \Drupal::database()->update('dc_queue')->fields(
      array(
        'completed' => 'Y',
        'complete_time' => (new CustomDateTime())->format(CustomDateTime::FORMAT),
        'updated_date' => (new CustomDateTime())->format(CustomDateTime::FORMAT),
        'status' => 'inactive'
      ))->expression('ask_count', 'ask_count + 1')
      ->condition('check_in', $dcid)->execute();
    return JsonResponse::create(array('status' => 'ok'));
  }

}