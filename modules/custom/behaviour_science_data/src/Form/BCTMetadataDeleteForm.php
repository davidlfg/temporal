<?php

namespace Drupal\behaviour_science_data\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting BCT Metadata entities.
 *
 * @ingroup behaviour_science_data
 */
class BCTMetadataDeleteForm extends ContentEntityDeleteForm {


}
