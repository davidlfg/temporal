<?php
/**
 * updatelinkController file
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\field_collection\Entity\FieldCollectionItem;


use Drupal\Component\Datetime\DateTimePlus;

/**
 * Class updatelinkController
 * @package Drupal\contentchain\Controller
 */
class updatelinkController {

  /**
   * Function for updating the text token variable with real text
   * @param $linkId
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function update_link($linkId) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $uid = $headlessAccount->id();

    if ($linkId && $uid) {
      $view = views_embed_view('chain_link_view', 'rest_export_1', $linkId);
      $view_result1 = \Drupal::service('renderer')->render($view);
      $chain_json1 = (string) $view_result1;
      $chain_array1 = json_decode($chain_json1);
      $items = array();
      if ($chain_array1['0']->type == 'Content Templates - Voice Over' or $chain_array1['0']->type == 'Content Templates - Voice Over (Multiple Cards)') {
        $view = views_embed_view('voice_over_content', 'rest_export_1', $linkId);
        $view_result = \Drupal::service('renderer')->render($view);
        $chain_json = (string) $view_result;
        $chain_array = json_decode($chain_json);

        $voice_over_slide_array = $chain_array['0']->field_voice_over_slide;
        $voice_over_slide_bottom_array = $chain_array['0']->field_voice_over_slide_bottom;
        $j = 0;
        if ($chain_array1['0']->type == 'Content Templates - Voice Over (Multiple Cards)') {
          $voiceOverCards = 'voiceOverCardsTop';
        }
        else {
          $voiceOverCards = 'voiceOverCards';
        }
        foreach ($voice_over_slide_array as $key => $value) {
          $field_collection_id = $value->value;
          $fieldcollectionitem = FieldCollectionItem::load($field_collection_id);
          $text = $fieldcollectionitem->field_slide_text_new->value;
          $pattern = "/\[(.*?)]/";
          preg_match_all($pattern, $text, $matches);
          if (count($matches[1]) > 0) {
            $items[$voiceOverCards][$j]['order'] = (int) $key;
            if ($text == NULL) {
              $items[$voiceOverCards][$j]['cardText'] = "";
            }
            else {
              $items[$voiceOverCards][$j]['cardText'] = $this->tokenReplace(str_replace("\n", "", htmlspecialchars_decode($text, ENT_QUOTES)), $uid,$user);
            }
            $j++;
          }
        }
        $k = 0;
        foreach ($voice_over_slide_bottom_array as $key => $value) {
          $field_collection_id = $value->value;
          $fieldcollectionitem = FieldCollectionItem::load($field_collection_id);
          $text = $fieldcollectionitem->field_slide_text_new->value;
          $pattern = "/\[(.*?)]/";
          preg_match_all($pattern, $text, $matches);
          if (count($matches[1]) > 0) {
            $items['voiceOverCardsBottom'][$k]['order'] = (int) $key;
            if ($text == NULL) {
              $items['voiceOverCardsBottom'][$k]['cardText'] = "";
            }
            else {
              $items['voiceOverCardsBottom'][$k]['cardText'] = $this->tokenReplace(str_replace("\n", "", htmlspecialchars_decode($text, ENT_QUOTES)), $uid,$user);
            }
            $k++;
          }
        }
      }
      else { //For all other content type
        $text = $chain_array1['0']->field_text_content_new;
        if ($text == NULL) {
          $items['text'] = "";
        }
        else {
          $items['text'] = $this->tokenReplace(str_replace("\n", "", htmlspecialchars_decode($text, ENT_QUOTES)), $uid,$user);
        }
      }
      return new JsonResponse($items, 200);
    }
    else {
      $response = array(
        'errorMessage' => 'Error',
      );
      return new JsonResponse($response, 400);
    }
  }

  /*
  * Function to replace token variable wiith actula value
  */
  public function tokenReplace($text, $uid,$user) {
    global $base_url;
    $pattern = "/\[(.*?)]/";
    preg_match_all($pattern, $text, $match);
    $newtxt = '';
    $new_array = array();
    $token_id ='';
    foreach ($match[1] as $keys => $values) {
      $token_arrays = explode('.', $values);
      $token_id = $token_arrays[0];
      $token_key = $token_arrays[1];
      $new_array[$token_id] = $token_key;
    }

    if ($token_id == 'user') {
      $userdata = $user[$token_key];
      if ($userdata) {
        if ($newtxt == '') {
          $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', $userdata, $text);
        }
        else {
          $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', $userdata, $newtxt);
        }
      }
      else {
        if ($newtxt == '') {
          $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', '', $text);
        }
        else {
          $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', '', $newtxt);
        }
      }

      if ($newtxt != '') {
        return str_replace(array(
          "src=\"/",
        ), "src=\"$base_url/",
          str_replace(array(
            "<li></li>",
            "<p></p>",
            "\t",
            "\r",
            "<ul></ul>"
          ), "", $newtxt));
      }

      else {
        return $text;
      }
    }


    if (count($new_array) == 1 && $token_id != 'user') { //For a single node
      $key_value = \Drupal::database()->select('responses', 'rs')
        ->fields('rs', array('key_value_array'))
        ->condition('rs.uid', $uid)
        ->condition('rs.link_id', $token_id)
        ->execute()
        ->fetchField();
      $key_value_array = unserialize($key_value);
      foreach ($key_value_array as $key => $value) {
        $matches[$token_id . '.' . $key] = $value;
      }
      $result = preg_replace_callback('/\[(.*?)]/', function ($preg) use ($matches) {
        return isset($matches[$preg[1]]) ? $matches[$preg[1]] : '';
      }, $text);

      $newtxt = str_replace(array(
        "src=\"/",
      ), "src=\"$base_url/",
        str_replace(array(
          "<li></li>",
          "<p></p>",
          "\t",
          "\r",
          "<ul></ul>"
        ), "", $result));
      return str_replace("<br />", "\n", $newtxt);
    }
    else { //For multiple node
      $patterns = "/\[(.*?)]/";
      preg_match_all($patterns, $text, $matches);
      $newtxt = '';
      foreach ($matches[1] as $key => $value) {
        $token_array = explode('.', $value);
        $token_id = $token_array[0];
        $token_key = $token_array[1];
        if (is_numeric($token_id)) {
          $key_value = \Drupal::database()->select('responses', 'rs')
            ->fields('rs', array('key_value_array'))
            ->condition('rs.uid', $uid)
            ->condition('rs.link_id', $token_id)
            ->execute()
            ->fetchField();
          $key_value_array = unserialize($key_value);  //print_r($key_value_array);
          if (isset($key_value_array[$token_key])) {
            if ($newtxt == '') { //$text2 = str_replace("]<br />" , "]" , $text);
              $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', $key_value_array[$token_key], $text);
            }
            else {
              $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', $key_value_array[$token_key], $newtxt);
            }
          }
          else {
            if ($newtxt == '') {
              $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', '', $text);
            }
            else {
              $newtxt = str_replace('[' . $token_id . '.' . $token_key . ']', '', $newtxt);
            }
          }
        }
      }
    }


    if ($newtxt != '') {
      return str_replace(array(
        "src=\"/",
      ), "src=\"$base_url/",
        str_replace(array(
          "<li></li>",
          "<p></p>",
          "\t",
          "\r",
          "<ul></ul>"
        ), "", $newtxt));
    }

    else {
      return $text;
    }
  }
}