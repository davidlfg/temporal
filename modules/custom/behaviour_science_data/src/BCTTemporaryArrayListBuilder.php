<?php

namespace Drupal\behaviour_science_data;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of BCT Temporary Array entities.
 *
 * @ingroup behaviour_science_data
 */
class BCTTemporaryArrayListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('BCT Temporary Array ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\behaviour_science_data\Entity\BCTTemporaryArray */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.bct_temporary_array.edit_form', array(
          'bct_temporary_array' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
