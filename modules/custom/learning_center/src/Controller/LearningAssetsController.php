<?php

namespace Drupal\learning_center\Controller;
define("API_HOST", ($env === "production") ? "example.com" : "headless.dev");

/**
 * LearningAssetsController file.
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any
 * means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Vocabulary;
use GuzzleHttp\Exception\RequestException;
use Drupal\image\Entity\ImageStyle;

/**
 * Manager for the Learning Center Resources.
 */
class LearningAssetsController {

  /**
   * @SWG\Get(
   *   path="/learning_center/filters",
   *   summary="list of existing taxonomies",
   *   tags={"taxonomies"},
   *   produces={"application/json"},
   *   @SWG\Response(
   *     response=200,
   *     description="successful operation",
   *     @SWG\Schema(
   *       type="array"
   *     )
   *   ),
   *   @SWG\Response(
   *     response="400",
   *     description="an ""unexpected"" error"
   *   )
   * )
   */

  /**
   * Retrieves available categories to filter content.
   *
   * @Return array with the list of existing taxonomies
   */
  public function learningCenterGetFilters() {
    $vids = Vocabulary::loadMultiple();
    $categories = array();
    foreach ($vids as $vid) {
      if (($vid->label() == 'Learning Center Concern')||($vid->label() == 'Learning Center Learn')||($vid->label() == 'Learning Center Other')) {
        $container = \Drupal::getContainer();
        $terms = $container->get('entity.manager')->getStorage('taxonomy_term')->loadTree($vid->id());
        if (!empty($terms)) {
          foreach ($terms as $term) {
            $categories[$vid->label()][] = $term->name;
          }
        }
      }
    }
    return new JsonResponse($categories, 200);

  }

  /**
   * @SWG\Get(
   *   path="/learning_center/detail/{nodeId}",
   *   summary="Retrieves the detailed page full fields for a learning center asset",
   *   tags={"fields"},
   *   produces={"application/json"},
   *   @SWG\Parameter(
   *     name="nodeId",
   *     in="query",
   *     description="fields to filter by nodeId",
   *     required=true,
   *     type="array",
   *     @SWG\Items(type="integer"),
   *   ),
   *   @SWG\Response(
   *     response=200,
   *     description="successful operation",
   *     @SWG\Schema(
   *       type="array"
   *     )
   *   ),
   *   @SWG\Response(
   *     response="400",
   *     description="an ""unexpected"" error"
   *   )
   * )
   */

  /**
   * Retrieves the detailed page full fields for a learning center asset.
   *
   * @Return array with the list of fields
   */
  public function learningCenterGetDetail($nodeId = 0) {

    $asset = Node::load($nodeId);
    if (!empty($asset)&&(($asset->get('type')->getValue()[0]['target_id'] == 'lc_media_page')||($asset->get('type')->getValue()[0]['target_id'] == 'learning_center_external_resourc'))) {
      $response['title'] = $asset->get('title')->getValue()[0]['value'];
      $response['id'] = $asset->get('nid')->getValue()[0]['value'];
      if (!empty($asset->get('field_resource_type')->getValue()[0]['target_id'])) {
        $response['resourceType'] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($asset->get('field_resource_type')->getValue()[0]['target_id']);
      }
      $response['image'] = file_create_url($asset->get('field_tile_image')->entity->getFileUri());

      if (!empty($asset->get('field_mobile_thumbnail')->entity)) {
        $response['thumbnailImage'] = ImageStyle::load('mobile_thumbnail')->buildUrl($asset->get('field_mobile_thumbnail')->entity->getFileUri());
      }

      $response['summary'] = $asset->get('body')->getValue()[0]['summary'];
      $response['body']    = $asset->get('body')->getValue()[0]['value'];

      foreach ($asset->get('field_concern')->getValue() as $concern) {
        $response['concern'][] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($concern['target_id']);
      }

      foreach ($asset->get('field_learn')->getValue() as $learn) {
        $response['learn'][] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($learn['target_id']);
      }

      foreach ($asset->get('field_other')->getValue() as $other) {
        $response['other'][] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($other['target_id']);
      }
      if (($asset->get('type')->getValue()[0]['target_id'] == 'lc_media_page')) {

        $response['accountID'] = $asset->get('field_brightcove_account_id')->getValue()[0]['value'];
        $response['videoURL'] = $asset->get('field_brightcove_video_url')->getValue()[0]['value'];

        if (!empty($response['videoURL'])) {

          $videoId = substr($response['videoURL'], -13);

        }

        if (!empty($videoId)) {

          try {
            $url = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $videoId . '&video_fields=name,length,videoStillURL,thumbnailURL&token=VBjklsIUTUadgme2oCoKSC6A5120bFTtFP93iHTN48uZfPGnGLgJWA..';
            $data = (string) \Drupal::httpClient()
              ->get($url)
              ->getBody();
            $videoData = json_decode($data, TRUE);
          }
          catch (RequestException $exception) {
            \Drupal::logger('learning_center')->error($exception->getMessage());
            return FALSE;
          }

          if (!empty($videoData)) {
            $response['videoData'] = $videoData;
          }

        }

        if (!empty($asset->get('field_related_resources')->getValue())) {
          $h = 0;
          foreach ($asset->get('field_related_resources')->getValue() as $related) {
            $response['relatedResources'][$h] = $related["target_id"];
            $h++;
          }
        }
      }

      return new JsonResponse($response, 200);

    }
    else {
      $response = array();
      return new JsonResponse($response, 200);
    }

  }

  /**
   * @SWG\Get(
   *   path="/learning_center/list",
   *   summary="Get Learning Center Assets List",
   *   tags={"node"},
   *   produces={"application/json"},
   *   @SWG\Response(
   *     response=200,
   *     description="successful operation",
   *     @SWG\Schema(
   *       type="array"
   *     )
   *   ),
   *   @SWG\Response(
   *     response="400",
   *     description="an ""unexpected"" error"
   *   )
   * )
   */

  /**
   * Retrieves the list of learning center assets available.
   *
   * @Return array with the list of existing taxonomies
   */
  public function learningCenterGetList() {

    $types = array('lc_media_page', 'learning_center_external_resourc');

    $query = \Drupal::entityQuery('node')
      ->condition('type', $types, 'IN');

    $nids = $query->execute();

    if (!empty($nids)) {
      $assets = Node::loadMultiple($nids);

      $i = 0;
      $items = array();
      /**
       * @var $asset \Drupal\Core\Entity\ContentEntityBase
       */
      foreach ($assets as $asset) {

        $items[$i]['title'] = $asset->get('title')->getValue()[0]['value'];
        $items[$i]['id'] = $asset->get('nid')->getValue()[0]['value'];
        if (!empty($asset->get('field_resource_type')->getValue()[0]['target_id'])) {
          $items[$i]['resourceType'] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($asset->get('field_resource_type')->getValue()[0]['target_id']);
        }
        $items[$i]['image'] = file_create_url($asset->get('field_tile_image')->entity->getFileUri());

        if (!empty($asset->get('field_mobile_thumbnail')->entity)) {
          $items[$i]['thumbnailImage'] = ImageStyle::load('mobile_thumbnail')->buildUrl($asset->get('field_mobile_thumbnail')->entity->getFileUri());
        }
        $items[$i]['summary'] = $asset->get('body')->getValue()[0]['summary'];

        foreach ($asset->get('field_concern')->getValue() as $concern) {
          $items[$i]['concern'][] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($concern['target_id']);
        }

        foreach ($asset->get('field_learn')->getValue() as $learn) {
          $items[$i]['learn'][] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($learn['target_id']);
        }

        foreach ($asset->get('field_other')->getValue() as $other) {
          $items[$i]['other'][] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($other['target_id']);
        }
        if (($asset->get('type')->getValue()[0]['target_id'] == 'lc_media_page')) {
          if (!empty($asset->get('field_related_resources')->getValue())) {
            foreach ($asset->get('field_related_resources')->getValue() as $related) {
              $items[$i]['relatedResources'][] = $related["target_id"];
            }
          }
        }
        if (!empty($asset->get('field_resource_type')->getValue()[0]['target_id'])) {
          if (($asset->get('type')->getValue()[0]['target_id'] == 'lc_media_page')&&(\Drupal::service('headless_toolbox.content')->getTermNamebyTid($asset->get('field_resource_type')->getValue()[0]['target_id']) == 'video')) {
            $items[$i]['accountID'] = $asset->get('field_brightcove_account_id')->getValue()[0]['value'];
            $items[$i]['videoURL'] = $asset->get('field_brightcove_video_url')->getValue()[0]['value'];
          }
        }

        $i++;
      }
      return new JsonResponse($items, 200);

    }
    else {
      $response = array();
      return new JsonResponse($response, 200);
    }

  }

}