<?php

namespace Drupal\watson_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\CronInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class WatsonConfigForm.
 *
 * @package Drupal\watson_connector\Form
 */
class WatsonConfigForm extends ConfigFormBase {
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The cron service.
   *
   * @var \Drupal\Core\CronInterface
   */
  protected $cron;

  /**
   * The state keyvalue collection.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $current_user, CronInterface $cron, StateInterface $state) {
    parent::__construct($config_factory);
    $this->currentUser = $current_user;
    $this->cron = $cron;
    $this->state = $state;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('cron'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'watson_connector.WatsonConfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'watson_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('watson_connector.WatsonConfig');
    $form['custom_cron'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom Cron Configuration'),
      '#open' => FALSE,
    ];
    $form['custom_cron']['status'] = [
      '#type' => 'details',
      '#title' => $this->t('Cron status information'),
      '#open' => FALSE,
    ];
    $form['custom_cron']['status']['intro'] = [
      '#type' => 'item',
      '#markup' => $this->t('If you have administrative privileges you can run cron from this page and see the results.'),
    ];

    $next_execution = $config->get('next_execution');
    $next_execution = !empty($next_execution) ? $next_execution : REQUEST_TIME;

    $args = [
      '%time' => date_iso8601($config->get('next_execution')),
      '%seconds' => $next_execution - REQUEST_TIME,
    ];
    $form['custom_cron']['status']['last'] = [
      '#type' => 'item',
      '#markup' => $this->t('Watson Cron will next execute the first time cron runs after %time (%seconds seconds from now)', $args),
    ];

    if ($this->currentUser->hasPermission('administer site configuration')) {
      $form['custom_cron']['cron_run'] = [
        '#type' => 'details',
        '#title' => $this->t('Run cron manually'),
        '#open' => FALSE,
      ];
      $form['custom_cron']['cron_run']['cron_reset'] = [
        '#type' => 'checkbox',
        '#title' => $this->t("Run Watson cron regardless of whether interval has expired."),
        '#default_value' => FALSE,
      ];
      $form['custom_cron']['cron_run']['cron_trigger']['actions'] = ['#type' => 'actions'];
      $form['custom_cron']['cron_run']['cron_trigger']['actions']['sumbit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Run cron now'),
        '#submit' => [[$this, 'cronRun']],
      ];
    }


    $form['custom_cron']['configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration of Watson Cron'),
      '#open' => FALSE,
    ];
    $form['custom_cron']['configuration']['cron_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Cron interval'),
      '#description' => $this->t('Time after which Watson Cron will respond to a processing request.'),
      '#default_value' => $config->get('interval'),
      '#options' => [
        3600 => $this->t('1 hour'),
        86400 => $this->t('1 day'),
      ],
    ];

    $form['apigee_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Configuration of Watson Apigee'),
      '#open' => FALSE,
    ];
    //Access Token
    $apigee_accesstoken_uri = $config->get('apigee_accesstoken_uri');
    $form['apigee_configuration']['apigee_accesstoken_uri'] = [
      '#type' => 'textfield',
      '#maxlength' => 150,
      '#size' => 100,
      '#title' => $this->t('Apigee Get Access Token : URI'),
      '#description' => $this->t('Example :https://jnj-dev.apigee.net/oauth/client_credential/accesstoken?grant_type=client_credentials.'),
      '#default_value' => isset($apigee_accesstoken_uri) ? $apigee_accesstoken_uri : 'https://jnj-dev.apigee.net/oauth/client_credential/accesstoken?grant_type=client_credentials',
    ];
    $apigee_accesstoken_client_id = $config->get('apigee_accesstoken_client_id');
    $form['apigee_configuration']['apigee_accesstoken_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apigee Get Access Token : Client Id'),
      '#description' => $this->t('Example :WaSmCphfqVAZGtIoTGOks7iBYVuiaCN5'),
      '#default_value' => isset($apigee_accesstoken_client_id) ? $apigee_accesstoken_client_id : 'WaSmCphfqVAZGtIoTGOks7iBYVuiaCN5',
    ];
    $apigee_accesstoken_client_secret = $config->get('apigee_accesstoken_client_secret');
    $form['apigee_configuration']['apigee_accesstoken_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apigee Get Access : Client Secret'),
      '#description' => $this->t('Example :5gmB4xJh4WGGcUL7'),
      '#default_value' => isset($apigee_accesstoken_client_secret) ? $apigee_accesstoken_client_secret : '5gmB4xJh4WGGcUL7',
    ];
    $apigee_file_upload_uri = $config->get('apigee_file_upload_uri');
    $form['apigee_configuration']['apigee_file_upload_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apigee File Upload URI'),
      '#maxlength' => 150,
      '#size' => 100,
      '#description' => $this->t('Example :https://jnj-dev.apigee.net/v1/jnj-watson-analytics/upload/questionnaireResponse.'),
      '#default_value' => isset($apigee_file_upload_uri) ? $config->get('apigee_file_upload_uri') : 'https://jnj-dev.apigee.net/v1/jnj-watson-analytics/upload/questionnaireResponse',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('watson_connector.WatsonConfig')
      ->set('interval', $form_state->getValue('cron_interval'))
      ->set('apigee_accesstoken_uri', $form_state->getValue('apigee_accesstoken_uri'))
      ->set('apigee_accesstoken_client_id', $form_state->getValue('apigee_accesstoken_client_id'))
      ->set('apigee_accesstoken_client_secret', $form_state->getValue('apigee_accesstoken_client_secret'))
      ->set('apigee_file_upload_uri', $form_state->getValue('apigee_file_upload_uri'))
      ->save();
  }
  /**
   * Allow user to directly execute cron, optionally forcing it.
   */
  public function cronRun(array &$form, FormStateInterface &$form_state) {
    $config = $this->configFactory->getEditable('watson_connector.WatsonConfig');

    $cron_reset = $form_state->getValue('cron_reset');
    if (!empty($cron_reset)) {
      $config->set('next_execution', 0);
    }

    // Use a state variable to signal that cron was run manually from this form.
    $this->state->set('custom_cron_show_status_message', TRUE);
    if ($this->cron->run()) {
      drupal_set_message($this->t('Cron ran successfully.'));
    }
    else {
      drupal_set_message($this->t('Cron run failed.'), 'error');
    }
  }



}
