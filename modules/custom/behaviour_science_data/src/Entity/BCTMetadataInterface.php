<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining BCT Metadata entities.
 *
 * @ingroup behaviour_science_data
 */
interface BCTMetadataInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the BCT Metadata name.
   *
   * @return string
   *   Name of the BCT Metadata.
   */
  public function getName();

  /**
   * Sets the BCT Metadata name.
   *
   * @param string $name
   *   The BCT Metadata name.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTMetadataInterface
   *   The called BCT Metadata entity.
   */
  public function setName($name);

  /**
   * Gets the BCT Metadata creation timestamp.
   *
   * @return int
   *   Creation timestamp of the BCT Metadata.
   */
  public function getCreatedTime();

  /**
   * Sets the BCT Metadata creation timestamp.
   *
   * @param int $timestamp
   *   The BCT Metadata creation timestamp.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTMetadataInterface
   *   The called BCT Metadata entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the BCT Metadata published status indicator.
   *
   * Unpublished BCT Metadata are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the BCT Metadata is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a BCT Metadata.
   *
   * @param bool $published
   *   TRUE to set this BCT Metadata to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTMetadataInterface
   *   The called BCT Metadata entity.
   */
  public function setPublished($published);

}
