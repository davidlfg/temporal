<?php
/**
 * individualchainController file to list all links in a given content chain
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\image\Entity\ImageStyle;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\contentchain\Service\FieldCollectionFields;
use Drupal\contentchain\Service\FileFields;
use Drupal\contentchain\Service\chainContent;
use Drupal\contentchain\Service\updateLongestPath;
use Drupal\field_collection\Entity\FieldCollectionItem;

/**
 * Class individualchainController
 * @package Drupal\contentchain\Controller
 */
class individualchainController {
  /**
   * @param $nid
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function individual_chain($nid) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $userId = $headlessAccount->id();

    $view = views_embed_view('individual_chain', 'rest_export_2', $nid);
    $view_result = \Drupal::service('renderer')->render($view);
    $chain_json = (string) $view_result;
    $chain_array = json_decode($chain_json);
    $items = [];
    $items['Title'] = htmlspecialchars_decode($chain_array['0']->field_display_title['0']->value, ENT_QUOTES);
    $chain_link_array = $chain_array['0']->field_content_chain;

    $items['chainId'] = (int) $nid;
    $chainLongestPath = $chain_array['0']->field_chain_longest_path['0']->value;
    $isDecisionTree = $chain_array['0']->field_is_decision_tree['0']->value;
    if ($chainLongestPath == '') {
      //Update content chain longest path
      $updateLongestPath = new updateLongestPath();
      $entity = $updateLongestPath->updateDecisionTreeLongestPath(\Drupal\node\Entity\Node::load($nid));
      $chainLongestPath = $entity->field_chain_longest_path->getValue()[0]['value'];
      $isDecisionTree = $entity->field_is_decision_tree->getValue()[0]['value'];
    }
    if ($isDecisionTree) {
      $path = self::getChainLongestPath($nid, $userId);
      $items['longPathCount'] = isset($path['longPathCount']) ? $path['longPathCount'] : 0;
    }
    else {
      $items['longPathCount'] = $chainLongestPath;
    }

    global $base_url;
    $fileFields = new FileFields();
    if ($chain_array['0']->field_image) {
      $image_content = $chain_array['0']->field_image['0']->url;
      $fid = $chain_array['0']->field_image['0']->target_id;
      $items = $fileFields->getFilesFields($image_content, $items, 0, 'backgroundImage', $fid);
    }
    $items['selectedChain'] = array();
    if (count($chain_link_array) > 0) {
      if (isset($_SERVER['HTTP_ELEMENTSCOUNT'])) {//Update elements order based on previous chain
        $i = $_SERVER['HTTP_ELEMENTSCOUNT'];
      }
      else { //Default order
        $i = 0;
      }

      foreach ($chain_link_array as $key => $value) {
        $nid = $value->target_id;
        $items['contentElements'][$i]['id'] = (int) $nid;
        $result = \Drupal::database()
          ->query("SELECT type FROM {node} WHERE nid = :nid;", array(':nid' => $nid))
          ->fetchField();
        if ($result) {
          switch ($result) {
            case 'prompt_text_only':
            case 'prompt_text_image':
            case 'prompt_text_image_audio':
            case 'prompt_text_audio':
            case 'content_templates_video':
            case 'content_templates_text_only':
            case 'content_templates_text_image_aud':
            case 'content_templates_text_image':
            case 'content_templates_text_audio':
            case 'content_templates_feedback':
              $typeName = array(
                'prompt_text_only' => 'Prompt - Text Only',
                'prompt_text_image' => 'Prompt - Text + Image',
                'prompt_text_image_audio' => 'Prompt - Text + Image + Audio',
                'prompt_text_audio' => 'Prompt - Text +Audio',
                'content_templates_video' => 'Content Templates - Video',
                'content_templates_text_only' => 'Content Templates - Text Only',
                'content_templates_text_image_aud' => 'Content Templates - Text + Image + Audio',
                'content_templates_text_image' => 'Content Templates - Text + Image',
                'content_templates_text_audio' => 'Content Templates - Text +Audio',
                'content_templates_feedback' => 'Content Templates - Feedback'
              );
              $view = views_embed_view('chain_link_view', 'rest_export_2', $nid);
              $view_result1 = \Drupal::service('renderer')->render($view);
              $chain_json1 = (string) $view_result1;
              $chain_array1 = json_decode($chain_json1);
              $items['contentElements'][$i]['id'] = (int) $chain_array1['0']->nid['0']->value;
              $items['contentElements'][$i]['order'] = (int) $i;
              $items['contentElements'][$i]['title'] = htmlspecialchars_decode($chain_array1['0']->field_display_title['0']->value, ENT_QUOTES);
              $items['contentElements'][$i]['type'] = $typeName[$chain_array1['0']->type['0']->target_id];
              $items['contentElements'][$i]['isMandatory'] = isset($chain_array1['0']->field_is_mandatory) ? (int) $chain_array1['0']->field_is_mandatory['0']->value : 0;
              $items['contentElements'][$i]['containsToken'] = (int) $chain_array1['0']->field_contains_token['0']->value;
              if (count($chain_array1['0']->field_hide_background_image) > 0) {
                $hideBackgroundImage = $chain_array1['0']->field_hide_background_image['0']->value;
              }
              else {
                $hideBackgroundImage = 0;
              }
              $items['contentElements'][$i]['hideBackgroundImage'] = (int) $hideBackgroundImage;
              $items['contentElements'][$i]['text'] = str_replace(array(
                "src=\"/",
              ), "src=\"$base_url/",
                str_replace(array(
                  "\n",
                  "\t",
                  "\r"
                ), "", htmlspecialchars_decode($chain_array1['0']->field_text_content_new['0']->value, ENT_QUOTES)));
              $fileUrl = '';
              $type = '';
              if (isset($chain_array1['0']->field_image_content)) {
                $fileUrl = $chain_array1['0']->field_image_content['0']->url;
                $fid = $chain_array1['0']->field_image_content['0']->target_id;
                $type = 'image';
              }

              $audioType = array(
                'prompt_text_image_audio',
                'content_templates_text_audio',
                'prompt_text_audio'
              );
              if (in_array($chain_array1['0']->type['0']->target_id, $audioType)) {
                if (isset($chain_array1['0']->field_audio_content)) {
                  $fileUrl = $chain_array1['0']->field_audio_content['0']->url;
                  $fid = $chain_array1['0']->field_audio_content['0']->target_id;
                  $type = 'audio';
                }
              }
              $videoType = array(
                'content_templates_video',
              );
              if (in_array($chain_array1['0']->type['0']->target_id, $videoType)) {
                if (isset($chain_array1['0']->field_video_content)) {
                  $fileUrl = $chain_array1['0']->field_video_content['0']->url;
                  $fid = $chain_array1['0']->field_video_content['0']->target_id;
                  $type = 'video';
                }
              }
              if ($fileUrl != '') {
                $items = $fileFields->getFilesFields($fileUrl, $items, $i, $type, $fid);
              }

              if (isset($chain_array1['0']->field_prompt_action)) {
                $field_prompt_action_id = $chain_array1['0']->field_prompt_action['0']->target_id;
                //Check for already submitted values
                $keyValue = \Drupal::database()->select('responses', 'rs')
                  ->fields('rs', array('key_value_array'))
                  ->condition('rs.uid', $userId)
                  ->condition('rs.link_id', $field_prompt_action_id)
                  ->execute()
                  ->fetchField();
                if ($keyValue) {
                  $keyValueArray = unserialize($keyValue);
                }
                else {
                  $keyValueArray = array();
                }
                $view = views_embed_view('action', 'rest_export_1', $field_prompt_action_id);
                $view_result2 = \Drupal::service('renderer')->render($view);
                $chain_json2 = (string) $view_result2;
                $chain_array2 = json_decode($chain_json2);
                $items['contentElements'][$i]['action']['id'] = (int) $chain_array2['0']->nid['0']->value;
                $items['contentElements'][$i]['action']['title'] = htmlspecialchars_decode($chain_array2['0']->field_display_title['0']->value, ENT_QUOTES);
                $items['contentElements'][$i]['action']['questionTaggedToProfile'] = isset($chain_array2['0']->field_question_tagged_to_profile['0']->value) ? (int) $chain_array2['0']->field_question_tagged_to_profile['0']->value : 0;
                $buttons_array = isset($chain_array2['0']->field_buttons) ? $chain_array2['0']->field_buttons : array();
                $checkboxes_array = isset($chain_array2['0']->field_checkboxes) ? $chain_array2['0']->field_checkboxes : array();
                $free_form_text_array = isset($chain_array2['0']->field_free_form_text) ? $chain_array2['0']->field_free_form_text : array();
                $slider_array = isset($chain_array2['0']->field_slider) ? $chain_array2['0']->field_slider : array();
                $toggle_array = isset($chain_array2['0']->field_toggle) ? $chain_array2['0']->field_toggle : array();
                $formFieldArray = isset($chain_array2['0']->field_form_fields) ? $chain_array2['0']->field_form_fields : array();

                $chainForm = new FieldCollectionFields();
                //Button options
                if (count($buttons_array) > 0) {
                  $items['contentElements'][$i]['action']['optionType'] = t('button');
                  $items = $chainForm->getFieldCollectionFields($userId, $buttons_array, $i, $items, $keyValueArray);
                }
                //Checkbox options
                if (count($checkboxes_array) > 0) {
                  $items['contentElements'][$i]['action']['optionType'] = t('checkbox');
                  $items = $chainForm->getFieldCollectionFields($userId, $checkboxes_array, $i, $items, $keyValueArray);
                }
                //FreeForm text options
                if (count($free_form_text_array) > 0) {
                  $items['contentElements'][$i]['action']['optionType'] = t('freeFormText');
                  $items = $chainForm->getFieldCollectionFields($userId, $free_form_text_array, $i, $items, $keyValueArray);
                }
                //Slider options
                if (count($slider_array) > 0) {
                  $items['contentElements'][$i]['action']['optionType'] = t('Slider');
                  $items['contentElements'][$i]['action']['sliderIncrement'] = (float) $chain_array2['0']->field_slider_increment['0']->value;
                  $items['contentElements'][$i]['action']['sliderMinimum'] = (float) $chain_array2['0']->field_slider_minimum['0']->value;
                  $items['contentElements'][$i]['action']['sliderMaximum'] = (float) $chain_array2['0']->field_slider_maximum['0']->value;
                  $items = $chainForm->getFieldCollectionFields($userId, $slider_array, $i, $items, $keyValueArray);
                }
                //Toggle options
                if (count($toggle_array) > 0) {
                  $items['contentElements'][$i]['action']['optionType'] = t('Toggle');
                  $items = $chainForm->getFieldCollectionFields($userId, $toggle_array, $i, $items, $keyValueArray);
                }
                //Forms
                if (count($formFieldArray) > 0) {
                  $items['contentElements'][$i]['action']['optionType'] = t('Form');
                  $items['contentElements'][$i]['action']['formId'] = $chainForm->getTaxonomyTerm((int) $chain_array2['0']->field_form_id['0']->target_id);
                  $items = $chainForm->getFieldCollectionFields($userId, $formFieldArray, $i, $items, $keyValueArray);
                }
              }
              break;
            case 'content_templates_voice_over':
            case 'content_templates_voice_over_mul':
              $view = views_embed_view('voice_over_content', 'rest_export_1', $nid);
              $view_result = \Drupal::service('renderer')->render($view);
              $chain_json = (string) $view_result;
              $chain_array = json_decode($chain_json);
              $items['contentElements'][$i]['id'] = (int) $chain_array['0']->nid['0']->value;
              $items['contentElements'][$i]['order'] = (int) $i;
              $items['contentElements'][$i]['type'] = t('Content Templates - Voice Over');
              $items['contentElements'][$i]['containsToken'] = (int) $chain_array['0']->field_contains_token['0']->value;
              $hideBackgroundImage = isset($chain_array['0']->field_hide_background_image['0']->value) ? $chain_array['0']->field_hide_background_image['0']->value : 0;
              $items['contentElements'][$i]['hideBackgroundImage'] = (int) $hideBackgroundImage;
              $items['contentElements'][$i]['title'] = htmlspecialchars_decode($chain_array['0']->field_display_title['0']->value, ENT_QUOTES);
              if (isset($chain_array['0']->field_voice_over_file['0']->url)) {
                $voice_over_file = $chain_array['0']->field_voice_over_file['0']->url;
                $fid = $chain_array['0']->field_voice_over_file['0']->target_id;
                $items = $fileFields->getFilesFields($voice_over_file, $items, $i, 'audio', $fid);
              }
              $voice_over_slide_array = $chain_array['0']->field_voice_over_slide;
              $voice_over_slide_bottom_array = isset($chain_array['0']->field_voice_over_slide_bottom) ? $chain_array['0']->field_voice_over_slide_bottom : array();
              if (count($voice_over_slide_bottom_array) > 0) {
                $voiceOver = 'voiceOverTop';
                $items['contentElements'][$i]['type'] = t('Content Templates - Voice Over (Multiple Cards)');
              }
              else {
                $voiceOver = 'voiceOver';
              }
              //Voice over & voice over top cards
              $chainForm = new FieldCollectionFields();
              $items = $chainForm->getVoiceOverFieldCollectionFields($items, $voiceOver, $i, $voice_over_slide_array);
              //Voice Over bottom Cards
              if (count($voice_over_slide_bottom_array) > 0) {
                $items = $chainForm->getVoiceOverFieldCollectionFields($items, 'voiceOverBottom', $i, $voice_over_slide_bottom_array);
              }
              break;
          }
          $i++;
        }
      }
      //get decision tree content chain Links and add to 1st chain
      if (count($items['selectedChain']) > 0) {
        $selectedChain = $items['selectedChain'];
        $chainContent = new chainContent();
        $items = $chainContent->getDecisionTreeLinks($selectedChain, $items, $i);
      }
    }

    return new JsonResponse($items, 200);
  }

  /**
   * @param $ContentChainId
   * @return mixed
   */
  private function getChainLongestPath($ContentChainId, $userId) {
    global $base_url;
    $contentChainUrl = $base_url . '/longest-path/' . $ContentChainId;
    try {
      $response = \Drupal::httpClient()->get($contentChainUrl,
        array(
          'headers' => array(
            'userId' => $userId,
          )
        ));
      $data = json_decode($response->getBody()->getContents(), TRUE);

      return $data;

    } catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode() == 400) {
        \Drupal::logger('getChainLongestPath')
          ->notice('Error: ' .
            Json::decode($e->getResponse()->getBody()->getContents()));
      }
      else {
        \Drupal::logger('getChainLongestPath')
          ->error('An error occurred"' .
            $ContentChainId . '"');
      }
    }
  }
}