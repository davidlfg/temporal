<?php

namespace Drupal\watson_connector;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;

/**
 * Class RESTServiceCall.
 *
 * @package Drupal\watson_connector
 */
class RESTServiceCall {

  /**
   * Constructor.
   */
  public function __construct() {

  }


  /**
   * @param $url
   * @param $headers
   * @param $formData
   * @param string $method
   * @return array
   */
  public function prepareRequestResponse($url, $headers, $formData, $method = 'post') {
    $data = array(
      'body' => http_build_query($formData),
      'headers' => [],
      'timeout' => 600
    );
    //Unset header Authorization if that is not a Bearer token
    if (isset($headers['Authorization'])) {
      if (strpos($headers['Authorization'], 'Bearer ') != FALSE) {
        unset($headers['Authorization']);
      }
    }
    //Set Header
    foreach ($headers as $key => $value) {
      $data['headers'][$key] = $value;
    }
    unset($data['headers']['host']);
    unset($data['headers']['Host']);
    unset($data['headers']['Content-Length']);
    //end

    \Drupal::logger('prepareRequest.')->notice(print_r($data, TRUE));
    $response = \Drupal::service('watson.connect')
      ->restCall($url, $data, $method);

    if ($response['StatusCode'] == 200 OR $response['StatusCode'] == 201) {
      return $response['ResponseBody'];
    }
    else {
      return array(
        $response['ResponseBody'],
      );
    }
  }

  /**
   * @param $url
   * @param $data
   * @param $method
   * @return array
   */
  public function restCall($url, $data, $method) {
    $client = \Drupal::httpClient();
    try {
      $request = $client->$method($url, $data);

      $headers = $request->getHeaders();
      $response = array(
        'StatusCode' => $request->getStatusCode(),
        'ResponseBody' => Json::decode($request->getBody()->getContents()),
        'ResponseTime' => $headers['Date'][0],
      );
      \Drupal::logger('RESTCall.' . $method)
        ->notice($url . ':' . print_r($response, TRUE));
    } catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode()) {
        $request = $e->getResponse();
        $headers = $request->getHeaders();
        $response = array(
          'StatusCode' => $request->getStatusCode(),
          'ResponseBody' => json_decode($request->getBody()),
          'ResponseTime' => $headers['Date'][0],
        );
        \Drupal::logger('RESTCall.' . $method)
          ->error($url . ':' . print_r($response, TRUE));
      }
    }
    return $response;
  }

}
