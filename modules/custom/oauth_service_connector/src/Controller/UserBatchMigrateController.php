<?php

/**
 * @file
 * Contains Drupal\rwb_stages_carousel\Controller.
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\oauth_service_connector\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\Exception;

class UserBatchMigrateController extends ControllerBase
{

  /**
   * Loads the 3 carousels and shows them
   */

  public function userMigrate($start , $end) {
    $counter = $start;
    $response = array();
    if($start == NULL || $end == NULL) {
      drush_print('$start or $end cant be NULL');
      die;
    }
    $query = \Drupal::entityQuery('user');
    $query->range($start, $end);
    $uids = $query->execute();
    $uids_bckp = $uids;
    if(count($uids) >1) {
      foreach ($uids as $key => $uid) {
        $user = User::load($uid);
        if($user != NULL) {
          try{
            $formData['field_first_name'] = $user->field_first_name->value;
            $formData['field_last_name'] = $user->field_last_name->value;
            $formData['mail'] = $user->getEmail();
            $formData['uuid'] = $user->uuid();
            $headers['uuid'] = $user->uuid();
            $response = \Drupal::service('oauth_service.connect')
              ->prepareRequestResponse("http://jnj-dev.apigee.net/v1/healthstore-partner/create-user-data", $headers, $formData, 'put');
            $counter ++;
            \Drupal::state()->set('user_count', $counter);
            unset($uids_bckp[$key]);
          }catch(\Exception $e) {
            $config = \Drupal::configFactory()->getEditable('oauth_service_connector.OauthConnectorConfig');
            $id_string  = $config->get('user_migration_record_ids').', '.implode(", ",$uids_bckp);
            $id_string  = implode(', ',array_unique(explode(', ', $id_string)));
            $config->set('user_migration_record_ids',$id_string);
            $config->save();
            throw $e;
          }
        }
      }
    }
    return $response;
  }
}