<?php

/**
 * @file
 * Drush command to migrate users
 */

use Drupal\oauth_service_connector\Controller\UserBatchMigrateController;

/**
 * Implements hook_drush_command().
*/

function oauth_service_connector_drush_command() {
  $items = array();
  $items['migrate-users'] = array(
    'description' => 'Migrate Users.',
    'examples' => array(
      'drush migrate-users' => 'Migrate Users.',
    ),
  );
 
  return $items;
}



/**
 * Finished callback for the user deletion batch.
 * @param int $success
 *   Equals 1 if batch is successfull else equals 0.
 * @param variable $results
 *   List of results parameter collected during batch processing.
 * @param variable $operations
 *   @todo  add description.
 */

function drush_oauth_service_connector_migrate_users(){
   drush_print('Migrate Users');
   $lastinsertUserLimit = Drupal::state()->get('user_count');
   $start  = drush_prompt("Please start Limit", isset($lastinsertUserLimit) ? $lastinsertUserLimit : NULL, FALSE);
   $end    = drush_prompt("Please end Limit", NULL, FALSE);
   $batch = usersMigrateBatch($start,$end);
   
   // Initialize the batch.
   batch_set($batch);

   // Start the batch process.
   drush_backend_batch_process();
}

 /**
   *
   */
  function usersMigrateBatch($start=NULL,$end=NULl) {
    // Define batch.
    $batch = array(
      'operations' => array(
        array('start_user_migration', array($start,$end)),
      ),
      'finished' => 'finished_user_migration',
      'title' => t('Users Migartion'),
      'init_message' => t('User Migartion is running.'),
      'progress_message' => t('Migarting users...'),
      'error_message' => t('User Migartion has encountered an error.'),
    );
    return $batch;
  }
  
  function start_user_migration($start = NULL, $end = NULL, &$context){
    $object = new UserBatchMigrateController();
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = $start;
      $context['sandbox']['current_count'] = 0;
      $context['sandbox']['max'] = $end;

      // Collect results to process in the finished callback.
      $context['results']['user_count'] = $context['sandbox']['max'];
    }
    
    if ($context['sandbox']['max'] + 1 > 0) {
        $context['sandbox']['current_count'] = $context['sandbox']['progress'];
        $context['sandbox']['progress']=$context['sandbox']['progress']+10;
        $context['message'] = t('Migrating Users from %start to %end', array('%start' => $context['sandbox']['current_count'], '%end' =>$context['sandbox']['progress']));
        \Drupal::state()->set('user_count', $context['sandbox']['current_count']);
        $object->userMigrate($context['sandbox']['current_count'],10);
    }
    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
     
  }

function finished_user_migration($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Run batch successfully for @user_count.', array('@user_count' => $results['user_count'])));
  }
}

