<?php

namespace Drupal\oauth_service_connector;

use Drupal\Component\Serialization\Json;
use Drupal\user\Entity\User;
use Drupal\Component\Datetime\DateTimePlus;

/**
 * Class OAuthServiceConnect.
 *
 * @package Drupal\oauth_service_connector
 */
class OAuthServiceConnect {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Validate User
   *
   * @param $form_state
   * @return array
   */
  function oauthValidateUser($form_state) {
    $email = $form_state->getValue('mail');
    $headers = getallheaders();
    //$header['Referer'] (http://dev.local.hips.jnj.com/user/register)
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');
    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_validate_email');
    $formData = [
      'mail' => $email
    ];

    $response = self::prepareRequestResponse($url, $headers, $formData, 'post');
    return $response;
  }

  /**
   * Function for creating user
   * @param $form_state
   * @return array
   */
  function oauthCreateUser($form_state, $registrationFields) {
    $headers = getallheaders();
    //$header['Referer'] (http://dev.local.hips.jnj.com/user/register)
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');

    $userProfile = array();
    //default fields
    $userProfile['mail'] = $form_state->getValue('mail');
    $userProfile['password'] = $form_state->getValue('pass');

    //Profile fields
    foreach ($registrationFields as $key => $fields) {
      $userProfile[$fields] = $form_state->getValue($fields)[0]['value'];
    }

    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_create_user');

    $response = self::prepareRequestResponse($url, $headers, $userProfile, 'post');
    return $response;
  }


  /**
   * Function for Updating user Profile
   * @param $form_state
   * @return array
   */
  function oauthUpdateProfile($form_state, $registrationFields) {
    $headers = getallheaders();
    //$header['Referer'] (http://dev.local.hips.jnj.com/user/register)
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');

    $userProfile = array();
    //default fields
    $userProfile['mail'] = $form_state->getValue('mail');
    $account = user_load_by_mail($form_state->getValue('mail'));
    if ($form_state->getValue('pass') != NULL) {
      $userProfile['password'] = $form_state->getValue('pass');
    }
    $userProfile['uuid'] = $account->uuid();
    //Profile fields
    foreach ($registrationFields as $key => $fields) {
      $userProfile[$fields] = $form_state->getValue($fields)[0]['value'];
    }

    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_update_user_profile');
    $response = self::prepareRequestResponse($url, $headers, $userProfile, 'post');
    return $response;
  }

  /**
   * oauthUpdatePassword
   *
   * @param $userProfile
   * @param $email
   * @return array
   */
  function oauthUpdatePassword($userProfile, $email) {
    $headers = getallheaders();
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');

    $account = user_load_by_mail($email);
    $headers['uuid'] = $account->uuid();
    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_update_user_password');
    $response = self::prepareRequestResponse($url, $headers, $userProfile, 'post');
    return $response;
  }

  /**
   * Validate User
   *
   * @param $form_state
   * @return array
   */
  function ValidateAuthentication($form_state) {
    $email = $form_state->getValue('mail');
    $headers = getallheaders();
    //$header['Referer'] (http://dev.local.hips.jnj.com/user/register)
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');
    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_validate_login');
    $formData = [
      'mail' => $email
    ];
    $response = self::prepareRequestResponse($url, $headers, $formData, 'post');
    return $response;
  }


  /**
   * OAuthUserLogin
   *
   * @param $form_state
   * @return array
   */
  function oauthUserLogin($form_state) {
    $headers = getallheaders();
    //$header['Referer'] (http://dev.local.hips.jnj.com/user/register)
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');
    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_user_login');
    $formData = array(
      'mail' => $form_state->getValue('mail'),
      'password' => $form_state->getValue('pass')
    );
    $response = self::prepareRequestResponse($url, $headers, $formData, 'post');
    return $response;
  }

  /**
   * Create a local user
   *
   * @param $form_state
   * @return mixed
   */
  function oauthCreateLocalUser($form_state) {
    $headers = getallheaders();

    //Create a Local User
    $user = User::create();
    //Mandatory settings
    $email = $form_state->getValue('mail');
    $user->setPassword($form_state->getValue('pass'));
    $user->enforceIsNew();
    $user->setEmail($email);
    $user->setUsername($email);//This username must be unique and accept only a-Z,0-9, - _ @ .
    $user->activate();
    $user->set('init', $email);

    //Set Language
    $language_interface = \Drupal::languageManager()->getCurrentLanguage();
    $user->set('langcode', $language_interface->getId());
    $user->set('preferred_langcode', $language_interface->getId());
    $user->set('preferred_admin_langcode', $language_interface->getId());


    //Set timezone
    $timezone = isset($headers['timezone']) ? $headers['timezone'][0] : NULL;
    $defaultTimezone = \Drupal::config('system.date')->get('timezone.default');
    $timezone = isset($timezone) ? $timezone : $defaultTimezone;
    $user->set('timezone', $timezone);

    //User time stamp
    $requestDateTime = isset($headers['usertimestamp']) ? $headers['usertimestamp'][0] : \date('m-d-Y H:i:s');
    $date = DateTimePlus::createFromFormat('m-d-Y H:i:s', $requestDateTime, $timezone);
    $datetime = $date->format('Y-m-d H:i:s');
    $unixTimestamp = strtotime($datetime);
    $user->set('created', $unixTimestamp);

    //Set the same UUID from OAuth website to local
    //loadEntityByUuid($entity_type_id, $uuid);
    $uuid = $form_state->get('uuid');
    if (isset($uuid)) {
      $user->set('uuid', $uuid);
    }

    //Adding default user roles, if any
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');
    $userRoles = $config->get('default_user_roles');
    if (isset($userRoles)) {
      $userRolesArray = explode(',', $userRoles);
      foreach ($userRolesArray as $key => $role) {
        $user->addRole($role);
      }
    }


    // Save has no return value so this cannot be tested.
    // Assume save has gone through correctly.
    $user1 = $user->save();

    \Drupal::logger('user')
      ->notice('New user:  %email.', array(
        '%email' => '<' . $form_state->getValue('mail') . '>'
      ));
    return $user;
  }

  /**
   * OAuthUserLogout
   *
   * @param $user
   * @return array
   */
  function oauthUserLogout($user) {
    $user = user::load($user->id());
    $headers = getallheaders();

    //$header['Referer'] (http://dev.local.hips.jnj.com/user/register)
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');
    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_user_logout');
    $formData = array(
      'mail' => $user->getEmail()
    );
    $headers['uuid'] = $user->uuid();

    $response = self::prepareRequestResponse($url, $headers, $formData, 'post');
    return $response;
  }

  /**
   * @param $url
   * @param $headers
   * @param $formData
   * @param string $method
   * @return array
   */
  public function prepareRequestResponse($url, $headers, $formData, $method = 'post') {
    unset($headers['Authorization']);
    $access_token = \Drupal::service('apigee.connect')
      ->get_apigee_bearer_token();
    $uid = \Drupal::currentUser()->id();
    $data = array(
      'body' => http_build_query($formData, '', '&', PHP_QUERY_RFC3986),
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
        'Authorization' => $access_token
      ],
      'timeout' => 600
    );
    //Unset header Authorization if that is not a Bearer token
    if (isset($headers['Authorization'])) {
      if (strpos($headers['Authorization'], 'Bearer ') != FALSE) {
        unset($headers['Authorization']);
      }
    }
    //Set UUID
    if (isset($headers['uuid'])) {
      $data['headers']['uuid'] = $headers['uuid'];
    }

    if ($uid > 0) {
      $uuid = isset($data['headers']['uuid']) ? $data['headers']['uuid'] : '';
      if ($uuid == '') {
        $user = user::load($uid);
        $data['headers']['uuid'] = $user->uuid();
      }
    }

    //end
    \Drupal::logger('prepareRequest.')->notice(print_r($data, TRUE));
    $response = \Drupal::service('oauth_service.restcall')
      ->restCall($url, $data, $method);

    if ($response['StatusCode'] == 200 OR $response['StatusCode'] == 201) {
      return $response['ResponseBody'];
    }
    else {
      return array(
        $response['ResponseBody'],
      );
    }
  }

  public function getUserProfile($uuid = NULL) {
    $uid = \Drupal::currentUser()->id();
    $user = user::load($uid);
    $headers = getallheaders();

    //$header['Referer'] (http://dev.local.hips.jnj.com/user/register)
    $config = \Drupal::configFactory()
      ->getEditable('oauth_service_connector.OauthConnectorConfig');
    $url = $config->get('oauth_service_api_base_url') . $config->get('oauth_service_user_profile');
    $formData = [];
    $headers['uuid'] = isset($uuid) ? $uuid : $user->uuid();

    $response = self::prepareRequestResponse($url, $headers, $formData, 'get');
    return $response;
  }
}
