<?php
/**
 * actionitemController file
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\node\Entity\Node;

class actionitemController {
  /*
   * List os action item for a user.
   * Status 0 = Active
   * Status 1 = completed
   * Status 2 = Deleted
   * @Return array  of active and completed action items list
   */
  public function user_action_item_list() {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $uid = $headlessAccount->id();

    if ($uid) {
      $db = \Drupal::database();
      $result = $db->select('action_items', 'ai')
        ->fields('ai', array(
          'action_item',
          'uid',
          'display_title',
          'status',
          'notification_date',
          'completed',
          'when_completed',
          'chainId',
          'is_recurrence',
          'recurrence_interval',
          'linkId'
        ))
        ->condition('ai.uid', $uid, '=')
        ->condition('ai.status', 'Deleted', '!=')
        ->execute();
      $i = 0;
      $items = array();
      $status = 0;
      foreach ($result as $values) {
        if ($values->status == 'Active') {
          $status = 1;
        }

        $actualActionItem = Node::load($values->linkId);
        $walkingActionItem = isset($actualActionItem->field_walking_action_item->value) ? $actualActionItem->field_walking_action_item->value : 0;
        $items[$i]['title'] = $values->display_title;
        $items[$i]['notificationDate'] = date_format(date_create($values->notification_date), 'm-d-Y H:i:s');
        $items[$i]['status'] = (int) $status;
        $items[$i]['weight'] = 0;
        $items[$i]['completed'] = (int) $values->completed;
        $items[$i]['userId'] = (int) $values->uid;
        if ($values->when_completed != NULL) {
          $items[$i]['whenCompleted'] = date_format(date_create($values->when_completed), 'm-d-Y H:i:s');
        }
        $items[$i]['chainId'] = (int) $values->chainId;
        $items[$i]['id'] = (int) $values->action_item;
        $items[$i]['is_recurrence'] = isset($values->is_recurrence) ? (int) $values->is_recurrence : 0;
        $items[$i]['recurrence_interval'] = isset($values->recurrence_interval) ? $values->recurrence_interval : 'None';
        $items[$i]['walkingActionItem'] = (int) $walkingActionItem;
        $i++;
      }
      if (count($items) == 0) {
        $items = array(
          'Message' => ('No Action List Item\'s found')
        );
      }
      return new JsonResponse($items, 200);
    }
    else {
      $items = array(
        'Message' => ('No Action List Item\'s found')
      );
      return new JsonResponse($items, 200);
    }
  }

  /*
   * Function to update or delete(soft delete)a single action items
   * @param requestArray
   * @return array of updated action item information
   */
  public function user_action_item(Request $request) {
    $headers = getallheaders();
    if (!$headers['uuid']) {
      $response = array(
        'errorMessage' => 'User Id missing in Header',
      );
      return new JsonResponse($response, 400);
    }
    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $uid = $headlessAccount->id();

    if (isset($userProfile['Error'])) {
      return new JsonResponse(array('errorMessage' => $userProfile['Error']), 400);
    }
    //User time stamp
    $requestDateTime = filter_input(INPUT_SERVER, 'HTTP_USERTIMESTAMP') ? filter_input(INPUT_SERVER, 'HTTP_USERTIMESTAMP') : \date('m-d-Y H:i:s');
    $last_update = DateTimePlus::createFromFormat('m-d-Y H:i:s', $requestDateTime)
      ->format('Y-m-d H:i:s');

    $content = json_decode($request->getContent(), TRUE);
    if ($content['id'] != '' && $uid != '') {
      $action_item = $content['id'];
      //$uid = $content['userId'];
      $display_title = $content['title'];
      $itemStatus = isset($content['status']) ? $content['status'] : 0;
      if ($itemStatus == 0) {
        $status = 'Active';
      }
      else {
        if ($itemStatus == 1) {
          $status = 'Inactive';
        }
        else {
          $status = 'Deleted';
        }
      }
      $notification_date = DateTimePlus::createFromFormat('m-d-Y H:i:s', $content['notificationDate'])
        ->format('Y-m-d H:i:s');
      $completed = isset($content['completed']) ? $content['completed'] : 0;
      $actionItem = \Drupal::database()->select('action_items')
        ->fields('action_items', array('action_item', 'checkins_dates'))
        ->condition('action_item', $action_item)
        ->condition('uid', $uid)
        ->execute()->fetchAssoc();
      if (isset($actionItem['action_item'])) {
        if (isset($content['whenCompleted'])) {
          $when_completed = DateTimePlus::createFromFormat('m-d-Y H:i:s', $content['whenCompleted'])
            ->format('Y-m-d H:i:s');
          if ($actionItem['checkins_dates']) {//If Exiting data, then update the value
            $checkinsArray = isset($actionItem['checkins_dates']) ? unserialize($actionItem['checkins_dates']) : NULL;
            array_push($checkinsArray, $content['whenCompleted']);
            $checkins_dates = serialize($checkinsArray);
          }
          else {//If null ,add a new value
            $checkins_dates = isset($content['whenCompleted']) ? serialize(array($content['whenCompleted'])) : NULL;
          }
        }
        $result = \Drupal::database()->update('action_items')
          ->fields(array(
            'display_title' => $display_title,
            'status' => $status,
            'weight' => isset($content['weight']) ? $content['weight'] : 0,
            'notification_date' => $notification_date,
            'completed' => $completed,
            'when_completed' => isset($when_completed) ? $when_completed : $last_update,
            'last_update' => $last_update,
            'is_recurrence' => isset($content['is_recurrence']) ? (int) $content['is_recurrence'] : 0,
            'recurrence_interval' => isset($content['recurrence_interval']) ? $content['recurrence_interval'] : 'None',
            'checkins_dates' => isset($checkins_dates) ? $checkins_dates : NULL
          ))
          ->condition('action_item', $actionItem['action_item'], '=')
          ->condition('uid', $uid, '=')
          ->execute();
      }
      else {
        $response = array(
          'errorMessage' => t('Error'),
        );
        return new JsonResponse($response, 400);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error'),
      );
      return new JsonResponse($response, 400);
    }
    if ($result) {
      $response = array(
        'id' => (int) $action_item,
        'userId' => (int) $uid,
        'title' => $display_title,
        'status' => isset($itemStatus) ? (int) $itemStatus : 1,
        'notificationDate' => date_format(date_create($notification_date), 'm-d-Y H:i:s'),
      );
      return new JsonResponse($response, 200);
    }
    else {
      $response = array(
        'errorMessage' => t('Error'),
      );
      return new JsonResponse($response, 400);
    }

  }

}