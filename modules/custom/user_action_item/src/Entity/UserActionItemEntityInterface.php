<?php

namespace Drupal\user_action_item\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining User action item entity entities.
 *
 * @ingroup user_action_item
 */
interface UserActionItemEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the User action item entity name.
   *
   * @return string
   *   Name of the User action item entity.
   */
  public function getName();

  /**
   * Sets the User action item entity name.
   *
   * @param string $name
   *   The User action item entity name.
   *
   * @return \Drupal\user_action_item\Entity\UserActionItemEntityInterface
   *   The called User action item entity entity.
   */
  public function setName($name);

  /**
   * Gets the User action item entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the User action item entity.
   */
  public function getCreatedTime();

  /**
   * Sets the User action item entity creation timestamp.
   *
   * @param int $timestamp
   *   The User action item entity creation timestamp.
   *
   * @return \Drupal\user_action_item\Entity\UserActionItemEntityInterface
   *   The called User action item entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the User action item entity published status indicator.
   *
   * Unpublished User action item entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the User action item entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a User action item entity.
   *
   * @param bool $published
   *   TRUE to set this User action item entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\user_action_item\Entity\UserActionItemEntityInterface
   *   The called User action item entity entity.
   */
  public function setPublished($published);

}
