<?php

namespace Drupal\learning_center\Service;

/**
 * LearningBookmarks file.
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any
 * means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

/**
 * CRUD Methods for the bookmark funcitonality.
 */
class LearningBookmarks {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * IsBookmark.
   *
   * @Return int bookmark ID in case it exists or FALSE otherwise
   */
  public function isBookmark($uid, $nid) {
    $query = \Drupal::database()->select('learning_center_bookmarks', 'lcb');
    $query->fields('lcb', ['bid']);
    $query->condition('lcb.uid', $uid);
    $query->condition('lcb.nid', $nid);
    $result = $query->execute()->fetchAll();
    if (!empty($result[0]->bid)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get bookmark list for a certain UID.
   *
   * @Return array of bookmark data or FALSE in case no bookmarks are found
   */
  public function getBookmarks($uid) {

    $db = \Drupal::database();
    $result = $db->select('learning_center_bookmarks', 'lcb')
      ->fields('lcb', array(
        'bid',
        'uuid',
        'uid',
        'nid',
      ))
      ->condition('lcb.uid', $uid, '=')
      ->execute();
    $i = 0;
    $items = array();
    $status = 0;

    foreach ($result as $bookmark) {
      $items[$i]['bid'] = $bookmark->bid;
      $items[$i]['uuid'] = $bookmark->uuid;
      $items[$i]['uid'] = $bookmark->uid;
      $items[$i]['nid'] = $bookmark->nid;
      $i++;
    }
    if (!empty($items)) {
      return $items;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Creates a bookmark from a set of user and node identifiers.
   *
   * @Return Boolean
   */
  public function addBookmark($uuid, $uid, $nid) {

    if (!(\Drupal::service('learning_center.bookmarks')->isBookmark($uid, $nid))) {
      $db = \Drupal::database();
      $query = $db->insert('learning_center_bookmarks');
      $query->fields([
        'uuid',
        'uid',
        'nid',
      ]);
      $query->values([
        $uuid,
        $uid,
        $nid,
      ]);
      $query->execute();

      if ($query) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return TRUE;
    }
  }

  /**
   * Removes a bookmark from a pair of user and node identifiers.
   *
   * @Return the number of bookmarks affected or FALSE
   */
  public function deleteBookmark($uid, $nid) {
    $query = \Drupal::database()->delete('learning_center_bookmarks');
    $query->condition('uid', $uid, '=');
    $query->condition('nid', $nid, '=');
    $query->execute();
    if ($query) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
