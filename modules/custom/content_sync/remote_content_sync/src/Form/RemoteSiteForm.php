<?php

namespace Drupal\remote_content_sync\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Uri;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class RemoteSiteForm.
 *
 * @package Drupal\remote_content_sync\Form
 */
class RemoteSiteForm extends EntityForm {

  protected $entityTypeManager;

  protected $httpClient;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManager $entity_type_manager, Client $http_client) {
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $remote_site = $this->entity;
    $uri = $remote_site->uri();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $remote_site->label(),
      '#description' => $this->t("Label for the Remote site."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $remote_site->id(),
      '#machine_name' => [
        'exists' => '\Drupal\remote_content_sync\Entity\RemoteSite::load',
      ],
      '#disabled' => !$remote_site->isNew(),
    ];

    $form['uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full URL'),
      '#description' => $this->t('Please type the full URL including the protocol and trailing slash: http://example.com/'),
      '#required' => TRUE,
      '#default_value' => (string) $uri->withUserInfo(NULL),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $remote_site->username(),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
    ];

    $form['auto_sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Automatic Synchronization'),
      '#description' => $this->t('Check for synchronize the content with the cron execution.'),
      '#default_value' => $remote_site->isAutoSyncEnabled(),
    ];

    $form['field_map'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Field map'),
      '#description' => $this->t('This should be written one mapping on each line, separated by a pipe with the local field name first: local_name|remote_name'),
      '#default_value' => $this->fieldMapToString($remote_site->getFieldMap()),
    ];

    $form['operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#options' => ['or' => 'OR', 'and' => 'AND'],
      '#default_value' => $remote_site->get('operator'),
    ];

    $filters = $remote_site->filters();
    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filters'),
      '#tree' => TRUE,
    ];
    $form['filters'][0] = [
      '#type' => 'details',
      '#title' => $this->t('Node Type Filter'),
      '#tree' => TRUE,
    ];
    $form['filters'][0]['id'] = [
      '#type' => 'value',
      '#value' => 'node_type',
    ];
    $form['filters'][0]['operator'] = [
      '#type' => 'value',
      '#value' => 'or',
    ];
    $options = [];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }
    $form['filters'][0]['data'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Types'),
      '#options' => $options,
      '#default_value' => (isset($filters[0]['data'])) ? $filters[0]['data'] : [],
    ];

    $form['filters'][1] = [
      '#type' => 'details',
      '#title' => $this->t('Taxonomy Filter'),
      '#tree' => TRUE,
    ];
    $form['filters'][1]['id'] = [
      '#type' => 'value',
      '#value' => 'taxonomy_term',
    ];
    $form['filters'][1]['operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#options' => ['or' => 'OR', 'and' => 'AND'],
      '#default_value' => $filters[1]['operator'],
    ];
    $terms = [];
    if (!empty($filters[1]['data'])) {
      foreach ($filters[1]['data'] as $term) {
        $terms += $this->entityTypeManager->getStorage('taxonomy_term')
          ->loadByProperties(array('uuid' => $term));
      }
    }
    $form['filters'][1]['data'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Terms'),
      '#default_value' => $terms,
      '#description' => $this->t('Enter a comma separated list of term names.'),
      '#target_type' => 'taxonomy_term',
      '#validate_reference' => TRUE,
      '#tags' => TRUE,
    ];

    $form['filters'][2] = [
      '#type' => 'details',
      '#title' => $this->t('Entity Type Filter'),
      '#tree' => TRUE,
    ];
    $form['filters'][2]['id'] = [
      '#type' => 'value',
      '#value' => 'entity_type',
    ];
    $form['filters'][2]['operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#options' => ['or' => 'OR', 'and' => 'AND'],
      '#default_value' => $filters[2]['operator'],
    ];
    $options = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      /* @var $definition \Drupal\Core\Entity\EntityTypeInterface */
      if ($definition instanceof ContentEntityTypeInterface) {
        $options[$definition->id()] = $definition->getLabel();
      }
    }
    $form['filters'][2]['data'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity Types'),
      '#options' => $options,
      '#default_value' => (isset($filters[2]['data'])) ? $filters[2]['data'] : [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /* @var $remote_site \Drupal\remote_content_sync\Entity\RemoteSite */
    $remote_site = $this->entity;

    $uri = new Uri($form_state->getValue('uri'));
    $uri = $uri->withUserInfo(
      $form_state->getValue('username'),
      $form_state->getValue('password')
    );
    $remote_site->set('remote_uri', (string) $uri);

    $remote_site->set('field_map', $this->stringToFieldMap($form_state->getValue('field_map')));

    $filters = $form_state->getValue('filters');

    // Node Filter.
    $filters[0]['data'] = array_diff($filters[0]['data'], [0]);

    // Taxonomy term Filter.
    $terms = [];
    if (!empty($filters[1]['data'])) {
      foreach ($filters[1]['data'] as $term) {
        $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($term['target_id']);
        $terms[] = $term->uuid();
      }
    }
    $filters[1]['data'] = $terms;

    // Entity Type Filter.
    $filters[2]['data'] = array_diff($filters[2]['data'], [0]);

    $remote_site->set('filters', $filters);
    $status = $remote_site->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Remote site.', [
          '%label' => $remote_site->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Remote site.', [
          '%label' => $remote_site->label(),
        ]));
    }

    $string_uri = (string) $uri->withUserInfo(NULL);

    if (!empty($string_uri)) {
      try {
        $this->httpClient->get($string_uri . 'content_sync/test-connection');
      }
      catch (RequestException $e) {
        drupal_set_message($this->t('There was an error connecting to @uri: request returned a code @code.', ['@uri' => $string_uri, '@code' => $e->getCode()]), 'warning');
      }
      if (!empty($remote_site->username()) && !empty($remote_site->password())) {
        try {
          $this->httpClient->get($string_uri . 'content_sync/test-connection-auth', [
            'auth' => [$remote_site->username(), $remote_site->password()],
          ]);
        }
        catch (RequestException $e) {
          drupal_set_message($this->t('There was an error connecting to @uri: request returned a code @code.', ['@uri' => $string_uri, '@code' => $e->getCode()]), 'warning');
        }

      }
    }

    $form_state->setRedirectUrl($remote_site->urlInfo('collection'));
  }

  /**
   * Converts a field map array to it's string equivalent.
   */
  protected function fieldMapToString($field_map_array) {
    $lines = [];
    if (is_array($field_map_array)) {
      foreach ($field_map_array as $field_map_item) {
        $lines[] = "{$field_map_item['local_name']}|{$field_map_item['remote_name']}";
      }
    }
    return implode("\n", $lines);
  }

  /**
   * Converts a field map string to array.
   */
  protected function stringToFieldMap($field_map_string) {
    $field_map_array = [];
    if (!empty($field_map_string)) {
      $field_map_exploded = explode("\n", $field_map_string);
      foreach ($field_map_exploded as $field_map_row) {
        $field_map_row_exploded = explode("|", $field_map_row, 2);
        $field_map_array[] = [
          'local_name' => $field_map_row_exploded[0],
          'remote_name' => $field_map_row_exploded[1],
        ];
      }
    }
    return $field_map_array;
  }

}
