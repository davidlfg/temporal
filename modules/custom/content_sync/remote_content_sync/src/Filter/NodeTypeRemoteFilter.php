<?php

namespace Drupal\remote_content_sync\Filter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\node\Entity\Node;

/**
 * Node Type Remote Filter class.
 */
class NodeTypeRemoteFilter extends RemoteFilter {

  /**
   * {@inheritdoc}
   */
  public function filter(ContentEntityInterface $entity) {
    if ($entity instanceof Node) {
      return in_array($entity->getType(), $this->configuration['types']);
    }
    return FALSE;
  }

}
