<?php
/**
 * FieldCollectionFields file to handle all field collection fields in content chain
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Service;

use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\taxonomy\Entity\Term;
use Drupal\file\Entity\File;


class FieldCollectionFields {
  /**
   * Function to retrieve all form fields
   * @param $userId
   * @param $field_collection_item
   * @param $i
   * @param $items
   * @param $keyValueArray
   * @return mixed
   */
  public function getFieldCollectionFields($userId, $field_collection_item, $i, $items, $keyValueArray) {
    $j = 0;
    foreach ($field_collection_item as $key => $value) {
      global $base_url;
      $items['contentElements'][$i]['action']['options'][$j]['order'] = (int) $key;
      $fcId = $value->value;
      $fc = FieldCollectionItem::load($fcId);
      $skipArray = array(
        'item_id',
        'host_type',
        'uuid',
        'revision_id',
        'field_name'
      );

      foreach ($fc as $fcKey => $field) {
        if (!in_array($fcKey, $skipArray)) {
          $fcKey = str_replace(array('next_chain_id'), array('nextChainId'), str_replace(array(
            'field_',
            'button_',
            '_new',
            'checkbox_',
            'free_form_text_',
            'freeform_text_'
          ), '', $fcKey));
          if ($fcKey == 'value') {
            if (isset($fc->field_label->value)) {//For form fields already selected
              $items['contentElements'][$i]['action']['options'][$j][$fcKey] = isset($keyValueArray[$fc->field_label->value]) ? $keyValueArray[$fc->field_label->value] : '';
              $items['contentElements'][$i]['action']['options'][$j]['selected'] = isset($keyValueArray[$fc->field_label->value]) ? 1 : 0;
            }
            else {
              if (isset($fc->field_free_form_text_label->value)) {//Free form value
                $items['contentElements'][$i]['action']['options'][$j][$fcKey] = isset($keyValueArray[$fc->field_free_form_text_label->value]) ? $keyValueArray[$fc->field_free_form_text_label->value] : '';
                $items['contentElements'][$i]['action']['options'][$j]['selected'] = isset($keyValueArray[$fc->field_free_form_text_label->value]) ? 1 : 0;
              }
              else {//For other fields selected
                if (array_key_exists($field->value, $keyValueArray)) {
                  $items['contentElements'][$i]['action']['options'][$j]['selected'] = 1;
                }
                else {//Field are not selected
                  $items['contentElements'][$i]['action']['options'][$j]['selected'] = 0;
                }
                $items['contentElements'][$i]['action']['options'][$j][$fcKey] = isset($field->value) ? $field->value : '';
              }
            }
          }
          else {
            //$items['contentElements'][$i]['action']['options'][$j]['selected'] = 0;
            if ($fcKey == 'feedback') {
              $feedback = str_replace(array(
                "src=\"/",
              ), "src=\"$base_url/", str_replace(array(
                "\n",
                "\t",
                "\r"
              ), "", htmlspecialchars_decode($field->value, ENT_QUOTES)));
              $items['contentElements'][$i]['action']['options'][$j][$fcKey] = isset($feedback) ? $feedback : '';
            }
            else {
              if ($fcKey == 'nextChainId') {
                $chainId = \Drupal::database()
                  ->select('field_collection_item__field_next_chain_id', 'nc')
                  ->fields('nc', array('field_next_chain_id_target_id'))
                  ->condition('nc.entity_id', $fc->item_id->value)
                  ->execute()
                  ->fetchField();
                if ($chainId != '') {
                  $items['contentElements'][$i]['action']['options'][$j][$fcKey] = isset($chainId) ? (int) $chainId : 0;
                  $items['contentElements'][$i]['DecisionTree'] = 1;
                  if ($items['contentElements'][$i]['action']['options'][$j]['selected'] == 1) {
                    $items['selectedChain'] = (int) $chainId;
                  }
                }
                else {
                  $items['contentElements'][$i]['DecisionTree'] = 0;
                }
              }
              else {
                $items['contentElements'][$i]['action']['options'][$j][$fcKey] = isset($field->value) ? $field->value : '';
              }
            }
          }
        }
      }
      $j++;
    }
    return $items;
  }

  /**
   * Function to retrieve voice over field collection fields
   * @param $items
   * @param $voiceOver
   * @param $i
   * @param $field_collection_item
   * @return mixed
   */
  function getVoiceOverFieldCollectionFields($items, $voiceOver, $i, $field_collection_item) {
    $j = 0;
    global $base_url;
    foreach ($field_collection_item as $key => $value) {
      $fcId = $value->value;
      $fc = FieldCollectionItem::load($fcId);
      $skipArray = array(
        'item_id',
        'host_type',
        'uuid',
        'revision_id',
        'field_name'
      );
      foreach ($fc as $fcKey => $field) {
        if (!in_array($fcKey, $skipArray)) {
          $fcKey = str_replace(array(
            'slide_start_time',
            'slide_end_time',
            'slide_text'
          ), array('startTime', 'endTime', 'cardText'), str_replace(array(
            'field_',
            'button_',
            '_new',
          ), '', $fcKey));

          $items['contentElements'][$i][$voiceOver]['voiceOverCards'][$j]['order'] = (int) $key;
          if ($fcKey == 'cardText') {
            $text = str_replace(array(
              "src=\"/",
            ), "src=\"$base_url/", str_replace(array(
              "\n",
              "\t",
              "\r"
            ), "", htmlspecialchars_decode($field->value, ENT_QUOTES)));
            $items['contentElements'][$i][$voiceOver]['voiceOverCards'][$j][$fcKey] = isset($text) ? $text : '';
          }
          else {
            if ($fcKey == 'slide_image') {
              if (isset($fc->field_slide_image->target_id)) {
                $image = File::load($fc->field_slide_image->target_id);
                $imageUri = $image->uri->value;
                if (isset($imageUri)) {
                  $items['contentElements'][$i][$voiceOver]['voiceOverCards'][$j]['image']['url'] = file_create_url($imageUri);
                  $items['contentElements'][$i][$voiceOver]['voiceOverCards'][$j]['image']['fileName'] = $image->filename->value;
                  $items['contentElements'][$i][$voiceOver]['voiceOverCards'][$j]['image']['format'] = $image->filemime->value;
                  $items['contentElements'][$i][$voiceOver]['voiceOverCards'][$j]['image']['fileId'] = $fc->field_slide_image->target_id;
                }
              }
            }
            else {
              $items['contentElements'][$i][$voiceOver]['voiceOverCards'][$j][$fcKey] = isset($field->value) ? (int) $field->value : '';
            }
          }
        }
      }
      $j++;
    }
    return $items;
  }

  /**
   * Function to get taxonomy term name from term id
   * @param $tid
   * @return mixed
   */
  public function getTaxonomyTerm($tid) {
    $termName = Term::load($tid)->get('name')->value;
    return $termName;
  }
}