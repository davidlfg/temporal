<?php

namespace Drupal\remote_content_sync;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Resolves remote sites.
 */
class RemoteSiteResolver {

  protected $remoteSites;
  protected $entityTypeManager;

  /**
   * RemoteSiteResolver constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets the remote sites for a given entity.
   */
  public function getRemoteSites(ContentEntityInterface $entity) {
    if (!$this->remoteSites) {
      $this->remoteSites = $this->entityTypeManager->getStorage('remote_site')
        ->loadMultiple();
    }
    $remote_sites = [];
    foreach ($this->remoteSites as $remote_site) {
      if ($remote_site->filter($entity)) {
        $remote_sites[$remote_site->id()] = $remote_site;
      }
    }
    return $remote_sites;
  }

}
