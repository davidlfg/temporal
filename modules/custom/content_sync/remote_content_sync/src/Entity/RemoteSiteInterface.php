<?php

namespace Drupal\remote_content_sync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\remote_content_sync\Filter\RemoteFilterInterface;

/**
 * Provides an interface for defining Remote site entities.
 */
interface RemoteSiteInterface extends ConfigEntityInterface, RemoteFilterInterface {

  /**
   * Returns the uri.
   */
  public function uri();

  /**
   * Returns the username.
   */
  public function username();

  /**
   * Returns the password.
   */
  public function password();

  /**
   * Getter for the field_map property.
   */
  public function getFieldMap();

  /**
   * Returns the filters.
   */
  public function filters();

  /**
   * Gets a remote filter by id.
   */
  public function getRemoteFilter($id);

  /**
   * Gets all remote filters.
   */
  public function getRemoteFilters();

  /**
   * Checks if the Automatic Synchronization is enabled for this remote.
   */
  public function isAutoSyncEnabled();

}
