<?php

namespace Drupal\user_action_item;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of User action item entity entities.
 *
 * @ingroup user_action_item
 */
class UserActionItemEntityListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
      $header['id'] = $this->t('User action item entity ID');
      $header['uid'] = $this->t('User Email (ID)');
      $header['display_title'] = $this->t('Display Title');
      $header['status'] = $this->t('Status');
      $header['action_item_status'] = $this->t('Action Item Status');
      $header['created'] = $this->t('Created');
      $header['changed'] = $this->t('Changed');
      $header['weight'] = $this->t("Weight");
      $header['reference_date'] = $this->t('Reference Date');
      $header['daysfromreference_date'] = $this->t('Days From Reference Date');
      $header['notification_date'] = $this->t('Notification Date');
      $header['completed'] = $this->t('Completed');
      $header['when_completed'] = $this->t('When Completed');
      $header['chainId'] = $this->t('Chain Id');
      $header['linkId'] = $this->t('Link Id');
      $header['is_recurrence'] = $this->t('Is Recurrence');
      $header['recurrence_interval'] = $this->t('Recurrence Interval');
      $header['checkins_date'] = $this->t('Checkins Date');
      $header['category'] = $this->t('Category');
      $header['notes'] = $this->t('Notes');
      $header['show_notification'] = $this->t('Show Notification');
      $header['hpb_recurrence_interval'] = $this->t('HPB Recurrence Interval');

      return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
      /* @var $entity \Drupal\user_action_item\Entity\UserActionItemEntity */
      $row['id'] = $entity->id();
      $row['uid'] = $entity->uid->value;
      $row['name'] = $entity->name->value;
      $row['status'] = $entity->status->value;
      $row['action_item_status'] = $entity->name->value;
      $row['created'] = date('Y-m-d H:i:s', $entity->created->value);
      $row['changed'] = date('Y-m-d H:i:s', $entity->changed->value);
      $row['weight'] = $entity->weight->value;
      $row['reference_date'] = date($entity->reference_date->value);
      $row['daysfromreference_date'] = $entity->daysfromreference_date->value;
      $row['notification_date'] = date('Y-m-d H:i:s', $entity->notification_date->value);
      $row['completed'] = $entity->completed->value;
      $row['when_completed'] = date('Y-m-d H:i:s', $entity->when_completed->value);
      $row['chainId'] = $entity->chainId->value;
      $row['linkId'] = $entity->linkId->value;
      $row['is_recurrence'] = $entity->is_recurrence->value;
      $row['recurrence_interval'] = $entity->recurrence_interval->value;
      $row['checkins_date'] = date('Y-m-d H:i:s', $entity->checkins_date->value);
      $row['category'] = $entity->category->value;
      $row['notes'] = $entity->notes->value;
      $row['show_notification'] = $entity->show_notification->value;
      $row['hpb_recurrence_interval'] = $entity->hpb_recurrence_interval->value;

      return $row + parent::buildRow($entity);
  }

}
