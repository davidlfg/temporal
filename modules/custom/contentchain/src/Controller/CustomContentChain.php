<?php
/**
 * Created by PhpStorm.
 * User: rabithk
 * Date: 8/2/16
 * Time: 9:57 AM
 */

namespace Drupal\contentchain\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
Use Drupal\user_management\Service\getTaxonomyTerm;
use Drupal\user\Entity\User;
use Drupal\contentchain\Service\chainContent;
use Drupal\Component\Datetime\DateTimePlus;


class CustomContentChain {
  /**
   * Function for creating content chain for onboarding amd reonboarding.
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function contentChain() {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }

    $type = isset($headers['type']) ? $headers['type'] : 'onboarding'; //onboarding , reonboarding or account

    $typeArray = array("onboarding", "reonboarding", "account");
    if (!in_array($type, $typeArray)) {
      $response = array(
        'errorMessage' => t('Wrong Chain Type.'),
      );
      return new JsonResponse($response, 400);
    }
    if ($user['field_surgery_date'] == '') {
      return new JsonResponse(array('errorMessage' => 'Error validating Surgery Date. Please Update your Surgery Date.'), 400);
    }
    $date = DateTimePlus::createFromFormat('m-d-Y', $user['field_surgery_date']);
    $userSurgeryDate = $date->format('Y-m-d');

    $now = date('Y-m-d');
    $chainContent = new chainContent();
    $currentWeek = $chainContent->weekDifference($userSurgeryDate, $now);

    if (strtotime($userSurgeryDate) > strtotime($now)) {
      $beforeSurgery = 1;//'Before surgery';
    }
    else {
      $beforeSurgery = 0; //'After surgery';
    }

    $config = \Drupal::configFactory()
      ->getEditable('contentchain.ContentChainConfig');
    //Check Default JointType
    $taxonomyTerm = new getTaxonomyTerm();
    $application = $taxonomyTerm->listTaxonomyTerm('application');
    $defaultType = array_search('Knee Replacement Surgery', $application);
    //End check
    $jointType = isset($user['field_joint_replacement_type']) ? $user['field_joint_replacement_type'] : $defaultType;
    switch ($jointType) {
      case '9':
        $jType = 'knees';
        break;
      case '10':
        $jType = 'hips';
        break;
      default:
        $jType = 'knees';
        break;
    }
    $contentIds = explode(",", $config->get($jType . '_' . $type . 'Id'));

    if (count($contentIds) == 0) {
      $response = array(
        'errorMessage' => t('Wrong content Id in configuration or NULL value provided.'),
      );
      return new JsonResponse($response, 400);
    }
    if (count($contentIds) > 0) {
      $chainContent = array();
      foreach ($contentIds as $key => $value) {
        $chainContent[$key] = self::getChainContent($value);
        $chainContent[$key]['order'] = $key;
      }
    }
    else {
      $chainContent = self::getChainContent($contentIds);
    }

    //For on-boarding :- Add an extra text card for a > 4 weeks to surgery user
    if ($type == 'onboarding' and $beforeSurgery == 1) {
      $linkCount = count($chainContent[0]['contentElements']);
      $newChain = self::getChainContent($config->get($jType . '_' . $type . '_feedbackId'));
      if ($currentWeek > 4) {
        $chainContent[0]['contentElements'][$linkCount] = $newChain['contentElements'][0];
      }
      else {
        $chainContent[0]['contentElements'][$linkCount] = $newChain['contentElements'][1];
      }
      $chainContent[0]['contentElements'][$linkCount]['order'] = $linkCount;
    }
    return new JsonResponse($chainContent, 200);
  }


  /**
   * @param $ContentChainId
   * @return mixed
   */
  private function getChainContent($ContentChainId) {
    $headers = getallheaders();
    global $base_url;
    $contentChainUrl = $base_url . '/content/' . $ContentChainId;
    try {
      $response = \Drupal::httpClient()->get($contentChainUrl,
        array(
          'headers' => array(
            'uuid' => $headers['uuid'],
          )
        ));
      $data = json_decode($response->getBody()->getContents(), TRUE);
      return $data;

    } catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode() == 400) {
        \Drupal::logger('getChainContent')
          ->notice('Error: ' .
            Json::decode($e->getResponse()->getBody()->getContents()));
      }
      else {
        \Drupal::logger('getChainContent')
          ->error('An error occurred"' .
            $ContentChainId . '"');
      }
    }
  }
}