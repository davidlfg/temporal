<?php

namespace Drupal\behaviour_science_data;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of BCT Bucket entities.
 *
 * @ingroup behaviour_science_data
 */
class BCTBucketListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('BCT Bucket ID');
    $header['bct_Bucket'] = $this->t('BCT Bucket');
    $header['content_chain_id'] = $this->t('content Chain Id');
    $header['link_id'] = $this->t('Link Id');
    $header['bct_status'] = $this->t('BCT Status');
    $header['created'] = $this->t('Created');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\behaviour_science_data\Entity\BCTBucket */
    $row['id'] = $entity->id();
    $row['bct_Bucket'] = $entity->bct_Bucket->value;
    $row['content_chain_id'] = $entity->get('content_chain_id')->target_id;
    $row['link_id'] = $entity->get('link_id')->target_id;
    $row['bct_status'] = $entity->bct_status->value;
    $row['created'] = date('Y-m-d H:i:s', $entity->created->value);
    return $row + parent::buildRow($entity);
  }

}
