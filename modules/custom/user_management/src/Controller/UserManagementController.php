<?php

namespace Drupal\user_management\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;


/**
 * Class UserManagementController.
 *
 * @package Drupal\user_management\Controller
 */
class UserManagementController extends ControllerBase {

  /**
   * Registration.
   *
   * @return string
   *   Return Hello string.
   */
  public function registration(Request $request) {
    $headers = $request->headers->all();
    //Validate Content
    $content = \Drupal::service('user_management.user')
      ->getRequestContent($request->getContent(), $headers);
    if (isset($content['Error'])) {
      return new JsonResponse(array('Error' => $content['Error']), 400);
    }

    if ($content['mail'] == NULL OR $content['password'] == NULL) {
      $response = array(
        'errorMessage' => t('Email-id or Password is empty'),
      );
      return new JsonResponse($response, 400);
    }
    //Validate Email for existing user
    $email = isset($content['mail']) ? $content['mail'] : NULL;
    if (isset($email)) {
      $response = \Drupal::service('user_management.user')
        ->validateEmail($content, $headers);
      if (isset($response['Error'])) {
        return new JsonResponse(array('Error' => $response['Error']), 400);
      }
      //Validate User
      if ($response['uuid'] == '') {
        //User Does not exist with email id , New User Registration
        $userProfile = \Drupal::service('user_management.user')
          ->createUser($content, $headers);
        if (count($userProfile) > 0) {//New user or error
          if (isset($userProfile['Error'])) {
            return new JsonResponse(array('Error' => $userProfile['Error']), 400);
          }
          return new JsonResponse($userProfile, 200);
        }
        else {
          return new JsonResponse(array('Error' => 'New User Registration Error'), 400);
        }
      }
      else {
        $valid = \Drupal\Component\Uuid\Uuid::isValid($response['uuid']);
        if ($valid and $response['uuid'] == $content['uuid']) {
          //Existing user , Update Profile
          $userProfile = \Drupal::service('user_management.user')
            ->updateProfile($content, $headers);
          if (count($userProfile) > 0) {//User Profile or error
            if (isset($userProfile['Error'])) {
              return new JsonResponse(array('Error' => $userProfile['Error']), 400);
            }
            return new JsonResponse($userProfile, 200);
          }
          else {
            return new JsonResponse(array('Error' => 'Error Updating User Profile'), 400);
          }
        }
        else {
          return new JsonResponse(array('Error' => 'Error validating Email id.'), 400);
        }
      }

    }

  }


  /**
   * Update_profile.
   *
   * @return string
   *   Return Hello string.
   */
  public function update_profile(Request $request) {
    $headers = $request->headers->all();
    $content = \Drupal::service('user_management.user')
      ->getRequestContent($request->getContent(), $headers);

    if (isset($content['Error'])) {
      return new JsonResponse(array('Error' => $content['Error']), 400);
    }
    $headers = $request->headers->all();
    if (!isset($headers['uuid'])) {
      return new JsonResponse(array('Error' => 'Error validating userId'), 400);
    }

    if ($headers['uuid'][0]) {
      //user UpdateProfile
      $response = \Drupal::service('user_management.user')
        ->updateProfile($content, $headers);
      if (isset($response['Error'])) {
        return new JsonResponse(array('Error' => $response['Error']), 400);
      }
      else {
        return new JsonResponse($response, 200);
      }
    }
    else {
      return new JsonResponse(array('Error' => 'Error validating userId'), 400);
    }
  }

  /**
   * Get_profile.
   *
   * @return string
   *   Return Hello string.
   */
  public function get_profile(Request $request) {
    $headers = $request->headers->all();
    if ($headers['uuid'][0]) {
      //user UpdateProfile
      $response = \Drupal::service('user_management.user')
        ->getUserProfile($headers['uuid'][0]);
      if (isset($response['Error'])) {
        return new JsonResponse(array('Error' => $response['Error']), 400);
      }
      else {
        return new JsonResponse($response, 200);
      }
    }
    else {
      return new JsonResponse(array('Error' => 'Error validating userId'), 400);
    }
  }

  /**
   * Update_password.
   *
   * @return string
   *   Return Hello string.
   */
  public function update_password(Request $request) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: update-password')
    ];
  }

  /**
   * Forget_password.
   *
   * @return string
   *   Return Hello string.
   */
  public function forget_password(Request $request) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: forget_password')
    ];
  }

}
