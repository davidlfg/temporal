<?php

namespace Drupal\user_action_item\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the User action item entity entity.
 *
 * @ingroup user_action_item
 *
 * @ContentEntityType(
 *   id = "user_action_item_entity",
 *   label = @Translation("User action item entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\user_action_item\UserActionItemEntityListBuilder",
 *     "views_data" = "Drupal\user_action_item\Entity\UserActionItemEntityViewsData",
 *     "translation" = "Drupal\user_action_item\UserActionItemEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\user_action_item\Form\UserActionItemEntityForm",
 *       "add" = "Drupal\user_action_item\Form\UserActionItemEntityForm",
 *       "edit" = "Drupal\user_action_item\Form\UserActionItemEntityForm",
 *       "delete" = "Drupal\user_action_item\Form\UserActionItemEntityDeleteForm",
 *     },
 *     "access" = "Drupal\user_action_item\UserActionItemEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\user_action_item\UserActionItemEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "user_action_item_entity",
 *   data_table = "user_action_item_entity_field_data",
 *   translatable = TRUE,
  *   admin_permission = "administer user action item entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/user_action_item_entity/{user_action_item_entity}",
 *     "add-form" = "/admin/structure/user_action_item_entity/add",
 *     "edit-form" = "/admin/structure/user_action_item_entity/{user_action_item_entity}/edit",
 *     "delete-form" = "/admin/structure/user_action_item_entity/{user_action_item_entity}/delete",
 *     "collection" = "/admin/structure/user_action_item_entity",
 *   },
 *   field_ui_base_route = "user_action_item_entity.settings"
 * )
 */
class UserActionItemEntity extends ContentEntityBase implements UserActionItemEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the User action item entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Display Title'))
      ->setDescription(t('The name of the User action item entity entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['status'] = BaseFieldDefinition::create('boolean')
          ->setLabel(t('Publishing status'))
          ->setDescription(t('A boolean indicating whether the Video logger entity is published.'))
          ->setDefaultValue(TRUE);

    $fields['action_item_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Action Item Status'))
      ->setDescription(t('The status of the User action item entity.'))
        ->setDisplayOptions('view', array(
            'type' => 'string',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'string',
            'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Weight'))
        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'string',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'label' => 'above',
          'type' => 'integer',
          'weight' => -4,
            ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['reference_date'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Reference Date'))

        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'string',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'label' => 'above',
            'type' => 'string',
            'weight' => -4,
        ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['daysfromreference_date'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Days From Reference Date'))
        ->setDefaultValue('3')
          ->setDisplayOptions('view', array(
              'label' => 'above',
              'type' => 'integer',
              'weight' => -4,
          ))
          ->setDisplayOptions('form', array(
              'label' => 'above',
              'type' => 'integer',
              'weight' => -4,
          ))
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);


    $fields['notification_date'] = BaseFieldDefinition::create('timestamp')
        ->setLabel(t('Notification Date'));

    $fields['completed'] = BaseFieldDefinition::create('boolean')
        ->setLabel('Completed')
        ->setDefaultValue(FALSE)
        ->setDisplayOptions('view', array(
            'type' => 'boolean_checkbox',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'boolean_checkbox',
            'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);


    $fields['when_completed'] = BaseFieldDefinition::create('timestamp')
        ->setLabel('When Completed');

    $fields['last_updated'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Last Updated'));

    $fields['chainId'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Chain ID'))
        ->setDefaultValue('0')
        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'integer',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'label' => 'above',
            'type' => 'integer',
            'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['linkId'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Link ID'))
        ->setDefaultValue('0')
        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'integer',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'integer',
            'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['is_recurrence'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Is Recurrence'))
        ->setDefaultValue(FALSE)
        ->setDisplayOptions('view', array(
            'type' => 'boolean_checkbox',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'boolean_checkbox',
            'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['recurrence_interval'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Recurrence Interval'))
        ->setDefaultValue('0')

        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'integer',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'integer',
            'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['checkins_date'] = BaseFieldDefinition::create('timestamp')
        ->setLabel(t('Checkins Date'));

    $fields['category'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Category'))
        ->setDisplayOptions('view', array(
            'label' => 'above',
            'type' => 'string',
            'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
            'type' => 'string',
            'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    $fields['notes'] = BaseFieldDefinition::create('string')
        ->setLabel('Notes')
        ->setDefaultValue('')
          ->setDisplayOptions('view', array(
              'label' => 'above',
              'type' => 'string',
              'weight' => -4,
          ))
          ->setDisplayOptions('form', array(
              'type' => 'string',
              'weight' => -4,
          ))
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

    $fields['show_notification'] = BaseFieldDefinition::create('boolean')
        ->setLabel('Show Notification')
        ->setDefaultValue(TRUE)
        ->setDisplayOptions('view', array(
            'type' => 'boolean_checkbox',
            'weight' => -4,))
        ->setDisplayOptions('form', array(
            'type' => 'boolean_checkbox',
            'weight' => -4,
        ))

        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);


      $fields['hpb_recurrence_interval'] = BaseFieldDefinition::create('string')
          ->setLabel(t('HPB Recurrence Interval'))
          ->setDisplayOptions('view', array(
              'label' => 'above',
              'type' => 'integer',
              'weight' => -4,
          ))
          ->setDisplayOptions('form', array(
              'label' => 'above',
              'type' => 'integer',
              'weight' => -4,
          ))
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
