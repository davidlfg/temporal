<?php

namespace Drupal\remote_content_sync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use GuzzleHttp\Psr7\Uri;

/**
 * Defines the Remote site entity.
 *
 * @ConfigEntityType(
 *   id = "remote_site",
 *   label = @Translation("Remote site"),
 *   handlers = {
 *     "list_builder" = "Drupal\remote_content_sync\RemoteSiteListBuilder",
 *     "form" = {
 *       "add" = "Drupal\remote_content_sync\Form\RemoteSiteForm",
 *       "edit" = "Drupal\remote_content_sync\Form\RemoteSiteForm",
 *       "delete" = "Drupal\remote_content_sync\Form\RemoteSiteDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\remote_content_sync\RemoteSiteHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "remote_site",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/services/remote_site/{remote_site}",
 *     "add-form" = "/admin/config/services/remote_site/add",
 *     "edit-form" = "/admin/config/services/remote_site/{remote_site}/edit",
 *     "delete-form" = "/admin/config/services/remote_site/{remote_site}/delete",
 *     "collection" = "/admin/config/services/remote_site"
 *   }
 * )
 */
class RemoteSite extends ConfigEntityBase implements RemoteSiteInterface {

  /**
   * The Remote site ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Remote site label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Remote content sync URI.
   *
   * @var string
   */
  protected $remote_uri;

  protected $field_map;

  protected $filters;

  protected $operator;

  protected $remoteFilters;

  protected $uri;

  protected $auto_sync;

  /**
   * Returns the uri.
   */
  public function uri() {
    if (!isset($this->uri) || !($this->uri instanceof Uri)) {
      $this->uri = new Uri($this->remote_uri);
    }
    return $this->uri;
  }

  /**
   * Returns the username.
   */
  public function username() {
    $user_info = explode(':', $this->uri()->getUserInfo());
    return $user_info[0];
  }

  /**
   * Returns the password.
   */
  public function password() {
    $user_info = explode(':', $this->uri()->getUserInfo());
    return !empty($user_info[1]) ? $user_info[1] : '';
  }

  /**
   * Getter for the field map property.
   */
  public function getFieldMap() {
    return $this->field_map;
  }

  /**
   * Returns the filters.
   */
  public function filters() {
    return $this->filters;
  }

  /**
   * Gets a remote filter by id.
   */
  public function getRemoteFilter($id) {
    $remote_filter = NULL;
    $filters = $this->getRemoteFilters();
    if (isset($filters[$id])) {
      $remote_filter = $filters[$id];
    }
    return $remote_filter;
  }

  /**
   * Gets all remote filters.
   */
  public function getRemoteFilters() {
    if (!$this->remoteFilters) {
      $manager = \Drupal::service('remote_content_sync.remote_filter_manager');
      foreach ($this->filters() as $filter) {
        if (!empty($filter['data'])) {
          $this->remoteFilters[$filter['id']] = $manager->createInstance($filter['id'], $filter);
        }
      }
    }
    return $this->remoteFilters;
  }

  /**
   * Filters an entity.
   */
  public function filter(ContentEntityInterface $entity) {
    $return = $this->operator === self::AND_OPERATOR;
    foreach ($this->getRemoteFilters() as $remoteFilter) {
      if ($this->operator === self::AND_OPERATOR) {
        $return = $return && $remoteFilter->filter($entity);
      }
      else {
        $return = $return || $remoteFilter->filter($entity);
      }
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function isAutoSyncEnabled() {
    return (bool) $this->auto_sync;
  }

}
