<?php

namespace Drupal\user_action_item;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the User action item entity entity.
 *
 * @see \Drupal\user_action_item\Entity\UserActionItemEntity.
 */
class UserActionItemEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\user_action_item\Entity\UserActionItemEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished user action item entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published user action item entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit user action item entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete user action item entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add user action item entity entities');
  }

}
