<?php

namespace Drupal\behaviour_science_data;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the BCT Bucket entity.
 *
 * @see \Drupal\behaviour_science_data\Entity\BCTBucket.
 */
class BCTBucketAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\behaviour_science_data\Entity\BCTBucketInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished bct bucket entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published bct bucket entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit bct bucket entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete bct bucket entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add bct bucket entities');
  }

}
