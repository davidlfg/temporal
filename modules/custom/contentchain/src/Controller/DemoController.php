<?php
/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Render\ViewsRenderPipelineMarkup;
use Drupal\Core\Entity\Entity;
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field_collection\Entity\FieldCollection;
use Drupal\KernelTests\KernelTestBase;

/**
 * DemoController.
 */
class DemoController extends ControllerBase {

  public function test_demo() {
  return array(
	'#markup' => t('Demo'),
    );
  }
}
