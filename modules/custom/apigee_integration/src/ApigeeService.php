<?php

namespace Drupal\apigee_integration;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;

/**
 * Class ApigeeService.
 *
 * @package Drupal\apigee_integration
 */
class ApigeeService {

  /**
   * Constructor.
   */
  public function __construct() {

  }





 
  /**
   * Function for  get_apigee_bearer_token
   * How to Use :  \Drupal::service('apigee.connect')->get_apigee_bearer_token();
   * @return array|string
   */
  public function get_apigee_bearer_token() {
    $config = \Drupal::configFactory()
      ->getEditable('apigee_integration.ApigeeConfig');
    $get_bearer_token = $config->get('get_bearer_token_url');
    $url = isset($get_bearer_token) ? $get_bearer_token : 'https://jnj-dev.apigee.net/headless/auth/app?response_type=token&client_id=9qQOg0LTdg3Zpm4ZAkLYFFzlVWuCZe23';
    $headers = [];
    $formData = [];
    $response = \Drupal::service('apigee.connect')
      ->prepareRequestResponse($url, $headers, $formData, 'get');
    if (isset($response['app-token'])) {
      $access_token = 'Bearer ' . $response['app-token'];
      return $access_token;
    }
    else {
      \Drupal::logger('get_apigee_bearer_token')
        ->error('Error creating apigee bearer token.');
      return array('error' => 'Error creating Apigee bearer token');
    }
  }

  /**
   * @param $uuid
   * @return array
   */
  public function get_health_store_user_profile($uuid) {
    if ($uuid == '') {
      return array('error' => 'UUID cannot be null.');
    }
    $config = \Drupal::configFactory()
      ->getEditable('apigee_integration.ApigeeConfig');
    $get_user_profile = $config->get('apigee_get_user_profile_url');

    $access_token = \Drupal::service('apigee.connect')
      ->get_apigee_bearer_token();

    $url = isset($get_user_profile) ? $get_user_profile : 'https://jnj-dev.apigee.net/headless/auth/user/get-profile';
    $headers = [
      'uuid' => $uuid,
      'Authorization' => $access_token
    ];
    $formData = [];
    $hsAccount = \Drupal::service('apigee.connect')
      ->prepareRequestResponse($url, $headers, $formData, 'get');
    if (isset($hsAccount['uuid'])) {
      return $hsAccount;
    }
    else {
      \Drupal::logger('get_health_store_user_profile')
        ->error('Error fetching user data from HealthStore.');
      return array('error' => 'Error fetching user data from HealthStore');
    }
  }

  /**
   * @param $uuid
   * @return array
   */
  public function get_headless_user_profile($uuid) {
    if ($uuid == '') {
      return array('error' => 'UUID cannot be null.');
    }
    $config = \Drupal::configFactory()
      ->getEditable('apigee_integration.ApigeeConfig');
    $headless_get_user_profile = $config->get('headless_get_user_profile');
    $url = isset($headless_get_user_profile) ? $headless_get_user_profile : 'http://dev.rwbheadless/user_management/get-profile';
    $headers = [
      'uuid' => $uuid,
    ];
    $formData = [];
    $profile = \Drupal::service('apigee.connect')
      ->prepareRequestResponse($url, $headers, $formData, 'get');
    return $profile;
  }

  /**
   * @param $formData
   * @param $uuid
   */
  public function update_health_store_user_profile($formData, $uuid) {
    $config = \Drupal::configFactory()
      ->getEditable('apigee_integration.ApigeeConfig');
    $update_user_profile = $config->get('apigee_update_user_profile_url');

    $access_token = \Drupal::service('apigee.connect')
      ->get_apigee_bearer_token();

    $formData['uuid'] = $uuid;
    $headers = [
      'uuid' => $uuid,
      'Authorization' => $access_token
    ];
    $url = isset($update_user_profile) ? $update_user_profile : 'https://jnj-dev.apigee.net/headless/auth/user/update-profile';
    $response = \Drupal::service('apigee.connect')
      ->prepareRequestResponse($url, $headers, $formData, 'post');
    //@TODO :check the response and add watchdog log
  }

  /**
   * @param $url
   * @param $headers
   * @param $formData
   * @param string $method
   * @return array
   */
  public function prepareRequestResponse($url, $headers, $formData, $method = 'post') {
    $config = \Drupal::configFactory()
      ->getEditable('apigee_integration.ApigeeConfig');
    $apigee_client_app = $config->get('apigee_client_app');

    $data = array(
      'body' => http_build_query($formData, '', '&', PHP_QUERY_RFC3986),
      'headers' => ['ClientApp' => $apigee_client_app],
      'timeout' => 600
    );
    //Unset header Authorization if that is not a Bearer token
    if (isset($headers['Authorization'])) {
      if (strpos($headers['Authorization'], 'Bearer ') != FALSE) {
        unset($headers['Authorization']);
      }
    }
    //Set Header
    foreach ($headers as $key => $value) {
      $data['headers'][$key] = $value;
    }
    unset($data['headers']['host']);
    unset($data['headers']['Host']);
    unset($data['headers']['Content-Length']);
    //end

    \Drupal::logger('prepareRequest.')->notice(print_r($data, TRUE));
    $response = \Drupal::service('apigee.connect')
      ->restCall($url, $data, $method);

    if ($response['StatusCode'] == 200 OR $response['StatusCode'] == 201) {
      return $response['ResponseBody'];
    }
    else {
      return array(
        $response['ResponseBody'],
      );
    }
  }

  /**
   * @param $url
   * @param $data
   * @param $method
   * @return array
   */
  public function restCall($url, $data, $method) {
    $client = \Drupal::httpClient();
    try {
      $request = $client->$method($url, $data);

      $headers = $request->getHeaders();
      $response = array(
        'StatusCode' => $request->getStatusCode(),
        'ResponseBody' => Json::decode($request->getBody()->getContents()),
        'ResponseTime' => $headers['Date'][0],
      );
      \Drupal::logger('RESTCall.' . $method)
        ->notice($url . ':' . print_r($response, TRUE));
    } catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode()) {
        $request = $e->getResponse();
        $headers = $request->getHeaders();
        $response = array(
          'StatusCode' => $request->getStatusCode(),
          'ResponseBody' => json_decode($request->getBody()),
          'ResponseTime' => $headers['Date'][0],
        );
        \Drupal::logger('RESTCall.' . $method)
          ->error($url . ':' . print_r($response, TRUE));
      }
    }
    return $response;
  }
}
