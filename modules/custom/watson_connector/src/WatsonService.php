<?php

namespace Drupal\watson_connector;

use Drupal\user\Entity\User;
use Drupal\Component\Datetime\DateTimePlus;

/**
 * Class WatsonService.
 *
 * @package Drupal\watson_connector
 */
class WatsonService {

  /**
   * Constructor.
   */
  public function __construct() {

  }


  /**
   * Function for creating on-boarding data
   * and sending to Apigee
   */
  function user_profile_questions() {
    $query = \Drupal::database()
      ->select('user__field_reg_data_send_to_apigee', 'u');
    $query->join('user__field_apigee_timestamp', 'uat', 'u.entity_id = uat.entity_id');
    $query->join('user__field_registration_date', 'urd', 'urd.field_registration_date_value = uat.field_apigee_timestamp_value');
    $query->join('user__field_registration_date', 'urd', 'u.entity_id = urd.entity_id');
    $query->join('user__field_watson_patient_id', 'wpi', 'u.entity_id = wpi.entity_id');
    $query->fields('u', array('entity_id'))
      ->condition('u.entity_id', 0, '<>')
      ->condition('u.entity_id', 1, '<>')
      ->condition('u.field_reg_data_send_to_apigee_value', 0, '=')
      ->condition('wpi.field_watson_patient_id_value', '', '<>')
      ->groupBy('u.entity_id')
      ->orderBy('u.entity_id', 'ASC');
    $result = $query->execute();
    $items = array();
    foreach ($result as $values) {
      $items[$values->entity_id] = isset($values->entity_id) ? $values->entity_id : '';
      $userObj = User::load($values->entity_id);
      //Get UserProfile
      $user = \Drupal::service('user_management.user')
        ->getUserProfile($userObj->uuid());
      if ($user['field_watson_patient_id'] == '') {
        continue;
      }

      $profileFields = array(
        'field_date_of_birth',
        'field_gender',
        'field_height',
        'field_race',
        'field_weight',
        'field_surgery_date'
      );
      $userProfileData = array("resourceType" => "QuestionnaireResponse");
      $i = 0;
      foreach ($profileFields as $fields) {
        $userProfileData['question'][$i]['CMS_Id'] = 2932;//TODO ;get the zombi id from configuration.
        $userProfileData['question'][$i]['questionItem'] = $fields;
        $userProfileData['question'][$i]['answer'] = isset($user[$fields]) ? $user[$fields] : '';
        $i++;
      }

      \Drupal::logger('$userProfileData.')
        ->notice($values->entity_id . '--' . $user['field_watson_patient_id'] . print_r($userProfileData, TRUE));
      //Send data to check-in questions to watson Apigee.
      $res = self::share_with_apigee($values->entity_id, $userProfileData, 'onboarding');
      if ($res['error']) {
        continue;
      }
    }
  }

  /**
   * Function to get Apigee Access Token
   * @return string
   */
  function get_apigee_access_token() {
    $headers = ['Content-Type' => 'application/x-www-form-urlencoded'];
    $config = \Drupal::configFactory()
      ->getEditable('watson_connector.WatsonConfig');
    $formData = [];
    $formData['client_id'] = $config->get('apigee_accesstoken_client_id');
    $formData['client_secret'] = $config->get('apigee_accesstoken_client_secret');
    $url = $config->get('apigee_accesstoken_uri');
    $response = \Drupal::service('watson.connect')
      ->prepareRequestResponse($url, $headers, $formData, 'post');
    if (isset($response['access_token'])) {
      return 'Bearer ' . $response['access_token'];
    }
    else {
      return 'error';
    }
  }


  /**
   * Function to create checkin date
   * for checkin bundle
   */
  function create_check_in_questions() {
    $query = \Drupal::database()
      ->select('user__field_reg_data_send_to_apigee', 'u');
    $query->join('dc_queue', 'dq', 'u.entity_id = dq.uid');
    $query->join('user__field_watson_patient_id', 'wpi', 'u.entity_id = wpi.entity_id');
    $query->fields('u', array('entity_id'))
      ->condition('u.entity_id', 0, '<>')
      ->condition('u.entity_id', 1, '<>')
      ->condition('u.field_reg_data_send_to_apigee_value', 1, '=')
      ->condition('wpi.field_watson_patient_id_value', '', '<>')
      ->groupBy('u.entity_id')
      ->isNotNull('dq.updated_date');
    $query->orderBy('u.entity_id', 'ASC');
    $result = $query->execute();;
    $j = 0;
    $userlist = array();
    $dcQueuelist = array();
    foreach ($result as $values) {
      $userlist[] = $values->entity_id;
      $user = User::load($values->entity_id);
      $apigeetimestamp = DateTimePlus::createFromFormat('Y-m-d\TH:i:s', $user->field_apigee_timestamp->value, $user->timezone->value);
      $apigee_timestamp = $apigeetimestamp->format('Y-m-d H:i:s');
      $apigee_timestamp = isset($apigee_timestamp) ? $apigee_timestamp : '';

      if ($apigee_timestamp) {
        $dc_queue = \Drupal::database()->select('dc_queue', 'dq');
        /*If reg data and apigeetimestap is same do not compare
              checkin updated date with apigee timestamp*/
        if ($user->field_apigee_timestamp->value == $user->field_registration_date->value) {
          $dc_queue->fields('dq', array(
            'check_in',
            'uid',
            'prompt_id',
            'completed',
            'complete_time',
            'ask_count',
            'creation_date',
            'updated_date'
          ))
            ->condition('dq.uid', $values->entity_id)
            ->isNotNull('dq.updated_date');
        }
        else {
          /*If reg data and apigeetimestap is different do compare
                   checkin updated date with apigee timestamp to get latest checkin*/
          $dc_queue->fields('dq', array(
            'check_in',
            'uid',
            'prompt_id',
            'completed',
            'complete_time',
            'ask_count',
            'creation_date',
            'updated_date'
          ))
            ->condition('dq.uid', $values->entity_id)
            ->condition('dq.updated_date', $apigee_timestamp, '>=')
            ->isNotNull('dq.updated_date');
        }
        $dcQueue = $dc_queue->execute();

        $i = 1;
        foreach ($dcQueue as $value) {
          $dcQueuelist[$value->uid][$i]['uid'] = $value->uid;
          $dcQueuelist[$value->uid][$i]['prompt_id'] = $value->prompt_id;
          $node = \Drupal\node\Entity\Node::load($value->prompt_id);
          $dcQueuelist[$value->uid][$i]['checkin_question'] = isset($node->field_text_content->value) ? $node->field_text_content->value : NULL;
          if (isset($node->field_prompt_action->target_id)) {
            $responses = \Drupal::database()->select('responses', 'rs')
              ->fields('rs', array('key_value_array', 'timestamp'))
              ->condition('rs.uid', $value->uid)
              ->condition('rs.link_id', $node->field_prompt_action->target_id)
              ->execute()
              ->fetchAssoc();
          }
          if ($responses['key_value_array']) {
            $key_value_array = unserialize($responses['key_value_array']);
            $checkinAnswer = array_values($key_value_array);
            $dcQueuelist[$value->uid][$i]['checkin_answer'] = $checkinAnswer[0];
            $dcQueuelist[$value->uid][$i]['checkin_time'] = isset($responses['timestamp']) ? date_iso8601(strtotime($responses['timestamp'])) : NULL;

          }
          else {
            if ($value->ask_count <= 2) {
              $dcQueuelist[$value->uid][$i]['checkin_answer'] = 'SKIP';
            }
            else {
              $dcQueuelist[$value->uid][$i]['checkin_answer'] = 'DECLINE';
            }
            $dcQueuelist[$value->uid][$i]['checkin_time'] = NULL;
          }
          $i++;
        }
      }
    }
    self::checkInBundle($dcQueuelist);
  }

  /*
  * Function for creating checkin bundle
  */
  function checkInBundle($dcQueuelist) {
    foreach ($dcQueuelist as $uid => $checkinArray) {
      $check_in_Data = array("resourceType" => "QuestionnaireResponse");
      $i = 0;
      foreach ($checkinArray as $key => $data) {
        //Data
        $check_in_Data['question'][$i]['CMS_Id'] = $data['prompt_id'];
        $check_in_Data['question'][$i]['questionItem'] = $data['checkin_question'];
        $check_in_Data['question'][$i]['answer'] = isset($data['checkin_answer']) ? $data['checkin_answer'] : '';
        //$check_in_Data['question'][$i]['checkin_time'] = isset($data['checkin_time']) ? $data['checkin_time'] : '';
        $i++;
      }
      // Call APIGEE post service
      $res = self::share_with_apigee($uid, $check_in_Data, 'checkin');
      if ($res['error']) {
        continue;
      }
    }
  }

  function share_with_apigee($uid, $data, $type) {
    $userObj = User::load($uid);
    if (count($userObj) > 0) {

      //Get UserProfile
      $user = \Drupal::service('user_management.user')
        ->getUserProfile($userObj->uuid());


      $publicSys = 'public://check-ins';
      $res = file_prepare_directory($publicSys, FILE_CREATE_DIRECTORY);
      if (!$res) {
        \Drupal::logger('Watson Apigee')->error('Error creating' .
          'check-in bundle folder for User ' . $uid);
      }

      $jsonData = json_encode($data, JSON_PRETTY_PRINT);
      $destination = $publicSys . '/bundle.json';
      $file = file_save_data($jsonData, $destination, FILE_EXISTS_REPLACE);
      if (!$file->id()) {
        return array('error' => 'error');
      }
      $config = \Drupal::configFactory()
        ->getEditable('watson_connector.WatsonConfig');
      $access_token = self::get_apigee_access_token();
      if ($access_token == 'error') {
        return array('error' => 'error');
      }
      $data = array(
        'multipart' => array(
          array(
            'name' => 'bundle',
            'contents' => fopen(drupal_realpath($destination), 'r')
          ),
          array(
            'name' => 'patientId',
            'contents' => $user['field_watson_patient_id']
          ),
        ),
        'headers' => array('Authorization' => $access_token)
      );
      \Drupal::logger('DATA.')
        ->notice(print_r($data, TRUE));

      $url = $config->get('apigee_file_upload_uri');
      $response = \Drupal::service('watson.connect')
        ->restCall($url, $data, 'post');
      if ($response['StatusCode'] == 200 OR $response['StatusCode'] == 201) {
        //@todo ; call update user profile
        $response = \Drupal::service('watson.services')
          ->update_apigee_timestamp($uid, $response['ResponseTime'], $type);
      }
    }
  }


  /**
   * Function to update user apigee_timestamp ,
   * if the APIGEE response is SUCCESS
   */
  function update_apigee_timestamp($uid, $datetime, $type) {
    $user = User::load($uid);
    if ($type == 'onboarding') {
      $formData['field_reg_data_send_to_apigee'] = 1;
      $hsAccountUpdated = \Drupal::service('apigee.connect')
        ->update_health_store_user_profile($formData, $user->uuid());

      \Drupal::logger('user_management')
        ->notice('User \'Onboarding data Send to Apigee field\' is updated for user : %email.', array('%email' => $user->mail->value,));
    }
    else {
      $formData['field_apigee_timestamp'] = date_format(date_create($datetime), 'Y-m-d\TH:i:s');
      $hsAccountUpdated = \Drupal::service('apigee.connect')
        ->update_health_store_user_profile($formData, $user->uuid());

      \Drupal::logger('user_management')
        ->notice('User  apigee_timestamp is updated to %time for user : %email.', array(
          '%time' => $datetime,
          '%email' => $user->mail->value,
        ));
    }
  }


}
