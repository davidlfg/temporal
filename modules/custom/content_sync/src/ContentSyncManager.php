<?php

namespace Drupal\content_sync;

use Drupal\content_sync\Plugin\SyncNormalizerDecoratorManager;
use Drupal\content_sync\Plugin\SyncNormalizerDecoratorTrait;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Path\AliasStorage;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ContentSyncManager.
 *
 * @package Drupal\content_sync
 */
class ContentSyncManager implements ContentSyncManagerInterface {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  protected $aliasStorage;

  protected $format = 'json';

  protected $updateEntities = TRUE;

  /**
   * ContentSyncManager constructor.
   *
   * @param EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   * @param Serializer $serializer
   *   The serializer service.
   * @param FileSystemInterface $file_system
   *   The file system interface service.
   * @param SyncNormalizerDecoratorManager $decorator_manager
   *   The decorator manager service.
   */
  public function __construct(EntityTypeManager $entity_type_manager, Serializer $serializer, FileSystemInterface $file_system, AliasStorage $alias_storage) {
    $this->entityTypeManager = $entity_type_manager;
    $this->serializer = $serializer;
    $this->fileSystem = $file_system;
    $this->aliasStorage = $alias_storage;
  }

  /**
   * Getter for the format property.
   *
   * @return string
   *   The format.
   */
  public function getFormat() {
    return $this->format;
  }

  /**
   * Setter for the format property.
   *
   * @param string $format
   *   The format.
   */
  public function setFormat($format) {
    $this->format = $format;
  }

  /**
   * Getter for the update entities property.
   *
   * @return bool
   *   The update entities property.
   */
  public function getUpdateEntities() {
    return $this->updateEntities;
  }

  /**
   * Setter for the update entities property.
   *
   * @param bool $updateEntities
   *   The update entities property.
   */
  public function setUpdateEntities($updateEntities) {
    $this->updateEntities = $updateEntities;
  }

  /**
   * {@inheritdoc}
   */
  public function importContent($folder) {
    $imported = array();
    if (file_exists($folder)) {
      $definitions = $this->entityTypeManager->getDefinitions();
      $definition_node = $definitions['node'];
      unset($definitions['node']);
      $definitions['node'] = $definition_node;
      foreach ($definitions as $entity_type_id => $entity_type) {
        $reflection = new \ReflectionClass($entity_type->getClass());
        // We are only interested in importing content entities.
        if (!$reflection->implementsInterface('\Drupal\Core\Entity\ContentEntityInterface')) {
          continue;
        }
        if (!file_exists($folder . '/' . $entity_type_id)) {
          continue;
        }
        $supported_extension = array(_content_sync_extension($this->format));
        $scanner = new ContentSyncScanner($supported_extension);
        $files = $scanner->scan($folder . '/' . $entity_type_id);
        $context = [
          'content_sync_directory' => $folder,
          'resolve_uuid' => TRUE,
          'content_sync' => TRUE,
        ];
        foreach ($files as $path => $file) {
          $content = file_get_contents($path);
          $decoded_entity = $this->serializer->decode($content, $this->format, $context);
          $entity = $this->serializer->denormalize($decoded_entity, $entity_type->getClass(), $this->format, $context);
          if (!empty($entity)) {
            $imported_entity = $this->syncEntity($entity);
            if (!empty($entity->_alias) && strpos($entity->_alias, '/') === 0 && !empty($imported_entity)) {
              $this->saveAlias($imported_entity, $entity->_alias);
            }
            if (!empty($imported_entity)) {
              $imported[] = $imported_entity;
            }
          }
        }
      }
    }
    return $imported;
  }

  /**
   * {@inheritdoc}
   */
  public function exportContent($entity_type, $entity_ids, $context = []) {
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $entities = $storage->loadMultiple($entity_ids);
    $return = [];
    if (!empty($entities)) {
      $return = $this->exportEntities($entities, $context);
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function exportContentByBundle($entity_type, $bundle, $context = []) {
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $bundle_key = $this->entityTypeManager->getDefinition($entity_type)
      ->getKey('bundle');
    if ($bundle_key) {
      $entities = $storage->loadByProperties([$bundle_key => $bundle]);
      if (!empty($entities)) {
        return $this->exportEntities($entities, $context);
      }
    }
    return $this->exportContent($entity_type, NULL, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function exportEntities($entities, $context = []) {
    $return = [];
    $context += [
      'content_sync' => TRUE,
    ];
    foreach ($entities as $entity) {
      $uuid = $entity->uuid();
      $referenced_entities = $this->getEntityReferences($entity);
      /* @var $referenced_entity ContentEntityInterface */
      foreach ($referenced_entities as $referenced_entity) {
        $id = $referenced_entity->uuid();
        if (!isset($return[$id])) {
          $return[$id] = $this->exportEntity($referenced_entity, $context);
        }
      }
      $return[$uuid] = $this->exportEntity($entity, $context);
    }

    return $return;
  }

  /**
   * Exports the given entity.
   */
  public function exportEntity(ContentEntityInterface $entity, $context = []) {
    $context += [
      'content_sync' => TRUE,
    ];
    $normalized_entity = $this->serializer->normalize($entity, $this->format, $context);
    $return = [
      'entity_type_id' => $entity->getEntityTypeId(),
      'entity' => $this->serializer->encode($normalized_entity, $this->format, $context),
      'original_entity' => $entity,
    ];

    return $return;
  }

  /**
   * Gets the entity references of a given entity.
   *
   * @param ContentEntityInterface $entity
   *   The entity.
   * @param array $entities
   *   The additional entities.
   *
   * @return array
   *   An array of the entities' references.
   */
  public function getEntityReferences(ContentEntityInterface $entity, &$entities = array()) {

    $referenced_entities = $entity->referencedEntities();
    $extra_entities = $this->getEntityReferencesFromText($entity);
    if ($extra_entities) {
      $referenced_entities += $extra_entities;
    }
    $entity_dependencies = [];
    foreach ($referenced_entities as $referenced_entity) {
      $entity_dependencies[$referenced_entity->uuid()] = $referenced_entity;
    }
    $entities[$entity->uuid()] = $entity->uuid();
    foreach ($entity_dependencies as $id => $dependent_entity) {
      // Config entities should not be exported but rather provided by default
      // config.
      if ($dependent_entity instanceof ConfigEntityInterface) {
        unset($entity_dependencies[$id]);
      }
      elseif (empty($entities[$dependent_entity->uuid()])) {
        $entities[$entity->uuid()] = $dependent_entity->uuid();
        $entity_dependencies = array_merge($entity_dependencies, $this->getEntityReferences($dependent_entity, $entities));
      }
    }
    return $entity_dependencies;
  }

  /**
   * Get the entity references for a given rich text entity.
   *
   * @param ContentEntityInterface $entity
   *   The entity.
   *
   * @return array
   *   The referenced entities array.
   */
  protected function getEntityReferencesFromText(ContentEntityInterface $entity) {
    $entities = [];
    $field_definitions = $entity->getFieldDefinitions();
    foreach ($field_definitions as $field_name => $field_definition) {
      // We are only interested in Text fields.
      if (!is_a($field_definition->getClass(), '\Drupal\Core\Field\FieldItemList', TRUE) ||
          !is_a($field_definition->getItemDefinition()
            ->getClass(), '\Drupal\text\Plugin\Field\FieldType\TextItemBase', TRUE)
      ) {
        continue;
      }
      if ($value = $entity->{$field_name}) {
        $value = $entity->{$field_name}->getString();
        if (!empty($value)) {
          $doc = new \DOMDocument();
          @$doc->loadHTML($value);
          $tags = $doc->getElementsByTagName('img');
          foreach ($tags as $tag) {
            $entity_type = $tag->getAttribute('data-entity-type');
            $uuid = $tag->getAttribute('data-entity-uuid');
            if ($uuid && $entity && !isset($entities[$uuid])) {
              $entity = $this->entityTypeManager->getStorage($entity_type)
                ->loadByProperties(array('uuid' => $uuid));
              if (!empty($entity)) {
                $entities[$uuid] = reset($entity);
              }
            }
          }
        }
      }
    }

    return $entities;
  }

  /**
   * Synchronize a given entity.
   *
   * @param ContentEntityInterface $entity
   *   The entity to update.
   *
   * @return ContentEntityInterface
   *   The updated entity
   */
  protected function syncEntity(ContentEntityInterface $entity) {
    $preparedEntity = $this->prepareEntity($entity);

    if ($this->validateEntity($preparedEntity)) {
      $preparedEntity->save();
      return $preparedEntity;
    }
    elseif (!$preparedEntity->isNew()) {
      return $preparedEntity;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntity(ContentEntityInterface $entity) {
    $uuid = $entity->uuid();
    $original_entity = $this->entityTypeManager->getStorage($entity->getEntityTypeId())
      ->loadByProperties(array('uuid' => $uuid));
    if (!empty($original_entity)) {
      $original_entity = reset($original_entity);
      if (!$this->updateEntities) {
        return $original_entity;
      }
      // Overwrite the received properties.
      if (!empty($entity->_restSubmittedFields)) {
        foreach ($entity->_restSubmittedFields as $field_name) {
          if ($this->isValidEntityField($original_entity, $entity, $field_name)) {
            $original_entity->set($field_name, $entity->get($field_name)
              ->getValue());
          }
        }
        if ($this->validateEntity($original_entity)) {
          $original_entity->save();
        }
      }
      return $original_entity;
    }
    $duplicate = $entity->createDuplicate();
    $entity_type = $entity->getEntityType();
    $duplicate->{$entity_type->getKey('uuid')}->value = $uuid;
    return $duplicate;
  }

  /**
   * {@inheritdoc}
   */
  public function validateEntity(ContentEntityInterface $entity) {
    $reflection = new \ReflectionClass($entity);
    $valid = TRUE;
    if ($reflection->implementsInterface('\Drupal\user\UserInterface')) {
      $validations = $entity->validate();
      if (count($validations)) {
        $valid = FALSE;
        foreach ($validations as $validation) {
          \Drupal::logger('content_sync')
            ->error($validation->getMessage());
        }
      }
    }
    return $valid;
  }

  /**
   * Generates the entity checksum.
   */
  public function generateChecksum(ContentEntityInterface $entity) {
    $reflection = new \ReflectionClass($entity);
    if ($entity->hasField('changed')) {
      $md5 = md5($entity->get('changed')->getString());
    }
    elseif ($reflection->implementsInterface('\Drupal\file\FileInterface')) {
      if ($entity->getFileUri()) {
        $file_data = base64_encode(file_get_contents($this->fileSystem->realpath($entity->getFileUri())));
        $md5 = md5($file_data);
      }
    }
    // @todo find a way to remove local references from this entity for hashing.
    if (empty($md5)) {
      $md5 = md5($this->serializer->encode($entity->toArray(), 'json'));
    }

    return $md5;
  }

  /**
   * Checks if the entity field needs to be synchronized.
   *
   * @param ContentEntityInterface $original_entity
   *   The original entity.
   * @param ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   True if the field needs to be synced.
   */
  protected function isValidEntityField(ContentEntityInterface $original_entity, ContentEntityInterface $entity, $field_name) {
    $valid = TRUE;
    $entity_keys = $entity->getEntityType()->getKeys();
    // Check if the target entity has the field.
    if (!$entity->hasField($field_name)) {
      $valid = FALSE;
    }
    // Entity key fields need special treatment: together they uniquely
    // identify the entity. Therefore it does not make sense to modify any of
    // them. However, rather than throwing an error, we just ignore them as
    // long as their specified values match their current values.
    elseif (in_array($field_name, $entity_keys, TRUE)) {
      // Unchanged values for entity keys don't need access checking.
      if ($original_entity->get($field_name)->getValue() === $entity->get($field_name)->getValue()

          // It is not possible to set the language to NULL as it is
          // automatically re-initialized.
          // As it must not be empty, skip it if it is.
          || isset($entity_keys['langcode']) && $field_name === $entity_keys['langcode'] && $entity->get($field_name)->isEmpty()

          || $field_name === $entity->getEntityType()->getKey('id')

          || $entity->getEntityType()->isRevisionable() && $field_name === $entity->getEntityType()->getKey('revision')

      ) {
        $valid = FALSE;
      }
    }
    return $valid;
  }

  /**
   * Save the alias for the given entity.
   *
   * @param ContentEntityInterface $entity
   *   The entity.
   * @param string $path_alias
   *   The alias.
   *
   * @return array|false
   *   FALSE if the path could not be saved or an associative array containing
   *   the following keys:
   *   - source (string): The internal system path with a starting slash.
   *   - alias (string): The URL alias with a starting slash.
   *   - pid (int): Unique path alias identifier.
   *   - langcode (string): The language code of the alias.
   *   - original: For updates, an array with source, alias and langcode with
   *     the previous values.
   */
  protected function saveAlias(ContentEntityInterface $entity, $path_alias) {
    try {
      $langcode = $entity->language()->getId();
      $system_path = '/' . $entity->toUrl()->getInternalPath();
      $current_alias = $this->aliasStorage->load([
        'source' => $system_path,
        'langcode' => $langcode,
      ]);
      if (empty($current_alias) || $current_alias['alias'] != $path_alias) {
        $pid = empty($current_alias['pid']) ? NULL : $current_alias['pid'];
        return \Drupal::service('path.alias_storage')
          ->save($system_path, $path_alias, $langcode, $pid);
      }
    }
    catch (UndefinedLinkTemplateException $e) {
      return FALSE;
    }
  }

}
