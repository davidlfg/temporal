<?php

namespace Drupal\user_action_item\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting User action item entity entities.
 *
 * @ingroup user_action_item
 */
class UserActionItemEntityDeleteForm extends ContentEntityDeleteForm {


}
