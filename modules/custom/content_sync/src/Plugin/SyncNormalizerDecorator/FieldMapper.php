<?php

namespace Drupal\content_sync\Plugin\SyncNormalizerDecorator;

use Drupal\Component\Serialization\Yaml;
use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides a decorator to map field names.
 *
 * @SyncNormalizerDecorator(
 *   id = "field_mapper",
 *   name = @Translation("Field Mapper"),
 * )
 */
class FieldMapper extends SyncNormalizerDecoratorBase {

  /**
   * {@inheritdoc}
   */
  public function decorateNormalization(array &$normalized_entity, ContentEntityInterface $entity, $format, array $context = []) {
    $field_map = $this->getFieldMap($context);
    if (!empty($field_map)) {
      foreach ($field_map as $mapping) {
        if (!empty($normalized_entity[$mapping['local_name']])) {
          $normalized_entity[$mapping['remote_name']] = $normalized_entity[$mapping['local_name']];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function decorateDenormalization(array &$normalized_entity, $type, $format, array $context = []) {
    $field_map = $this->getFieldMap($context);
    if (!empty($field_map)) {
      foreach ($field_map as $mapping) {
        if (!empty($normalized_entity[$mapping['remote_name']])) {
          $normalized_entity[$mapping['local_name']] = $normalized_entity[$mapping['remote_name']];
        }
      }
    }
  }

  /**
   * Extract the field map from the serializer context.
   *
   * @param array $context
   *   The serializer context.
   *
   * @return array
   *   The field map.
   */
  protected function getFieldMap($context) {
    $field_map = [];
    if (!empty($context['field_map'])) {
      $field_map = $context['field_map'];
    }
    elseif (!empty($context['content_sync_directory'])) {
      $file = realpath($context['content_sync_directory'] . '/field_map.yml');
      if (file_exists($file)) {
        $field_map = Yaml::decode(file_get_contents($file));
        if (isset($field_map['field_map'])) {
          $field_map = $field_map['field_map'];
        }
      }
    }
    return $field_map;
  }

}
