<?php

namespace Drupal\oauth_service_connector;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Drupal\Component\Serialization\Json;

/**
 * Class RESTCall.
 *
 * @package Drupal\oauth_service_connector
 */
class RESTCall {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * @param $url
   * @param $data
   * @param $method
   * @return array
   */
  public function restCall($url, $data, $method) {
    $client = \Drupal::httpClient();

    try {
      $request = $client->$method($url, $data);

      $headers = $request->getHeaders();
      $response = array(
        'StatusCode' => $request->getStatusCode(),
        'ResponseBody' => Json::decode($request->getBody()->getContents()),
        'ResponseTime' => $headers['Date'][0],
      );
      \Drupal::logger('RESTCall.' . $method)
        ->notice($url . ':' . print_r($response, TRUE));
    } catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode()) {
        $request = $e->getResponse();
        $headers = $request->getHeaders();
        $response = array(
          'StatusCode' => $request->getStatusCode(),
          'ResponseBody' => json_decode($request->getBody()),
          'ResponseTime' => $headers['Date'][0],
        );
        \Drupal::logger('RESTCall.' . $method)
          ->error($url . ':' . print_r($response, TRUE));
      }
    }
    return $response;
  }
}
