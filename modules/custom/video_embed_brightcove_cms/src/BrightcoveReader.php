<?php

namespace Drupal\video_embed_brightcove_cms;
use Brightcove\API\Client;
use Brightcove\API\CMS;
use Brightcove\Object\Video\Image;
use Drupal\Core\Config\ConfigFactoryInterface;


/**
 * Class BrightcoveReader.
 *
 * @package Drupal\video_embed_brightcove_cms
 */
class BrightcoveReader implements BrightcoveReaderInterface {

  protected $cms;

  protected $client;

  /**
   * Constructor.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $config = $configFactory->get('video_embed_brightcove_cms.settings');
    $client_id = $config->get('client_id');
    $client_secret = $config->get('secret_key');
    $account_id = $config->get('account_id');
    $this->client = Client::authorize($client_id, $client_secret);
    $this->cms = new CMS($this->client, $account_id);
  }

  /**
   * Gets the images for a single video.
   *
   * @return \Brightcove\Object\Video\Images
   */
  protected function getVideoImages($video_id) {
    return $this->cms->getVideoImages($video_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoImage($video_id) {
    $images = $this->getVideoImages($video_id);
    if (!empty($images)) {
      $image = $images->getPoster();
      if (is_array($image)) {
        return $image['src'];
      }
      elseif ($image instanceof Image) {
        return $image->getSrc();
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getVideoThumbnail($video_id) {
    $images = $this->getVideoImages($video_id);
    if (!empty($images)) {
      $image = $images->getThumbnail();
      if (is_array($image)) {
        return $image['src'];
      }
      elseif ($image instanceof Image) {
        return $image->getSrc();
      }
    }
    return FALSE;
  }

}
