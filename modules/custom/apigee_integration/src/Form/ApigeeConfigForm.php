<?php

namespace Drupal\apigee_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ApigeeConfigForm.
 *
 * @package Drupal\apigee_integration\Form
 */
class ApigeeConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'apigee_integration.ApigeeConfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apigee_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('apigee_integration.ApigeeConfig');
    $headless_get_user_profile = $config->get('headless_get_user_profile');
    $form['headless_get_user_profile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Headless Get User Profile URL'),
      '#description' => $this->t('Example :http://[[dev|stage]].headless.careadvantage.jnj.com/user_management/get-profile'),
      '#maxlength' => 150,
      '#size' => 100,
      '#default_value' => isset($headless_get_user_profile) ? $headless_get_user_profile : 'http://dev.rwbheadless/user_management/get-profile',
    ];
    $apigee_update_user_profile_url = $config->get('apigee_update_user_profile_url');
    $form['apigee_update_user_profile_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apigee Update User Profile URL'),
      '#description' => $this->t('Example :https://jnj-[[dev|test|prod]].apigee.net/headless/auth/user/update-profile'),
      '#maxlength' => 150,
      '#size' => 100,
      '#default_value' => isset($apigee_update_user_profile_url) ? $apigee_update_user_profile_url : 'https://jnj-dev.apigee.net/headless/auth/user/update-profile',
    ];
    $get_bearer_token_url = $config->get('get_bearer_token_url');
    $form['get_bearer_token_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Get Bearer Token URL'),
      '#description' => $this->t('Example :https://jnj-[[dev|test|prod]].apigee.net/headless/auth/app?response_type=token'),
      '#maxlength' => 150,
      '#size' => 100,
      '#default_value' => isset($get_bearer_token_url) ? $get_bearer_token_url : 'https://jnj-dev.apigee.net/headless/auth/app?response_type=token',
    ];
    $apigee_get_user_profile_url = $config->get('apigee_get_user_profile_url');
    $form['apigee_get_user_profile_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apigee Get User Profile URL'),
      '#description' => $this->t('Example :https://jnj-[[dev|test|prod]].apigee.net/headless/auth/user/get-profile'),
      '#maxlength' => 150,
      '#size' => 100,
      '#default_value' => isset($apigee_get_user_profile_url) ? $apigee_get_user_profile_url : 'https://jnj-dev.apigee.net/headless/auth/user/get-profile',
    ];
    $apigee_client_app = $config->get('apigee_client_app');
    $form['apigee_client_app'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apigee Client App'),
      '#description' => $this->t('Example :Knees App'),
      '#maxlength' => 150,
      '#size' => 100,
      '#default_value' => isset($apigee_client_app) ? $apigee_client_app : 'Knees App',
    ];
    $questionnaire_response = $config->get('apigee_get_questionnaire_response');
    $form['apigee_get_questionnaire_response'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apigee Get Questionnaire Response'),
      '#description' => $this->t('Example :http://jnj-[[dev|test|prod]].apigee.net/v1/health-partners/user/questionnaireResponse'),
      '#maxlength' => 150,
      '#size' => 100,
      '#default_value' => isset($questionnaire_response) ? $questionnaire_response : 'http://jnj-dev.apigee.net/v1/health-partners/user/questionnaireResponse',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('apigee_integration.ApigeeConfig')
      ->set('headless_get_user_profile', $form_state->getValue('headless_get_user_profile'))
      ->set('apigee_update_user_profile_url', $form_state->getValue('apigee_update_user_profile_url'))
      ->set('get_bearer_token_url', $form_state->getValue('get_bearer_token_url'))
      ->set('apigee_get_user_profile_url', $form_state->getValue('apigee_get_user_profile_url'))
      ->set('apigee_client_app', $form_state->getValue('apigee_client_app'))
      ->set('apigee_get_questionnaire_response', $form_state->getValue('apigee_get_questionnaire_response'))
      ->save();
  }

}
