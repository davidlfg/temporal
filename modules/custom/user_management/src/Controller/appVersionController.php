<?php
/**
 * appVersionController file to different version /environment of Applicaiton
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\user_management\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Datetime\DateTimePlus;

class appVersionController {
  /*
   * Function to get all build version
   * @return json array
   */
  public function appVersion() {
    $config = \Drupal::configFactory()->getEditable('user_management.settings');
    $appVersion = array();
    //Daily Build Version
    $dailyDownloadLink = $config->get('dailyDownloadLink');
    if ($dailyDownloadLink['value']) {
      $dailyEnvironmentName = $config->get('dailyEnvironmentName');
      $appVersion[$dailyEnvironmentName]['latestVersion'] = $config->get('dailyVersion');
      $appVersion[$dailyEnvironmentName]['downloadLink'] = str_replace("<br />", "", str_replace("\t", "    ", str_replace("\r\n", "", htmlspecialchars_decode($dailyDownloadLink['value']))));
    }
    //Dev Build Version
    $devDownloadLink = $config->get('devDownloadLink');
    if ($devDownloadLink['value']) {
      $devEnvironmentName = $config->get('devEnvironmentName');
      $appVersion[$devEnvironmentName]['latestVersion'] = $config->get('devVersion');
      $appVersion[$devEnvironmentName]['downloadLink'] = str_replace("<br />", "", str_replace("\t", "    ", str_replace("\r\n", "", htmlspecialchars_decode($devDownloadLink['value']))));
    }
    //stage Build Version
    $stageDownloadLink = $config->get('stageDownloadLink');
    if ($stageDownloadLink['value']) {
      $dailyEnvironmentName = $config->get('stageEnvironmentName');
      $appVersion[$dailyEnvironmentName]['latestVersion'] = $config->get('stageVersion');
      $appVersion[$dailyEnvironmentName]['downloadLink'] = str_replace("<br />", "", str_replace("\t", "    ", str_replace("\r\n", "", htmlspecialchars_decode($stageDownloadLink['value']))));
    }
    //Prod Build Version
    $prodDownloadLink = $config->get('prodDownloadLink');
    if ($prodDownloadLink['value']) {
      $devEnvironmentName = $config->get('prodEnvironmentName');
      $appVersion[$devEnvironmentName]['latestVersion'] = $config->get('prodVersion');
      $appVersion[$devEnvironmentName]['downloadLink'] = str_replace("<br />", "", str_replace("\t", "    ", str_replace("\r\n", "", htmlspecialchars_decode($prodDownloadLink['value']))));
    }
    //Adding batchNotificationTime
    $config = \Drupal::configFactory()
      ->getEditable('user_management.settings');
    $batch_notification_time = $config->get('batch_notification_time');
    if (isset($batch_notification_time)) {
      $date = DateTimePlus::createFromFormat('Y-m-d H:i:s', $batch_notification_time);
      $batch_notification_time = $date->format('H:i:s');
      $appVersion['batchNotificationTime'] = isset($batch_notification_time) ? date('m-d-Y') . ' ' . $batch_notification_time : date('m-d-Y') . ' 13:00:00';
    }
    return new JsonResponse($appVersion, 200);
  }

}