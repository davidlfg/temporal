<?php

namespace Drupal\headless_toolbox\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\node\Entity\Node;

/**
 * Manager for the endpoint that retrieves static content.
 */
class HeadlessContentController {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Retrieves the list of static content.
   *
   * @return array
   *   with the list of existing page nodes
   */
  public function getStaticPages() {

    $query = \Drupal::entityQuery('node')
      ->condition('type', 'page', '=');

    $nids = $query->execute();

    if (!empty($nids)) {
      $assets = Node::loadMultiple($nids);

      $i = 0;
      $items = array();

      foreach ($assets as $asset) {
        $items[$i]['title'] = $asset->get('title')->getValue()[0]['value'];
        $items[$i]['body'] = $asset->get('body')->getValue()[0]['value'];
        $i++;
      }
      return new JsonResponse($items, 200);

    }
    else {
      $response = array();
      return new JsonResponse($response, 200);
    }

  }

  /**
   * Function to upload images from base64 encoded strings.
   *
   * @return string
   *   Returns the public url for the image.
   */
  public function uploadImageBase64(Request $request) {

    $headers = getallheaders();
    $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);
    $content = json_decode($request->getContent(), TRUE);

    if (!empty($content['base64Image'])&&!empty($headers['UUID'])) {
      if (!empty($uid)) {
        $path = "public://user/uploaded";
        if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {

          $filename = md5(time() . uniqid()) . ".jpg";

          $data = explode(',', $content['base64Image']);

          $value = empty($data[1]) ? base64_decode($data[0]) : base64_decode($data[1]);

          if ($value !== FALSE) {

            $file = file_save_data($value, "public://user/uploaded/$filename", FILE_EXISTS_RENAME);
            $file_usage = \Drupal::service('file.usage');
            $file_usage->add($file, 'headless_toolbox', 'user', $uid);
            $file->save();
            $path = file_create_url($file->getFileUri());

            $response = array(
              'imageUrl' => $path,
            );

            return new JsonResponse($response, 200);
          }
          else {
            $response = array(
              'errorMessage' => t('Error: Not a valid base64 string.'),
            );
            return new JsonResponse($response, 400);
          }
        }
        else {
          $response = array(
            'errorMessage' => t('Error: File system error.'),
          );
          return new JsonResponse($response, 400);
        }
      }
      else {
        $response = array(
          'errorMessage' => t('Error: The user does not exist.'),
        );
        return new JsonResponse($response, 400);
      }

    }
    else {
      $response = array(
        'errorMessage' => t('Error: there are missing parameters'),
      );
      return new JsonResponse($response, 400);
    }
  }

}
