<?php

namespace Drupal\behaviour_science_data\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining BCT Temporary Array entities.
 *
 * @ingroup behaviour_science_data
 */
interface BCTTemporaryArrayInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the BCT Temporary Array name.
   *
   * @return string
   *   Name of the BCT Temporary Array.
   */
  public function getName();

  /**
   * Sets the BCT Temporary Array name.
   *
   * @param string $name
   *   The BCT Temporary Array name.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTTemporaryArrayInterface
   *   The called BCT Temporary Array entity.
   */
  public function setName($name);

  /**
   * Gets the BCT Temporary Array creation timestamp.
   *
   * @return int
   *   Creation timestamp of the BCT Temporary Array.
   */
  public function getCreatedTime();

  /**
   * Sets the BCT Temporary Array creation timestamp.
   *
   * @param int $timestamp
   *   The BCT Temporary Array creation timestamp.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTTemporaryArrayInterface
   *   The called BCT Temporary Array entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the BCT Temporary Array published status indicator.
   *
   * Unpublished BCT Temporary Array are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the BCT Temporary Array is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a BCT Temporary Array.
   *
   * @param bool $published
   *   TRUE to set this BCT Temporary Array to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\behaviour_science_data\Entity\BCTTemporaryArrayInterface
   *   The called BCT Temporary Array entity.
   */
  public function setPublished($published);

}
