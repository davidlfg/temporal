<?php

/**
 * @file
 * Contains bct_temporary_array.page.inc.
 *
 * Page callback for BCT Temporary Array entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for BCT Temporary Array templates.
 *
 * Default template: bct_temporary_array.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_bct_temporary_array(array &$variables) {
  // Fetch BCTTemporaryArray Entity Object.
  $bct_temporary_array = $variables['elements']['#bct_temporary_array'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
