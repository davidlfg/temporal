<?php

namespace Drupal\behaviour_science_data;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of BCT Reference entities.
 *
 * @ingroup behaviour_science_data
 */
class BCTReferenceListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('BCT Reference ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\behaviour_science_data\Entity\BCTReference */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.bct_reference.edit_form', array(
          'bct_reference' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
