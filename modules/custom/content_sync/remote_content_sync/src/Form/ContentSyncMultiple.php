<?php

namespace Drupal\remote_content_sync\Form;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Url;
use Drupal\remote_content_sync\Entity\RemoteSiteInterface;
use Drupal\remote_content_sync\EntityOperations;
use Drupal\remote_content_sync\Exception\RemoteContentSyncException;
use Drupal\remote_content_sync\RemoteEntityOperations;
use Drupal\remote_content_sync\RemoteSiteResolver;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ContentSyncMultiple.
 *
 * @package Drupal\remote_content_sync\Form
 */
class ContentSyncMultiple extends ConfirmFormBase {

  /**
   * Entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Private Temp Store Factory service.
   *
   * @var PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Remote Entity Operations service.
   *
   * @var RemoteEntityOperations
   */
  protected $remoteEntityOperations;

  /**
   * Queue factory service.
   *
   * @var QueueFactory
   */
  protected $queueFactory;

  /**
   * Remote site resolver service.
   *
   * @var RemoteSiteResolver
   */
  protected $remoteSiteResolver;

  protected $entityInfo = [];

  /**
   * Constructs a ContentSyncMultiple form object.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $manager
   *   The entity type manager.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $manager, RemoteEntityOperations $remote_entity_operations, QueueFactory $queue_factory, RemoteSiteResolver $remoteSiteResolver) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypeManager = $manager;
    $this->remoteEntityOperations = $remote_entity_operations;
    $this->queueFactory = $queue_factory;
    $this->remoteSiteResolver = $remoteSiteResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('entity_type.manager'),
      $container->get('remote_content_sync.remote_entity_operations'),
      $container->get('queue'),
      $container->get('remote_content_sync.remote_site_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_sync_multiple_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->formatPlural(count($this->entityInfo), 'Are you sure you want to synchronize this item?', 'Are you sure you want to synchronize these items?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Synchronize');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->entityInfo = $this->tempStoreFactory->get('content_sync_multiple_confirm')
      ->get(\Drupal::currentUser()
        ->id());

    if (empty($this->entityInfo)) {
      return new RedirectResponse($this->getCancelUrl()
        ->setAbsolute()
        ->toString());
    }

    // Remotes.
    $storage = $this->entityTypeManager->getStorage('remote_site');
    $remotes = $storage->loadMultiple();
    $options = [];
    foreach ($remotes as $remote) {
      $options[$remote->id()] = $remote->label();
    }
    $form['remotes'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Remote Sites'),
      '#description' => $this->t('Select the remote(s) to synchronize content with. Leave empty to synchronize with all available sites'),
    );

    // List of items to synchronize.
    $items = [];
    foreach ($this->entityInfo as $uuid => $entity_info) {
      $storage = $this->entityTypeManager->getStorage($entity_info['entity_type']);
      $entity = $storage->load($entity_info['entity_id']);
      if (!empty($entity)) {
        $items[$uuid] = $entity->label();
      }
    }
    $form['content_list'] = array(
      '#theme' => 'item_list',
      '#title' => 'Content List.',
      '#items' => $items,
    );
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('confirm') && !empty($this->entityInfo)) {
      $batch = [
        'title' => $this->t('Synchronizing Content...'),
        'message' => $this->t('Synchronizing Content...'),
        'operations' => [],
        'finished' => [$this, 'batchFinished'],
      ];
      $remotes = $form_state->getValue('remotes');
      $selected_remotes = array_diff($remotes, [0]);
      $remotes = !empty($selected_remotes) ? array_keys($selected_remotes) : array_keys($remotes);

      foreach ($this->entityInfo as $entity_info) {
        $entity = $this->entityTypeManager
          ->getStorage($entity_info['entity_type'])
          ->load($entity_info['entity_id']);
        $remote_sites = $this->remoteSiteResolver->getRemoteSites($entity);
        if (!empty($remote_sites)) {
          foreach ($remotes as $remote) {
            if ($remote_sites[$remote]) {
              $batch['operations'][] = [
                [$this, 'processItem'],
                [$entity, $remote_sites[$remote]],
              ];
            }
          }
        }
      }
      batch_set($batch);
    }
    else {
      $form_state->setRedirect('system.admin_content');
    }
  }

  /**
   * Batch operations callback.
   */
  public function processItem(ContentEntityInterface $entity, RemoteSiteInterface $remote_site, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['entity_references'] = $this->remoteEntityOperations->getEntityReferences($entity);
      $context['sandbox']['max'] = count($context['sandbox']['entity_references']) + 1;
    }

    $error = FALSE;
    try {
      if (!empty($context['sandbox']['entity_references'])) {
        $referenced_entity = array_pop($context['sandbox']['entity_references']);
        $sync = $referenced_entity;
        $this->remoteEntityOperations->updateRemoteEntity($referenced_entity, $remote_site);
      }
      else {
        $context['results'][] = TRUE;
        $sync = $entity;
        $this->remoteEntityOperations->updateRemoteEntity($entity, $remote_site);
      }
    }
    catch (RemoteContentSyncException $e) {
      if ($sync->uuid() === $entity->uuid()) {
        $queue = $this->queueFactory->get('remote_content_sync');
        $queue->createItem(
          (object) [
            'remote_site_id' => $remote_site->id(),
            'entity_id' => $entity->id(),
            'entity_uuid' => $entity->uuid(),
            'entity_type_id' => $entity->getEntityTypeId(),
            'operation' => EntityOperations::OPERATION_UPDATE,
          ]
        );
        $error = TRUE;
      }
      watchdog_exception('remote_content_sync', $e);
    }
    catch (\Exception $e) {
      watchdog_exception('remote_content_sync', $e);
    } finally {
      $context['sandbox']['progress']++;
      $context['message'] = $this->t('Synchronized content @label (@entity_type: @id) with remote @remote.', [
        '@label' => $sync->label(),
        '@id' => $sync->id(),
        '@entity_type' => $sync->getEntityTypeId(),
        '@remote' => $remote_site->label(),
      ]);
      if ($error) {
        $context['message'] = $this->t('Synchronization with remote @remote failed on content @label (@entity_type: @id).', [
          '@label' => $sync->label(),
          '@id' => $sync->id(),
          '@entity_type' => $sync->getEntityTypeId(),
          '@remote' => $remote_site->label(),
        ]);
      }
      if ($error) {
        drupal_set_message($context['message'], 'error');
      }
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Callback for batch finished.
   */
  public function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One content Synchronized.', '@count contents Synchronized.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
    $this->tempStoreFactory->get('content_sync_multiple_confirm')
      ->delete(\Drupal::currentUser()->id());
    $this->redirect('system.admin_content');
  }

}
