<?php

namespace Drupal\contentchain\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\node\Entity\Node;

/**
 * Manager for the Action Entity CRUD operations.
 */
class ActionEntityController {

  /**
   * Function to update or delete(soft delete)a single action entity items.
   *
   * JsonResponse @return
   * array of updated action item information.
   */
  public function actionEntity(Request $request) {
    $headers = getallheaders();
    $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);

    // User time stamp.
    $date = DateTimePlus::createFromTimestamp(time());
    $last_update = $date->format('Y-m-d H:i:s');

    $content = json_decode($request->getContent(), TRUE);

    if ($content['id'] != '' && $uid !== FALSE) {
      $action_item = $content['id'];
      $display_title = $content['title'];

      $itemStatus = isset($content['status']) ? $content['status'] : 0;
      $status = $this->statusToString($itemStatus);
      $notification_date = isset($content['notificationDate']) ? DateTimePlus::createFromFormat('m-d-Y H:i:s', $content['notificationDate'])->format('Y-m-d H:i:s') : NULL;
      $completed = isset($content['completed']) ? $content['completed'] : 0;

      // Loads action entity node from provided ID.
      $actionEntity = Node::load($action_item);

      $tid = \Drupal::service('headless_toolbox.content')->getTidbyTermName($content['category']);

      $result = FALSE;
      if (isset($actionEntity)&&($actionEntity->getOwnerId() == $uid)) {
        if (isset($content['whenCompleted'])) {
          $when_completed = DateTimePlus::createFromFormat('m-d-Y H:i:s', $content['whenCompleted'])
            ->format('Y-m-d H:i:s');
          if ($actionEntity->get('field_checkins_dates')->getValue()[0]['value']) {
            // If Exiting data, then update the value.
            $checkinsArray = !empty($actionEntity->get('field_checkins_dates')->getValue()[0]['value']) ? unserialize($actionEntity->get('field_checkins_dates')->getValue()[0]['value']) : NULL;
            array_push($checkinsArray, $content['whenCompleted']);
            $checkins_dates = serialize($checkinsArray);
          }
          else {
            // If null ,add a new value.
            $checkins_dates = isset($content['whenCompleted']) ? serialize(array($content['whenCompleted'])) : NULL;
          }
        }

        $actionEntity->set('title', $display_title);
        $actionEntity->set('field_status', $status);
        $actionEntity->set('field_weight', isset($content['weight']) ? $content['weight'] : "0");
        $actionEntity->set('field_daily_completion_statuses', isset($content['dailyCompletionStatuses']) ? implode(", ", $content['dailyCompletionStatuses']) : 0);
        $actionEntity->set('field_earliest_completion_status', isset($content['earliestCompletionStatusDate']) ? $content['earliestCompletionStatusDate'] : NULL);
        $actionEntity->set('field_notification_date', $notification_date);
        $actionEntity->set('field_completed', $completed);
        $actionEntity->set('field_when_completed', isset($when_completed) ? $when_completed : $last_update);
        $actionEntity->setChangedTime(time());
        $actionEntity->set('field_is_recurrence', isset($content['is_recurrence']) ? (string) $content['is_recurrence'] : "0");
        $actionEntity->set('field_recurrence_interval', isset($content['recurrence_interval']) ? $content['recurrence_interval'] : 'None');
        $actionEntity->set('field_checkins_dates', isset($checkins_dates) ? $checkins_dates : NULL);
        $actionEntity->set('field_category', isset($tid) ? $tid : NULL);
        $actionEntity->set('field_notes', isset($content['notes']) ? $content['notes'] : '');
        $actionEntity->set('field_show_notification', isset($content['show_notification']) ? $content['show_notification'] : FALSE);
        $actionEntity->save();
        $result = TRUE;
      }
      else {
        $response = array(
          'errorMessage' => t('Error: This action does not belong to this user'),
        );
        return new JsonResponse($response, 400);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error: Missing Parameters'),
      );
      return new JsonResponse($response, 400);
    }
    if ($result) {
      $response = array(
        'id' => (string) $action_item,
        // Important: Transactions must always use the uuid.
        'UUID' => $headers['UUID'],
        'title' => $display_title,
        'status' => isset($itemStatus) ? (string) $itemStatus : "1",
        'notificationDate' => date_format(date_create($notification_date), 'm-d-Y H:i:s'),
      );
      return new JsonResponse($response, 200);
    }
    else {
      $response = array(
        'errorMessage' => t('Error while saving the action, please try again.'),
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * List of action entity items for a user.
   *
   * Status 0 = Active
   * Status 1 = completed
   * Status 2 = Deleted.
   *
   * @Return array  of active and completed action entity items list
   */
  public function actionEntityList(Request $request) {
    $headers = getallheaders();
    $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);

    // User UUID.
    if ($uid !== FALSE) {

      $query = \Drupal::entityQuery('node')
        ->condition('uid', $uid, '=')
        ->condition('type', 'action_item_entity');

      $nids = $query->execute();

      $actions = Node::loadMultiple($nids);

      $i = 0;
      $items = array();

      foreach ($actions as $values) {
        $status = $this->statusToInt($values->get('field_status')->getValue()[0]['value']);
        $walkingActionItem = !empty($values->get('field_walking_action_item')->getValue()[0]['value']) ? $values->get('field_walking_action_item')->getValue()[0]['value'] : "0";
        $items[$i]['title'] = $values->get('title')->getValue()[0]['value'];
        if (!empty($values->get('field_notification_date')->getValue())) {
          $items[$i]['notificationDate'] = date_format(date_create($values->get('field_notification_date')->getValue()[0]['value']), 'm-d-Y H:i:s');
        }
        $items[$i]['status'] = (string) $status;
        $items[$i]['weight'] = "0";
        if ($values->get('field_completed')->getValue() != NULL) {
          $items[$i]['completed'] = $values->get('field_completed')->getValue()[0]['value'];
        }
        // Important: Transactions must always use the uuid.
        $items[$i]['UUID'] = $headers['UUID'];
        if ($values->get('field_when_completed')->getValue() != NULL) {
          $items[$i]['whenCompleted'] = date_format(date_create($values->get('field_when_completed')->getValue()[0]['value']), 'm-d-Y H:i:s');
        }
        if (!empty($values->get('field_chainid')->getValue())) {
          $items[$i]['chainId'] = $values->get('field_chainid')->getValue()[0]['value'];
        }
        $items[$i]['id'] = $values->get('nid')->getValue()[0]['value'];
        $items[$i]['is_recurrence'] = !empty($values->get('field_is_recurrence')->getValue()[0]['value']) ? $values->get('field_is_recurrence')->getValue()[0]['value'] : "0";
        $items[$i]['recurrence_interval'] = !empty($values->get('field_recurrence_interval')->getValue()[0]['value']) ? $values->get('field_recurrence_interval')->getValue()[0]['value'] : 'None';
        $items[$i]['walkingActionItem'] = (string) $walkingActionItem;
        $items[$i]['dailyCompletionStatuses'] = explode(',', $values->get('field_daily_completion_statuses')->getValue()[0]['value']);
        $items[$i]['earliestCompletionStatusDate'] = $values->get('field_earliest_completion_status')->getValue()[0]['value'];
        if (!empty($values->get('field_learning_resources')->getValue())) {
          $h = 0;
          foreach ($values->get('field_learning_resources')->getValue() as $related) {
            $items[$i]['relatedResources'][$h] = $related["target_id"];
            $h++;
          }
        }
        $items[$i]['category'] = \Drupal::service('headless_toolbox.content')->getTermNamebyTid($values->get('field_category')->getValue()[0]['target_id']);
        if (!empty($values->get('field_notes')->getValue())) {
          $items[$i]['notes'] = $values->get('field_notes')->getValue()[0]['value'];
        }
        $items[$i]['show_notification'] = $values->get('field_show_notification')->getValue()[0]['value'];
        $i++;
      }
      if (count($items) == 0) {
        $items = array();
      }
      return new JsonResponse($items, 200);
    }
    else {
      $items = array();
      return new JsonResponse($items, 200);
    }
  }

  /**
   * Function to create a single action entity item.
   *
   * JSON @param requestArray.
   *
   * JsonResponse @return
   * Array of created action entity item information.
   */
  public function actionEntityAdd(Request $request) {
    $headers = getallheaders();
    $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);

    // User time stamp.
    $date = DateTimePlus::createFromTimestamp(time());
    $last_update = $date->format('Y-m-d H:i:s');

    $content = json_decode($request->getContent(), TRUE);

    $notification_date = isset($content['notificationDate']) ? DateTimePlus::createFromFormat('m-d-Y H:i:s', $content['notificationDate'])->format('Y-m-d H:i:s') : NULL;
    $completed = isset($content['completed']) ? $content['completed'] : 0;
    if (isset($content['whenCompleted'])) {
      $when_completed = DateTimePlus::createFromFormat('m-d-Y H:i:s', $content['whenCompleted'])->format('Y-m-d H:i:s');
      if (isset($content['field_checkins_dates']) ? $content['field_checkins_dates'] : 0) {
        // If Exiting data, then update the value.
        $checkinsArray = !empty($content['field_checkins_dates']) ? unserialize($content['field_checkins_dates']) : NULL;
        array_push($checkinsArray, $content['whenCompleted']);
        $checkins_dates = serialize($checkinsArray);
      }
      // If null ,add a new value.
      else {
        $checkins_dates = isset($content['whenCompleted']) ? serialize(array($content['whenCompleted'])) : NULL;
      }
    }
    $itemStatus = isset($content['status']) ? $content['status'] : 0;
    $status = $this->statusToString($itemStatus);

    $tid = \Drupal::service('headless_toolbox.content')->getTidbyTermName($content['category']);

    $missing_params = FALSE;
    $missing_msg = 'The following parameters are missing: ';
    $required_params = array(
      'title',
      'category',
      'notes',
      'show_notification',
    );
    foreach ($required_params as $param) {
      if (!isset($content[$param])) {
        $missing_msg .= $param . ', ';
        $missing_params = TRUE;
      }
    }
    if (!$missing_params&&($uid !== FALSE)) {
      $node = Node::create([
        'type'                    => 'action_item_entity',
        'title'                   => $content['title'],
        'uid'                     => $uid,
        'field_category'          => isset($tid) ? $tid : NULL,
        'field_notes'             => $content['notes'],
        'field_show_notification' => $content['show_notification'],
        'field_status'            => $status,
        'field_weight'            => isset($content['weight']) ? $content['weight'] : 0,
        'field_daily_completion_statuses'  => isset($content['dailyCompletionStatuses']) ? implode(", ", $content['dailyCompletionStatuses']) : 0,
        'field_earliest_completion_status' => isset($content['earliestCompletionStatusDate']) ? $content['earliestCompletionStatusDate'] : NULL,
        'field_notification_date' => $notification_date,
        'field_completed'         => $completed,
        'field_when_completed'    => isset($when_completed) ? $when_completed : $last_update,
        'field_is_recurrence'     => isset($content['is_recurrence']) ? (string) $content['is_recurrence'] : 0,
        'field_recurrence_interval' => isset($content['recurrence_interval']) ? $content['recurrence_interval'] : 'None',
        'field_checkins_dates'    => isset($checkins_dates) ? $checkins_dates : NULL,
      ]);

      $saved = TRUE;

      try {
        $node->save();
      }
      catch (EntityStorageException $e) {
        $saved = FALSE;
      }
    }
    else {
      $saved = FALSE;
      $e = $missing_msg;
    }

    if ($saved) {
      $response = array(
        'id' => $node->get('nid')->getValue()[0]['value'],
        // Important: Transactions must always use the uuid.
        'UUID' => $headers['UUID'],
        'title' => $node->get('title')->getValue()[0]['value'],
        'status' => isset($itemStatus) ? (string) $itemStatus : "0",
        'notificationDate' => date_format(date_create($notification_date), 'm-d-Y H:i:s'),
      );
      return new JsonResponse($response, 200);
    }
    else {
      $response = array(
        'errorMessage' => 'Error: ' . $e,
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * Function to retrieve a single action details.
   *
   * JSON @param requestArray.
   *
   * JsonResponse @return
   * Array of created action entity item information.
   */
  public function actionEntityDetails(Request $request) {
    $headers = getallheaders();
    $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);

    // User time stamp.
    $date = DateTimePlus::createFromTimestamp(time());
    $last_update = $date->format('Y-m-d H:i:s');

    $content = json_decode($request->getContent(), TRUE);
    $nid = $content['id'];
    if (!empty($nid)&&($uid !== FALSE)) {
      // Loads action entity node from provided ID.
      $actionEntity = Node::load($nid);

      if (!empty($actionEntity)) {
        $status = $this->statusToInt($actionEntity->get('field_status')->getValue()[0]['value']);
        if (!empty($uid)) {
          if ($actionEntity->getOwnerId() == $uid) {
            $response = array(
              'id' => $actionEntity->get('nid')->getValue()[0]['value'],
              // Important: Transactions must always use the uuid.
              'UUID' => $headers['UUID'],
              'title' => $actionEntity->get('title')->getValue()[0]['value'],
              'status' => $status,
              'notificationDate' => $actionEntity->get('field_notification_date')->getValue()[0]['value'],
              'walkingActionItem' => $actionEntity->get('field_walking_action_item')->getValue()[0]['value'],
              'weight' => $actionEntity->get('field_weight')->getValue()[0]['value'],
              'completed' => $actionEntity->get('field_completed')->getValue()[0]['value'],
              'whenCompleted' => $actionEntity->get('field_when_completed')->getValue()[0]['value'],
              'isRecurrence' => $actionEntity->get('field_is_recurrence')->getValue()[0]['value'],
              'recurrenceInterval' => $actionEntity->get('field_recurrence_interval')->getValue()[0]['value'],
              'dailyCompletionStatuses' => explode(',', $actionEntity->get('field_daily_completion_statuses')->getValue()[0]['value']),
              'earliestCompletionStatusDate' => $actionEntity->get('field_earliest_completion_status')->getValue()[0]['value'],
              'chainId' => $actionEntity->get('field_chainid')->getValue()[0]['value'],
              'category' => \Drupal::service('headless_toolbox.content')->getTermNamebyTid($actionEntity->get('field_category')->getValue()[0]['target_id']),
              'notes' => $actionEntity->get('field_notes')->getValue()[0]['value'],
              'showNotification' => $actionEntity->get('field_show_notification')->getValue()[0]['value'],
            );
            return new JsonResponse($response, 200);
          }
          else {
            $response = array(
              'errorMessage' => t('Error: This action does not belong to the especified user'),
            );
            return new JsonResponse($response, 403);
          }
        }
        else {
          $response = array(
            'errorMessage' => t('Error: There is no such user in our database'),
          );
          return new JsonResponse($response, 404);
        }
      }
      else {
        $response = array(
          'errorMessage' => t('Error: There is no such action in our database'),
        );
        return new JsonResponse($response, 404);
      }
    }
    else {
      $response = array(
        'errorMessage' => t('Error: There are missing parameters'),
      );
      return new JsonResponse($response, 400);
    }
  }

  /**
   * Function to retrieve a single action related articles.
   *
   * JSON @param requestArray.
   *
   * JsonRequest @return
   * Array of created action entity item information.
   */
  public function actionEntityRelated(Request $request) {
    $headers = getallheaders();
    $uid = \Drupal::service('user_management.user')->getUidFromUuid($headers['UUID']);

    $content = json_decode($request->getContent(), TRUE);
    $nid = $content['id'];

    if (!empty($nid)&&($uid !== FALSE)) {

      // Loads action entity node from provided ID.
      $actionEntity = Node::load($nid);
      if (!empty($actionEntity)) {
        if ($actionEntity->getOwnerId() == $uid) {
          if (!empty($actionEntity->get('field_learning_resources')->getValue())) {
            $i = 0;
            foreach ($actionEntity->get('field_learning_resources')->getValue() as $related) {
              $node = Node::load($related["target_id"]);
              $items['relatedResources'][$i]['id'] = $related["target_id"];
              $items['relatedResources'][$i]['title'] = $node->get('title')->getValue()[0]['value'];
              $i++;
            }
            return new JsonResponse($items, 200);
          }
          else {
            return new JsonResponse($response, 404);
          }
        }
        else {
          $response = array(
            'errorMessage' => t('Error: This user has no access to this action.'),
          );
          return new JsonResponse($response, 403);
        }
      }
      else {
        $response = array(
          'errorMessage' => t('Error: The specified action does not exist.'),
        );
        return new JsonResponse($response, 404);
      }

    }
    else {
      $response = array(
        'errorMessage' => t('Error: There are missing parameters'),
      );
      return new JsonResponse($response, 400);
    }

  }

  /**
   * Returns the string equivalent for the int status.
   *
   * String @return
   * 0 Active.
   * 1 Inactive.
   * 2 Deleted.
   */
  private function statusToString($intStatus) {
    switch ($intStatus) {
      case 0:
        $status = 'Active';
        break;

      case 1:
        $status = 'Inactive';
        break;

      case 2:
        $status = 'Deleted';
        break;

      default:
        $status = 'Active';
        break;
    }
    return $status;
  }

  /**
   * Returns the int equivalent for the string status.
   *
   * Int @return
   * 0 Active.
   * 1 Inactive.
   * 2 Deleted.
   */
  private function statusToInt($stringStatus) {
    switch ($stringStatus) {
      case 'Active':
        $status = 0;
        break;

      case 'Inactive':
        $status = 1;
        break;

      case 'Deleted':
        $status = 2;
        break;

      default:
        $status = 0;
        break;
    }
    return $status;
  }

}
