<?php

namespace Drupal\content_sync\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Sync normalizer decorator plugins.
 */
abstract class SyncNormalizerDecoratorBase extends PluginBase implements SyncNormalizerDecoratorInterface {


  // Add common methods and abstract methods for your plugin type here.
}
