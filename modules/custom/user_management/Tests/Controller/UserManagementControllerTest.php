<?php

namespace Drupal\user_management\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the user_management module.
 */
class UserManagementControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "user_management UserManagementController's controller functionality",
      'description' => 'Test Unit for module user_management and controller UserManagementController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests user_management functionality.
   */
  public function testUserManagementController() {
    // Check that the basic functions of module user_management.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
