<?php

namespace Drupal\behaviour_science_data;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the BCT Metadata entity.
 *
 * @see \Drupal\behaviour_science_data\Entity\BCTMetadata.
 */
class BCTMetadataAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\behaviour_science_data\Entity\BCTMetadataInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished bct metadata entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published bct metadata entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit bct metadata entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete bct metadata entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add bct metadata entities');
  }

}
