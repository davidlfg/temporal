<?php

namespace Drupal\rest_content_sync\Normalizer;

use \Drupal\content_sync\Normalizer\FileEntityNormalizer as FileEntityNormalizerBase;

/**
 * Adds the file URI to embedded file entities.
 */
class FileEntityNormalizer extends FileEntityNormalizerBase {

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $class, $format = NULL, array $context = array()) {
    $file_data = '';
    if (!empty($data['data'][0]['value'])) {
      $file_data = $data['data'][0]['value'];
      // Avoid 'data' being treated as a field.
      unset($data['data']);
    }

    $entity = parent::denormalize($data, $class, $format, $context);

    if ($file_data) {
      // Decode and save to file.
      $file_contents = base64_decode($file_data);
      $dirname = $this->fileSystem->dirname($entity->getFileUri());
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);
      if ($uri = file_unmanaged_save_data($file_contents, $entity->getFileUri())) {
        $entity->setFileUri($uri);
      }
      else {
        throw new \RuntimeException(SafeMarkup::format('Failed to write @filename.', array('@filename' => $entity->getFilename())));
      }
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = array()) {
    $data = parent::normalize($object, $format, $context);
    /* @var $object \Drupal\file\FileInterface */
    if (!empty($context['expose_image'])) {
      // Save base64-encoded file contents to the "data" property.
      $file_data = base64_encode(file_get_contents($this->fileSystem->realpath($object->getFileUri())));
      $data['data'] = [['value' => $file_data]];
    }
    return $data;
  }

}
