<?php

namespace Drupal\remote_content_sync;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * EntityOperations class with hook bridges.
 */
class EntityOperations implements ContainerInjectionInterface {

  const OPERATION_INSERT = 'insert';
  const OPERATION_UPDATE = 'update';
  const OPERATION_DELETE = 'delete';

  protected $remoteEntityOperations;
  protected $queueFactory;
  protected $remoteSiteResolver;

  /**
   * {@inheritdoc}
   */
  public function __construct(RemoteEntityOperations $remote_entity_operations, QueueFactory $queue_factory, RemoteSiteResolver $remoteSiteResolver) {
    $this->remoteEntityOperations = $remote_entity_operations;
    $this->queueFactory = $queue_factory;
    $this->remoteSiteResolver = $remoteSiteResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('remote_content_sync.remote_entity_operations'),
      $container->get('queue'),
      $container->get('remote_content_sync.remote_site_resolver')
    );
  }

  /**
   * Hook bridge.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that was just saved.
   *
   * @see hook_entity_update()
   */
  public function entityUpdate(ContentEntityInterface $entity) {
    $this->queueOperation($entity, static::OPERATION_UPDATE);
  }

  /**
   * Hook bridge.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that was just inserted.
   *
   * @see hook_entity_insert()
   */
  public function entityInsert(ContentEntityInterface $entity) {
    $this->queueOperation($entity, static::OPERATION_INSERT);
  }

  /**
   * Hook bridge.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that was just deleted.
   *
   * @see hook_entity_delete()
   */
  public function entityDelete(ContentEntityInterface $entity) {
    $this->queueOperation($entity, static::OPERATION_DELETE);
  }

  /**
   * Adds an operatio to the queue.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be added.
   * @param string $operation
   *   The operation to execute on the entity.
   */
  protected function queueOperation(ContentEntityInterface $entity, $operation) {
    // Remote Site.
    // Needs to be resolved for host; children should be sent to the same site.
    $remote_sites = $this->remoteSiteResolver->getRemoteSites($entity);
    if (!empty($remote_sites)) {
      // Queue.
      $queue = $this->queueFactory->get('remote_content_sync');
      foreach ($remote_sites as $remote_site) {
        $queue->createItem(
          (object) [
            'remote_site_id' => $remote_site->id(),
            'entity_id' => $entity->id(),
            'entity_uuid' => $entity->uuid(),
            'entity_type_id' => $entity->getEntityTypeId(),
            'operation' => $operation,
          ]
        );
      }
    }
  }

}
