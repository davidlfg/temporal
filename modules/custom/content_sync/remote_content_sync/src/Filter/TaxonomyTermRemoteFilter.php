<?php

namespace Drupal\remote_content_sync\Filter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Taxonomy Term Remote Filter class.
 */
class TaxonomyTermRemoteFilter extends RemoteFilter {

  /**
   * {@inheritdoc}
   */
  public function filter(ContentEntityInterface $entity) {
    if (empty($this->configuration['terms'])) {
      return TRUE;
    }
    $references = $entity->referencedEntities();
    $i = 0;
    foreach ($references as $reference) {
      if ($reference instanceof Term && in_array($reference->uuid(), $this->configuration['terms'])) {
        if ($this->operator === self::OR_OPERATOR) {
          return TRUE;
        }
        $i++;
      }
    }
    if ($i === count($this->configuration['terms'])) {
      return TRUE;
    }
    return FALSE;
  }

}
