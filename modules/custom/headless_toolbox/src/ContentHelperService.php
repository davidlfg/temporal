<?php

namespace Drupal\headless_toolbox;

use Drupal\taxonomy\Entity\Term;

/**
 * Class ContentHelperService.
 *
 * @package Drupal\headless_toolbox
 */
class ContentHelperService {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Function to retrieve the term name for a term ID.
   */
  public function getTermNamebyTid($tid) {
    $term = Term::load($tid);
    return $term->get('name')->value;
  }

  /**
   * Function to retrieve the term ID for a term name.
   */
  public function getTidbyTermName($name) {
    $category = taxonomy_term_load_multiple_by_name($name);
    $term = reset($category);
    return $term->get('tid')->value;
  }

}
