<?php

namespace Drupal\behaviour_science_data\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for BCT Metadata edit forms.
 *
 * @ingroup behaviour_science_data
 */
class BCTMetadataForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\behaviour_science_data\Entity\BCTMetadata */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label BCT Metadata.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label BCT Metadata.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.bct_metadata.canonical', ['bct_metadata' => $entity->id()]);
  }

}
