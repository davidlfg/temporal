<?php

namespace Drupal\content_sync\Plugin\SyncNormalizerDecorator;

use Drupal\Component\Serialization\Yaml;
use Drupal\content_sync\Plugin\SyncNormalizerDecoratorBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides a decorator to set default vaules to the fields.
 *
 * @SyncNormalizerDecorator(
 *   id = "field_default_value",
 *   name = @Translation("Field Default Value"),
 * )
 */
class FieldDefaultValue extends SyncNormalizerDecoratorBase {

  /**
   * {@inheritdoc}
   */
  public function decorateNormalization(array &$normalized_entity, ContentEntityInterface $entity, $format, array $context = []) {
    $field_defaults = $this->getDefaultValues($context);
    if (!empty($field_defaults)) {
      foreach ($field_defaults as $key => $default_value) {
        if (empty($normalized_entity[$key])) {
          $normalized_entity[$key] = $default_value;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function decorateDenormalization(array &$normalized_entity, $type, $format, array $context = []) {
    $field_defaults = $this->getDefaultValues($context);
    if (!empty($field_defaults)) {
      foreach ($field_defaults as $key => $default_value) {
        if (empty($normalized_entity[$key])) {
          $normalized_entity[$key] = $default_value;
        }
      }
    }
  }

  /**
   * Extract the field map from the serializer context.
   *
   * @param array $context
   *   The serializer context.
   *
   * @return array
   *   The field map.
   */
  protected function getDefaultValues($context) {
    $field_defaults = [];
    if (!empty($context['field_default_value'])) {
      $field_defaults = $context['field_default_value'];
    }
    elseif (!empty($context['content_sync_directory'])) {
      $file = realpath($context['content_sync_directory'] . '/field_default_value.yml');
      if ($file && file_exists($file)) {
        $field_defaults = Yaml::decode(file_get_contents($file));
        if (isset($field_defaults)) {
          $field_defaults = $field_defaults;
        }
      }
    }
    return $field_defaults;
  }

}
