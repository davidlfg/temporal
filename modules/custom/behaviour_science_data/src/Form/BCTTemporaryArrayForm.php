<?php

namespace Drupal\behaviour_science_data\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for BCT Temporary Array edit forms.
 *
 * @ingroup behaviour_science_data
 */
class BCTTemporaryArrayForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\behaviour_science_data\Entity\BCTTemporaryArray */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label BCT Temporary Array.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label BCT Temporary Array.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.bct_temporary_array.canonical', ['bct_temporary_array' => $entity->id()]);
  }

}
