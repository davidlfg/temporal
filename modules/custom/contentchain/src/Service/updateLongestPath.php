<?php
/**
 * Short description for file
 *
 * Long description for file (if any)...
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release x.x.x
 */

namespace Drupal\contentchain\Service;

use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\contentchain\Controller\individualchainController;


class updateLongestPath {
  /**
   * Function for updating content chain longest path
   * @param $entity
   */
  public function updateDecisionTreeLongestPath($entity) {
    $contentChain = $entity->field_content_chain;
    //$contentChainCount = count($entity->field_content_chain);
    $i = 0;
    $counter = 0;

    $chainCounter = count($contentChain);
    //\Drupal::logger('$contentChainCount')->notice($chainCounter);

    foreach ($contentChain as $key => $value) {
      $linkId = $value->target_id;
      $chainLink = \Drupal\node\Entity\Node::load($linkId);
      $linkTypeArray = $chainLink->get('type')->getValue();
      $chainLinkType = $linkTypeArray[0]['target_id'];

      if ($chainLinkType == 'content_templates_voice_over') {
        $voiceOverSlides = $chainLink->field_voice_over_slide->getValue();
        $slideCount = count($voiceOverSlides);
        $counter = $counter + $slideCount;
      }
      else {
        $counter++;
        $i++;
      }
    }

    //get last link id from the content chain
    $promptContetType = array(
      'prompt_text_only',
      'prompt_text_image_audio',
      'prompt_text_image',
      'prompt_text_audio'
    );
    $isDecisionTree = 0;
    if (in_array($chainLinkType, $promptContetType)) {
      $actionId = $chainLink->field_prompt_action->getValue()[0]['target_id'];
      //load Action Entity
      $actionEntity = \Drupal\node\Entity\Node::load($actionId);
      //Check any question type ==button
      if (count($actionEntity->field_buttons->getValue()) > 0) {
        //load field collection entity to check if any decision tree
        foreach ($actionEntity->field_buttons->getValue() as $key => $subArray) {
          $fc = FieldCollectionItem::load($subArray['value']);
          if (isset($fc->field_next_chain_id->getValue()[0]['target_id'])) {
            $isDecisionTree = 1;
            //Decision Tree Exist
            /*Recalculate longest chain path*/
          }
        }
      }
      //$contentChainObj = new individualchainController();
      //$path = $contentChainObj->getChainLongestPath($linkId);
    }

    if ($isDecisionTree) {
      $entity->set('field_is_decision_tree', 1);
      //If decision tree setting the clongest path to Zero, for more calculation.
      $entity->set('field_chain_longest_path', 0);
    }
    else {
      $entity->set('field_is_decision_tree', 0);
      $entity->set('field_chain_longest_path', $counter);
    }
    /*
     *   $contentChainObj = new individualchainController();
         $contentChainObj->getChainLongestPath()
     */


    $entity->save();
    return $entity;
  }

}