<?php

namespace Drupal\video_embed_brightcove_cms;

/**
 * Interface BrightcoveReaderInterface.
 *
 * @package Drupal\video_embed_brightcove_cms
 */
interface BrightcoveReaderInterface {

  /**
   * Gets the image for a single video.
   *
   * @param $video_id string The Brightcove video ID
   * @return mixed The image URL or FALSE if the image cannot be found
   */
  public function getVideoImage($video_id);

  /**
   * Gets the thumbnail for a single video.
   *
   * @param $video_id string The Brightcove video ID
   * @return mixed The image URL or FALSE if the image cannot be found
   */
  public function getVideoThumbnail($video_id);

}
