<?php

namespace Drupal\oauth_service_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OauthConnectorConfigForm.
 *
 * @package Drupal\oauth_service_connector\Form
 */
class OauthConnectorConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'oauth_service_connector.OauthConnectorConfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oauth_connector_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('oauth_service_connector.OauthConnectorConfig');

    $form['local_website'] = [
      '#type' => 'details',
      '#title' => $this->t('Local Website Configuration'),
      '#open' => FALSE,
    ];
    $default_user_roles = $config->get('default_user_roles');
    $form['local_website']['default_user_roles'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default User Roles'),
      '#description' => $this->t('List out all user roles to be added for a new user(separated by coma).'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => isset($default_user_roles) ? $default_user_roles : '',
    ];

    $form['remote_website'] = [
      '#type' => 'details',
      '#title' => $this->t('Remote Website Configuration'),
      '#open' => FALSE,
    ];
    $form['remote_website']['basic_auth'] = [
      '#type' => 'details',
      '#title' => $this->t('Header Authorization'),
      '#open' => FALSE,
    ];
    $form['remote_website']['basic_auth']['header_authentication'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Header Authorization'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('header_authentication'),
    ];

    $form['remote_website']['oauth_service_api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService API Base URL'),
      '#description' => $this->t('OAuth MicroService API Base URL (It may be Apigee URL also).'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_api_base_url'),
    ];
    $form['remote_website']['oauth_service_validate_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService Validate Email'),
      '#description' => $this->t('End points for Validate Email.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_validate_email'),
    ];
    $form['remote_website']['oauth_service_create_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService Create User'),
      '#description' => $this->t('End points for Create User.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_create_user'),
    ];
    $form['remote_website']['oauth_service_user_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService User Login'),
      '#description' => $this->t('End points for User Login.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_user_login'),
    ];
    $form['remote_website']['oauth_service_user_logout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService User Login out'),
      '#description' => $this->t('End points for User Login out.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_user_logout'),
    ];
    $form['remote_website']['oauth_service_user_profile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService User Get User Profile'),
      '#description' => $this->t('End points for retiving User Profile.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_user_profile'),
    ];
    $form['remote_website']['oauth_service_update_user_profile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService User Profile Profile'),
      '#description' => $this->t('End points for updating User Profile.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_update_user_profile'),
    ];
    $form['remote_website']['oauth_service_update_user_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth MicroService User Profile update password'),
      '#description' => $this->t('End points for updating User password.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('oauth_service_update_user_password'),
    ];
    $form['user_migration_record'] = [
      '#type' => 'details',
      '#title' => $this->t('User Migration Record(uid not Migrated)'),
      '#open' => FALSE,
    ];
    $form['user_migration_record']['user_migration_record_ids'] = [
      '#type' => 'textarea',
      '#title' => $this->t('User Migration Record uid not Migrated'),
      '#description' => $this->t('Ids'),
      '#rows' => 15,
      '#resizable' =>'horizontal',
      '#default_value' => $config->get('user_migration_record_ids'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('oauth_service_connector.OauthConnectorConfig')
      ->set('oauth_service_api_base_url', $form_state->getValue('oauth_service_api_base_url'))
      ->set('oauth_service_validate_email', $form_state->getValue('oauth_service_validate_email'))
      ->set('oauth_service_create_user', $form_state->getValue('oauth_service_create_user'))
      ->set('oauth_service_user_login', $form_state->getValue('oauth_service_user_login'))
      ->set('oauth_service_user_logout', $form_state->getValue('oauth_service_user_logout'))
      ->set('oauth_service_user_profile', $form_state->getValue('oauth_service_user_profile'))
      ->set('default_user_roles', $form_state->getValue('default_user_roles'))
      ->set('oauth_service_update_user_profile', $form_state->getValue('oauth_service_update_user_profile'))
      ->set('header_authentication', $form_state->getValue('header_authentication'))
      ->set('user_migration_record_ids', $form_state->getValue('user_migration_record_ids'))
      ->set('oauth_service_update_user_password', $form_state->getValue('oauth_service_update_user_password'))
      ->save();
  }

}
