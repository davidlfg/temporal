<?php
/**
 * chainprogressionController file
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Datetime\DateTimePlus;

class chainprogressionController {
  public function chain_progression(Request $request) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }

    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $uid = $headlessAccount->id();

    $content = json_decode($request->getContent(), TRUE);
    $chainid = $content['chainId'];
    $progress = $content['progress'];
    $lastSeenContentId = $content['lastSeenContentId'];
    $date = DateTimePlus::createFromFormat('m-d-Y H:i:s', $content['dataTime']);
    $datetime = $date->format('Y-m-d H:i:s');

    $progression_id = \Drupal::database()->select('chain_progression', 'cp')
      ->fields('cp', array('progression',))
      ->condition('cp.uid', $uid)
      ->condition('cp.chain_id', $chainid)
      ->execute()
      ->fetchField();

    if ($progression_id) {
      $result = \Drupal::database()->update('chain_progression')
        ->fields(array(
          'link_id' => $lastSeenContentId,
          'progress_percent' => $progress,
          'datetime' => $datetime,
        ))
        ->condition('progression', $progression_id, '=')
        ->execute();
    }
    else {
      $fields = array(
        'uid' => $uid,
        'chain_id' => $chainid,
        'link_id' => $lastSeenContentId,
        'progress_percent' => $progress,
        'datetime' => $datetime
      );
      $result = \Drupal::database()->insert('chain_progression')
        ->fields($fields)
        ->execute();
    }
    if ($result) {
      $response = array(
        'Message' => 'Success',
      );
      return new JsonResponse($response, 200);
    }
    else {
      $response = array(
        'errorMessage' => 'Error',
      );
      return new JsonResponse($response, 400);
    }


  }


}