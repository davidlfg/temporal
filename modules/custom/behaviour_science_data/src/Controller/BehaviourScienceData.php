<?php

namespace Drupal\behaviour_science_data\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\file\Entity\File;

/**
 * Class BehaviourScienceData.
 *
 * @package Drupal\behaviour_science_data\Controller
 */
class BehaviourScienceData extends ControllerBase {

  /**
   * File_id_mapping.
   *
   * @return string
   *   Return Hello string.
   */
  public function file_id_mapping() {
    $view = views_embed_view('bct_fileid_maping', 'rest_export_1');
    $view_result = \Drupal::service('renderer')->render($view);
    $output_json = (string) $view_result;
    $data = json_decode($output_json, TRUE);
    $item = array();
    $order = 1;
    foreach ($data as $key => $value) {
      $linkId = $value['nid']['0']['value'];
      $res = self::getContentJoinType($linkId);
      $url = "";
      $fileId = "";
      if (isset($value['field_audio_content']['0']['target_id'])) {
        $fileId = $value['field_audio_content']['0']['target_id'];
        $url = $value['field_audio_content']['0']['url'];
      }
      if (isset($value['field_image_content']['0']['target_id'])) {
        $fileId = $value['field_image_content']['0']['target_id'];
        $url = $value['field_image_content']['0']['url'];
      }
      if (isset($value['field_video_content']['0']['target_id'])) {
        $fileId = $value['field_video_content']['0']['target_id'];
        $url = $value['field_video_content']['0']['url'];
      }
      if (isset($value['field_voice_over_file']['0']['target_id'])) {
        $fileId = $value['field_voice_over_file']['0']['target_id'];
        $url = $value['field_voice_over_file']['0']['url'];
      }
      if (isset($value['field_image']['0']['target_id'])) {
        $fileId = $value['field_image']['0']['target_id'];
        $url = $value['field_image']['0']['url'];
      }
      $item[$order]['Sl No'] = $order;
      $item[$order]['Chain Id'] = isset($res['chainId']) ? $res['chainId'] : '';
      $item[$order]['Link Id'] = $linkId;
      $item[$order]['Type'] = isset($res['jType']) ? $res['jType'] : '';
      $item[$order]['file Id'] = $fileId;
      $item[$order]['File URL'] = $url;
      $order++;
    }
//Voice Over

    $view = views_embed_view('bct_fileid_maping', 'rest_export_2');
    $view_result = \Drupal::service('renderer')->render($view);
    $output_json = (string) $view_result;
    $data1 = json_decode($output_json, TRUE);
    $i = count($item);
    foreach ($data1 as $keys => $value) {
      if (count($value['field_voice_over_file']) > 0) {
        $linkId = $value['nid']['0']['value'];
        $res = self::getContentJoinType($linkId);
        $order++;
        $item[$order]['Sl No'] = $order;
        $item[$order]['Chain Id'] = isset($res['chainId']) ? $res['chainId'] : '';
        $item[$order]['Link Id'] = $value['nid']['0']['value'];
        $item[$order]['Type'] = isset($res['jType']) ? $res['jType'] : '';
        $item[$order]['file Id'] = $value['field_voice_over_file']['0']['target_id'];
        $item[$order]['File URL'] = $value['field_voice_over_file']['0']['url'];
      }
      foreach ($value['field_voice_over_slide'] as $slideKey => $slides) {
        $linkId = $value['nid']['0']['value'];
        $res = self::getContentJoinType($linkId);
        $fc = FieldCollectionItem::load($slides['value'])->toArray();
        $field_slide_image = $fc['field_slide_image'];
        if (count($field_slide_image) > 0) {
          $order++;
          $image = File::load($field_slide_image['0']['target_id'])->toArray();
          $item[$order]['Sl No'] = $order;
          $item[$order]['Chain Id'] = isset($res['chainId']) ? $res['chainId'] : '';
          $item[$order]['Link Id'] = $value['nid']['0']['value'];
          $item[$order]['Type'] = isset($res['jType']) ? $res['jType'] : '';
          $item[$order]['file Id'] = $field_slide_image['0']['target_id'];
          $item[$order]['File URL'] = file_create_url($image['uri']['0']['value']);
        }
      }

    }

    // filename for download
    $filename = "FileId-Mapping-" . date('Ymd') . ".xls";
    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: application/vnd.ms-excel");

    $flag = FALSE;
    foreach ($item as $row) {
      if (!$flag) {
        // display field/column names as first row
        echo implode("\t", array_keys($row)) . "\r\n";
        $flag = TRUE;
      }
      array_walk($row, array('self', 'cleanData'));
      echo implode("\t", array_values($row)) . "\r\n";
    }

    exit;
  }


  /*
   *
   */
  public function cleanData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", " ", $str);
    if (strstr($str, '"')) {
      $str = '"' . str_replace('"', '""', $str) . '"';
    }
  }

  /**
   * @param $nid
   * @return string
   */
  public function getContentJoinType($nid) {
    $res = [];
    $chainId = \Drupal::database()
      ->select('node__field_content_chain', 'cc')
      ->fields('cc', array('entity_id'))
      ->condition('cc.field_content_chain_target_id', $nid)
      ->execute()
      ->fetchField();
    if ($chainId) {
      $jointReplacementType = \Drupal\node\Entity\Node::load($chainId)
        ->get('field_joint_replacement_type')
        ->getValue()[0]['target_id'];
      $jointType = isset($jointReplacementType) ? $jointReplacementType : 'NULL';
      switch ($jointType) {
        case '9':
          $jType = 'knees';
          break;
        case '10':
          $jType = 'hips';
          break;
        default:
          $jType = 'NULL';
          break;
      }
      $res = array('chainId' => $chainId, 'jType' => $jType);
    }
    return $res;
  }

}
