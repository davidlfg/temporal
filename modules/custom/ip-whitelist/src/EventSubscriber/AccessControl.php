<?php

/**
 * This file contains the event listener for all requests,
 * @file AccessControl.php
 */
namespace Drupal\colorful_list\EventSubscriber;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Url;

class AccessControl implements EventSubscriberInterface {

  private $whiteListUris = [];
  private $whiteListIps = [];

  /**
   * @param GetResponseEvent $event
   */
  public function whiteListAccess(GetResponseEvent $event) {
    $config = \Drupal::config('colorful_list.settings');
    //get config whitelist_status
    if (!($config->get('whitelist_status') ?: 0)) {
      return;
    }
    //get config whiteListUris
    $this->whiteListUris = array_map('trim', explode("\n", ($config->get('whitelist_paths') ?: '')));
    if (empty($this->whiteListUris)) {
      return;
    }

    $currentUri = Url::fromRoute('<current>')->getInternalPath();
    // Adding leading slash for standardizing regular expressions and avoiding ambiguous conditions
    if (substr($currentUri, 0, 1) !== '/') {
      $currentUri = '/' . $currentUri;
    }
    //Gets original IP or IP through load balancer
    $http_headers = apache_request_headers();
    if (isset($http_headers["X-Forwarded-For"])) {
      $clientIp = $http_headers["X-Forwarded-For"];
    }
    else {
      $clientIp = \Drupal::request()->getClientIp();
    }
    // Sometimes more than one IP is there, comma separated
    $clientIps = explode(',', $clientIp);
    $clientIp  = reset($clientIps);

    $matchUri = FALSE;
    foreach ($this->whiteListUris as $uriPattern) {
      if ($matchUri = preg_match($uriPattern, $currentUri)) {
        break;
      }
    }
    //get config whitelist_ips
    $this->whiteListIps = array_map('trim', explode("\n", $config->get('whitelist_ips') ?: ''));
    // If URI doesn't match whitelisted, stop processing
    if (!$matchUri || empty($this->whiteListIps)) {
      return;
    }

    $matchIp = FALSE;
    foreach ($this->whiteListIps as $ipPattern) {
      if ($matchIp = preg_match($ipPattern, $clientIp)) {
        break;
      }
    }
    if (!$matchIp) {
      //get config whitelist_exception
      if (($config->get('whitelist_exception') ?: Response::HTTP_NOT_FOUND) == Response::HTTP_NOT_FOUND) {
        throw new NotFoundHttpException();
      }
      else {
        throw new AccessDeniedHttpException('You\'re not authorized to view this page.');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events = array(KernelEvents::REQUEST => array(
        array('whiteListAccess'),
    ));
    return $events;
  }
}
