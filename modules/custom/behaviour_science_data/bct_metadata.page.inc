<?php

/**
 * @file
 * Contains bct_metadata.page.inc.
 *
 * Page callback for BCT Metadata entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for BCT Metadata templates.
 *
 * Default template: bct_metadata.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_bct_metadata(array &$variables) {
  // Fetch BCTMetadata Entity Object.
  $bct_metadata = $variables['elements']['#bct_metadata'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
