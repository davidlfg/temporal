<?php

namespace Drupal\user_management;

use Drupal\user\Entity\User;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\contentchain\Service\ChainWebForms;


/**
 * Class UserManagementService.
 *
 * @package Drupal\user_management
 */
class UserManagementService {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Get request content (urlencoded or JSON)
   * @param $requestContent
   * @param $headers
   * @return array|mixed
   */
  public function getRequestContent($requestContent, $headers) {
    $contentType = isset($headers['content-type'][0]) ? $headers['content-type'][0] : $headers['Content-Type'][0];
    if ($contentType == 'application/x-www-form-urlencoded') {
      parse_str(urldecode($requestContent), $content);
    }
    else {
      if ($contentType == 'application/json') {
        $content = json_decode($requestContent, TRUE);
      }
      else {
        return array('Error' => 'Data format error.');
      }
    }
    return $content;
  }

  /**
   * validateEmail
   * @param $content
   * @param $headers
   * @return array
   */
  public function validateEmail($content, $headers) {
    $email = isset($content['mail']) ? $content['mail'] : NULL;
    $validEmail = \Drupal::service('email.validator')->isValid($email);
    if (!$validEmail) {
      return array('Error' => 'Email id is not valid');
    }
    else {
      $account = user_load_by_mail($email);
      if (isset($account->uid->value)) {
        \Drupal::logger('ValidateEmail')
          ->notice('User Already Exist with Email: %email.', array(
            '%email' => '<' . $email . '>',
          ));
        return array('uuid' => $account->uuid->value);
      }
      else {
        \Drupal::logger('ValidateEmail')
          ->notice('User does not exist with Email: %email.', array(
            '%email' => '<' . $email . '>',
          ));
        return array('uuid' => '');
      }
    }
    //if any previous condition fail, return error
    return array('Error' => 'Error validating Email id');
  }


  /**
   * @param $content
   * @param $headers
   * @return User Profile Array
   */
  public function createUser($content, $headers) {
    $user = User::create();
    //Mandatory settings
    $email = $content['mail'];
    $user->setPassword($content['password']);
    $user->enforceIsNew();
    $user->setEmail($email);
    $user->setUsername($email);//This username must be unique and accept only a-Z,0-9, - _ @ .
    $user->activate();
    $user->set('init', $email);

    //Set Language
    $language_interface = \Drupal::languageManager()->getCurrentLanguage();
    $user->set('langcode', $language_interface->getId());
    $user->set('preferred_langcode', $language_interface->getId());
    $user->set('preferred_admin_langcode', $language_interface->getId());


    //Set timezone
    $timezone = isset($headers['timezone']) ? $headers['timezone'][0] : NULL;
    $defaultTimezone = \Drupal::config('system.date')->get('timezone.default');
    $timezone = isset($timezone) ? $timezone : $defaultTimezone;
    $user->set('timezone', $timezone);

    //User time stamp
    $requestDateTime = isset($headers['usertimestamp']) ? $headers['usertimestamp'][0] : \date('m-d-Y H:i:s');
    $date = DateTimePlus::createFromFormat('m-d-Y H:i:s', $requestDateTime, $timezone);
    $datetime = $date->format('Y-m-d H:i:s');
    $unixTimestamp = strtotime($datetime);
    $user->set('created', $unixTimestamp);
    $user->set('field_registration_date', $date->format('Y-m-d\TH:i:s'));
    $user->set('field_apigee_timestamp', $date->format('Y-m-d\TH:i:s'));

    //Set the same UUID from website to local
    if (isset($content['uuid'])) {
      $user->set('uuid', $content['uuid']);
    }

    //Remove profile fields already set
    unset($content['mail']);
    unset($content['password']);

    //Save user
    $newUser = $user->save();
    if ($newUser) {//Success
      \Drupal::logger('UserRegistration')
        ->notice('New user:  %email.', array(
          '%email' => '<' . $email . '>',
        ));

      //Trigger Welcome Email only for App users
      if (isset($headers['user-agent'])) {
        if (preg_match('/com.jnj.ascm.RedWhiteBlue/i', $headers['user-agent']['0'])) {
          $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
          $mail = _user_mail_notify('register_no_approval_required', $user, $language);
          if (!empty($mail)) {
            \Drupal::logger('user')
              ->notice('Welcome Email has been send to %email.', array('%email' => $email));
          }
          else {
            \Drupal::logger('user')
              ->error('Error sending email to %email.', array('%email' => $email));
          }
        }
      }
      else {
        \Drupal::logger('user-agent')
          ->error('Missing "user-agent" variable in Header.This is required for sending welcome email in Knees and Hips App');
      }

      $account = user_load_by_mail($email);
      return self::getUserProfile($account->uuid());
    }
    else {//Failure
      \Drupal::logger('UserRegistration')
        ->error('Error in user registration: %email.', array(
          '%email' => '<' . $email . '>',
        ));
      return array('Error' => 'Error validating userID.' . $email);
    }
  }

  /**
   * updateProfile
   *
   * @param $content
   * @param $headers
   * @return array
   */
  public function updateProfile($content, $headers) {
    $uuid = $headers['uuid'][0];
    $valid = \Drupal\Component\Uuid\Uuid::isValid($uuid);
    if ($valid) {
      $user = \Drupal::entityManager()
        ->loadEntityByUuid('user', $uuid);
      if ($user) {
        if (isset($content['mail'])) {
          $email = $content['mail'];
          $isEmailUsed = self::isEmail_already_Used($email, $user->id());
          if (isset($isEmailUsed['Error'])) {
            return $isEmailUsed['Error'];
          }
          else {
            $user->setEmail($email);
            $user->setUsername($email);//This username must be unique and accept only a-Z,0-9, - _ @ .
          }
        }

        if (isset($content['password'])) {
          $user->setPassword($content['password']);
        }

        //Remove profile fields already set
        unset($content['mail']);
        unset($content['password']);
        unset($content['temp_password']);
        unset($content['temp_password_created']);

        //User Profile Data in HealthStore
        $formData['uuid'] = $uuid;
        $hsAccountUpdated = \Drupal::service('apigee.connect')
          ->update_health_store_user_profile($content, $uuid);

        try {
          //Save user
          $res = $user->save();
          if ($res) {//Success
            $email = isset($email) ? $email : $user->getEmail();
            \Drupal::logger('UpdateProfile')
              ->notice('User Profile Updated: %name %email.', array(
                '%name' => $email,
                '%email' => '<' . $email . '>',
              ));
            //Get user Profile
            $response = \Drupal::service('user_management.user')
              ->getUserProfile($uuid);
            return $response;
          }
        } catch (Exception $e) {
          if ($e->getResponse()->getStatusCode()) {
            $request = $e->getResponse();
            $headers = $request->getHeaders();
            $response = array(
              'StatusCode' => $request->getStatusCode(),
              'ResponseBody' => json_decode($request->getBody()),
              'ResponseTime' => $headers['Date'][0],
            );
            \Drupal::logger('UpdateProfile')
              ->error(print_r($response, TRUE));
            return array('Error' => 'Error Updating User Profile');
          }
        }

      }
      else {
        return array('Error' => 'Error validating userId');
      }
    }
    else {
      return array('Error' => 'Error validating userId');
    }
  }

  /**
   * Load getUserProfile from HealdLess Drupal and HealthStore
   * @param $uuid
   * @return array
   */
  public function getUserProfile($uuid) {
    $valid = \Drupal\Component\Uuid\Uuid::isValid($uuid);
    if ($valid) {
      $headlessAccount = \Drupal::entityManager()
        ->loadEntityByUuid('user', $uuid);

      if ($headlessAccount) {
        //Get HealthStore Data
        $hsAccount = \Drupal::service('apigee.connect')
          ->get_health_store_user_profile($uuid);

        if (isset($hsAccount['error'])) {
          return array('Error' => $hsAccount['error']);
        }
        //Create user profile Array
        $profile = array();
        $skipKey = array(
          'langcode',
          'preferred_langcode',
          'preferred_admin_langcode',
          'default_langcode',
          'user_picture',
          'login',
          'access',
          'changed',
          'init',
          'uid',
          'pass',
          'roles'
        );
        $userProfile = $headlessAccount->toArray();
        foreach ($userProfile as $field => $value) {
          if (in_array($field, $skipKey)) {
            continue;
          }
          if (count($value) == 0) {
            $profile[$field] = '';
          }
          else {
            $profile[$field] = $value['0']['value'];
          }
        }
        //Updating field_registration_date format
        if (isset($profile['field_registration_date']) and $profile['field_registration_date'] != '') {
          $timezone = \Drupal::config('system.date')->get('timezone.default');
          $date = DateTimePlus::createFromFormat('Y-m-d\TH:i:s', $profile['field_registration_date'], $timezone);
          $profile['field_registration_date'] = $datetime = $date->format('m-d-Y H:i:s');
        }
        //Merge HealthStore Data
        if (isset($hsAccount['uuid'])) {
          foreach ($hsAccount as $field => $value) {
            $profile[$field] = $value;
          }
        }

        //Check and update mailChimp subscription status, if the status if not "subscribed"
        $caretakerEmail = isset($profile['field_caretaker_email']) ? $profile['field_caretaker_email'] : '';
        if ($caretakerEmail and $profile['field_caretaker_subscription_sta'] != 'subscribed') {
          //If status is "pending" , re-check subscription status. If "Subscribed" update the status.
          $subscribe = new ChainWebForms();
          $CareTaker = array(
            'userId' => $headlessAccount->id(),
            'caretakerFirstName' => $profile['field_caretaker_first_name'],
            'caretakerLastName' => $profile['field_caretaker_last_name'],
            'caretakerEmail' => $profile['field_caretaker_email'],
            'jointReplacementType' => $profile['field_joint_replacement_type'],
          );
          $result = $subscribe->CareTakerSubscription($CareTaker, 'Status');
          $CareTaker['subscriptionStatus'] = $result['status'];
          if ($profile['field_caretaker_subscription_sta'] != $result['status']) {
            //Update HealthSore User User Data
            $formData['field_caretaker_subscription_sta'] = $result['status'];
            $formData['uuid'] = $uuid;
            $hsAccountUpdated = \Drupal::service('apigee.connect')
              ->update_health_store_user_profile($formData, $uuid);
            //@TODO :check the response adn add watchdog log

            $profile['field_caretaker_subscription_sta'] = $result['status'];
          }
        }
        //End status check

        return $profile;
      }
      else {
        //User does not exist in Headless.Get user data from HealthStore and create a local user
        $formData = \Drupal::service('apigee.connect')
          ->get_health_store_user_profile($uuid);
        if ($formData['uuid'] == '') {
          //If the user does not exist in HS ,error
          return array('Error' => 'Error validating userId');
        }
        else {
          $formData['password'] = user_password(10);
          global $base_url;
          $url = $base_url . '/user_management/registration';
          $headers = getallheaders();
          $headers['content-type'] = 'application/x-www-form-urlencoded';
          $response = \Drupal::service('apigee.connect')
            ->prepareRequestResponse($url, $headers, $formData, 'post');
          return $response;
        }
      }
    }
    else {
      return array('Error' => 'Error validating userId');
    }
  }

  /**
   * isEmail_already_Used
   * Function to check is the email is already used by
   * any other user for update user profile
   *
   * @param $email
   * @param $userId
   * @return array
   */
  protected function isEmail_already_Used($email, $userId) {
    $existingUserId = \Drupal::database()
      ->select('users_field_data', 'ufd')
      ->fields('ufd', array('uid'))
      ->condition('ufd.mail', $email)
      ->condition('ufd.uid', $userId, '<>')
      ->execute()
      ->fetchField();

    if ($existingUserId) {
      return array(
        'Error' => t('The E-mail ID ' . $email . ' is already taken.'),
      );
    }
    else {
      return array();
    }
  }

  /**
   * @param $headers
   * @return array
   */
  public function validate_user_id($headers) {
    if (!$headers['uuid']) {
      return array('Error' => 'User Id missing in Header');
    }
    else {
      $user = \Drupal::service('user_management.user')
        ->getUserProfile($headers['uuid']);
      if (isset($userProfile['Error'])) {
        return array('Error' => $userProfile['Error']);
      }
      else {
        return $user;
      }
    }


  }

  /*
   * Function to retrieve a user entity from an uuid
   * @param string uuid
   * @return user entity object or FALSE
   */
  public function getUserFromUuid($uuid) {

    $user = \Drupal::entityManager()->loadEntityByUuid('user', $uuid);

    if (!empty($user)) {
      return $user;
    }
    else {
      return FALSE;
    }
  }

  /*
 * Function to retrieve an uid from an uuid
 * @param string uuid
 * @return int uid or FALSE
 */
  public function getUidFromUuid($uuid) {

    $user = \Drupal::entityManager()->loadEntityByUuid('user', $uuid);
    if (!empty($user)) {
      $uid = $user->get('uid')->value;
    }

    if (!empty($uid)) {
      return $uid;
    }
    else {
      return FALSE;
    }
  }

  public function update_date_format($date) {
    $defaultTimezone = \Drupal::config('system.date');
    $updateDateFormat = DateTimePlus::createFromFormat('Y-m-d', $date, $defaultTimezone)
      ->format('m-d-Y');
    return $updateDateFormat;
  }

}
