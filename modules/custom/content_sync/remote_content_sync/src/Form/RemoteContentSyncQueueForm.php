<?php

namespace Drupal\remote_content_sync\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\remote_content_sync\EntityOperations;
use Drupal\remote_content_sync\RemoteEntityOperations;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;

/**
 * Sync content manually if queue has pending items.
 */
class RemoteContentSyncQueueForm extends ConfigFormBase {

  /**
   * Queue factory service.
   *
   * @var QueueFactory
   */
  protected $queueFactory;

  /**
   * Connection service.
   *
   * @var Connection
   */
  protected $connection;

  /**
   * Account proxy service.
   *
   * @var AccountProxy
   */
  protected $currentUser;

  /**
   * Remote entity operations service.
   *
   * @var RemoteEntityOperations
   */
  protected $remoteEntityOperations;

  /**
   * Entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Date formatter service.
   *
   * @var Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(QueueFactory $queue, Connection $connection, DateFormatter $date_formatter, AccountProxy $current_user, EntityTypeManagerInterface $manager, RemoteEntityOperations $remote_entity_operations) {
    $this->queueFactory = $queue;
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $manager;
    $this->remoteEntityOperations = $remote_entity_operations;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue'),
      $container->get('database'),
      $container->get('date.formatter'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('remote_content_sync.remote_entity_operations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'remote_content_sync_queue_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return 'remote_content_sync.queue_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var QueueInterface $queue */
    $form['queuetable'] = [
      '#type' => 'table',
      '#header' => [
        'Item id',
        'operation',
        'Local id',
        'UUID',
        'Remote Site ID',
        'Created',
        'Will Expire',
      ],
      '#empty' => $this->t('There are no items in the queue.'),
    ];

    $result = $this->connection->select('queue', 'q')
      ->fields('q', ['item_id', 'data', 'expire', 'created'])
      ->condition('q.name', 'remote_content_sync')
      ->execute();

    while ($queued_op = $result->fetchAssoc()) {
      $data_object = unserialize($queued_op['data']);
      $form['queuetable'][$queued_op['item_id']]['item_id'] = [
        '#plain_text' => $queued_op['item_id'],
      ];
      $form['queuetable'][$queued_op['item_id']]['operation'] = [
        '#plain_text' => $data_object->operation,
      ];
      $form['queuetable'][$queued_op['item_id']]['local_id'] = [
        '#plain_text' => $data_object->entity_id,
      ];
      $form['queuetable'][$queued_op['item_id']]['uuid'] = [
        '#plain_text' => $data_object->entity_uuid,
      ];
      $form['queuetable'][$queued_op['item_id']]['remote_site_id'] = [
        '#plain_text' => $data_object->remote_site_id,
      ];
      $form['queuetable'][$queued_op['item_id']]['created'] = [
        '#plain_text' => $this->dateFormatter->formatTimeDiffSince($queued_op['created']),
      ];
      $form['queuetable'][$queued_op['item_id']]['expire'] = [
        '#plain_text' => 'Expired',
      ];
      if ((int) $queued_op['expire'] !== 0) {
        $form['queuetable'][$queued_op['item_id']]['#attributes'] = ['class' => ['color-warning']];
        $form['queuetable'][$queued_op['item_id']]['expire'] = [
          '#plain_text' => $this->dateFormatter->formatTimeDiffUntil($queued_op['expire']) ,
        ];
      }
    }

    $queue = $this->queueFactory->get('remote_content_sync');

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>Submitting this form will process the Manual Queue which contains @number items.</p><p><strong>Important:</strong> This action will take some time.</p>', ['@number' => $queue->numberOfItems()]),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send pending content items'),
    ];

    // Only show these buttons for administrator roles.
    $current_user_roles = $this->currentUser->getRoles();
    if (in_array('administrator', $current_user_roles)) {
      $form['advanced'] = [
        '#type' => 'details',
        '#title' => $this->t('Advanced operations'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#description' => $this->t('Expire or delete ALL items in the queue.'),
      ];
      $form['advanced']['expirequeue'] = [
        '#type' => 'submit',
        '#value' => $this->t('Force expiration'),
        '#submit' => [[$this, 'expireQueue']],
      ];
      $form['advanced']['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete all'),
        '#submit' => [[$this, 'deleteQueue']],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $queue = $this->queueFactory->get('remote_content_sync');
    $batch = [
      'title' => $this->t('Synchronizing Content...'),
      'message' => $this->t('Synchronizing Content...'),
      'operations' => [],
      'finished' => [$this, 'queueFinished'],
    ];

    for ($i = 0; $i < $queue->numberOfItems(); $i++) {
      $batch['operations'][] = [
        [$this, 'processQueueItem'],
        [$queue],
      ];
    }

    batch_set($batch);
  }

  /**
   * Batch operations callback.
   *
   * @param QueueInterface $queue
   *   The queue to process.
   * @param array $context
   *   The batch context.
   */
  public function processQueueItem(QueueInterface $queue, &$context) {

    if (empty($context['sandbox'])) {
      // Claim Item.
      $item = $queue->claimItem();

      if (!$item) {
        return;
      }

      $entity_info = $item->data;
      if ($entity_info->operation === EntityOperations::OPERATION_DELETE) {
        // Load remote to sync with.
        $storage = $this->entityTypeManager->getStorage('remote_site');
        $remote_site = $storage->load($entity_info->remote_site_id);
        try {
          $this->remoteEntityOperations->deleteRemote($entity_info->entity_type_id, $entity_info->entity_uuid, $remote_site);
          $queue->deleteItem($item);
        }
        catch (RemoteContentSyncException $e) {
          $queue->releaseItem($item);
          watchdog_exception('remote_content_sync', $e);
          $error = TRUE;
        }
        catch (\Exception $e) {
          watchdog_exception('remote_content_sync', $e);
          $error = TRUE;
        } finally {
          $context['results'][] = TRUE;
          $context['message'] = $this->t('Deleted content @entity_type: @uuid from remote @remote.', [
            '@uuid' => $entity_info->entity_uuid,
            '@entity_type' => $entity_info->entity_type_id,
            '@remote' => $remote_site->label(),
          ]);
          if ($error) {
            $context['message'] = $this->t('Deletion failed on content @entity_type: @uuid from remote @remote.', [
              '@uuid' => $entity_info->entity_uuid,
              '@entity_type' => $entity_info->entity_type_id,
              '@remote' => $remote_site->label(),
            ]);
          }
          if ($error) {
            drupal_set_message($context['message'], 'error');
          }
        }
        return;
      }

      // Load Entity to sync.
      $storage = $this->entityTypeManager->getStorage($entity_info->entity_type_id);
      $entity = $storage->load($entity_info->entity_id);

      // Load remote to sync with.
      $storage = $this->entityTypeManager->getStorage('remote_site');
      $remote_site = $storage->load($entity_info->remote_site_id);

      $context['sandbox']['item'] = $item;
      $context['sandbox']['entity'] = $entity;
      $context['sandbox']['remote_site'] = $remote_site;
      $context['sandbox']['operation'] = $entity_info->operation;
      $context['sandbox']['entity_references'] = [];
      $context['sandbox']['entity_references'] = $this->remoteEntityOperations->getEntityReferences($entity);
      $context['sandbox']['max'] = count($context['sandbox']['entity_references']) + 1;
      $context['sandbox']['progress'] = 0;
    }

    $error = FALSE;
    try {
      if (!empty($context['sandbox']['entity_references'])) {
        $referenced_entity = array_pop($context['sandbox']['entity_references']);
        $entity_to_sync = $referenced_entity;
      }
      else {
        $context['results'][] = TRUE;
        $entity_to_sync = $context['sandbox']['entity'];
        $queue->deleteItem($context['sandbox']['item']);
      }
      $remote_site = $context['sandbox']['remote_site'];
      if (!empty($entity_to_sync)) {
        $this->remoteEntityOperations->updateRemoteEntity($entity_to_sync, $remote_site);
      }
    }
    catch (RemoteContentSyncException $e) {
      if ($entity_to_sync->uuid() === $entity->uuid()) {
        $queue = $this->queueFactory->get('remote_content_sync');
        $queue->createItem(
          (object) [
            'remote_site_id' => $remote_site->id(),
            'entity_id' => $entity_to_sync->id(),
            'entity_uuid' => $entity_to_sync->uuid(),
            'entity_type_id' => $entity_to_sync->getEntityTypeId(),
            'operation' => EntityOperations::OPERATION_UPDATE,
          ]
        );
        $error = TRUE;
        $queue->releaseItem($context['sandbox']['item']);
      }
      watchdog_exception('remote_content_sync', $e);
    }
    catch (\Exception $e) {
      watchdog_exception('remote_content_sync', $e);
    } finally {
      $context['sandbox']['progress']++;
      $context['message'] = $this->t('Synchronized content @label (@entity_type: @id) with remote @remote.', [
        '@label' => $entity_to_sync->label(),
        '@id' => $entity_to_sync->id(),
        '@entity_type' => $entity_to_sync->getEntityTypeId(),
        '@remote' => $remote_site->label(),
      ]);
      if ($error) {
        $context['message'] = $this->t('Synchronization with remote @remote failed on content @label (@entity_type: @id).', [
          '@label' => $entity_to_sync->label(),
          '@id' => $entity_to_sync->id(),
          '@entity_type' => $entity_to_sync->getEntityTypeId(),
          '@remote' => $remote_site->label(),
        ]);
      }
      if ($error) {
        drupal_set_message($context['message'], 'error');
      }
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
    }
  }

  /**
   * Callback for batch finished.
   */
  public function queueFinished($success, $results, $operations) {
    if ($success) {
      $message = $this->formatPlural(
        count($results),
        'One content Synchronized.', '@count contents Synchronized.'
      );
    }
    else {
      $message = $this->t('Finished with an error.');
    }
    drupal_set_message($message);

  }

  /**
   * Submit callback for forced expiration of queue items.
   */
  public function expireQueue() {
    $this->connection->update('queue')
      ->fields(['expire' => 0])
      ->condition('expire', 0, '>')
      ->condition('name', 'remote_content_sync')
      ->execute();
    drupal_set_message($this->t('All queue items have been expired!'));
  }

  /**
   * Submit callback for deletion of all queue items.
   */
  public function deleteQueue() {
    $this->connection->delete('queue')
      ->condition('name', 'remote_content_sync')
      ->execute();
    drupal_set_message($this->t('All queue items have been deleted!'));
  }

}
