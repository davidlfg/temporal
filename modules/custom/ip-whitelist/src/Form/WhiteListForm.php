<?php

/**
 * @file
 * Contains \Drupal\colorful_list\Form\WhiteListForm
 */

namespace Drupal\colorful_list\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 *  WhiteListForm Form.
 */
class WhiteListForm extends ConfigFormBase {

 /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'whitelist_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form   = parent::buildForm($form, $form_state);
    $config = $this->config('colorful_list.settings');

    $form['#id']              = 'whitelist-form';
    $form['whitelist_status'] = array(
      '#title'         => t('Active'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('whitelist_status') ?: '',
    );
    //field ips
    $form['whitelist_ips'] = array(
      '#type'          => 'textarea',
      '#title'         => $this->t('IP Addresses to whitelist'),
      '#description'   => $this->t('Enter one ip in format regular expressions per line.'),
      '#default_value' => $config->get('whitelist_ips') ?: '',
    );
    //field path
    $form['whitelist_paths']     = array(
      '#type'          => 'textarea',
      '#title'         => $this->t('URLs to whitelist'),
      '#description'   => $this->t('Specify pages by using their paths. Enter one path in format regular expressions per line.  Example: /\/admin[\/|$].*/, /\/user[\/|$].*/'),
      '#default_value' => $config->get('whitelist_paths') ?: '',
    );
    $form['whitelist_exception'] = array(
      '#title'         => t('HTTP Exception Type'),
      '#type'          => 'select',
      '#options'       => array(Response::HTTP_NOT_FOUND => sprintf('%s - %s', Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]),
                                Response::HTTP_FORBIDDEN => sprintf('%s - %s', Response::HTTP_FORBIDDEN, Response::$statusTexts[Response::HTTP_FORBIDDEN])),
      '#default_value' => $config->get('whitelist_exception') ?: '',
    );

    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->cleanValues()->getValues();
    $config = $this->config('colorful_list.settings');
    foreach ($values as $key => $value) {
      $config->set($key, $form_state->getValue($key))->save();
    }

    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['colorful_list.settings'];
  }
}
