<?php

namespace Drupal\user_action_item\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for User action item entity entities.
 */
class UserActionItemEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['user_action_item_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('User action item entity'),
      'help' => $this->t('The User action item entity ID.'),
    );

    return $data;
  }

}
