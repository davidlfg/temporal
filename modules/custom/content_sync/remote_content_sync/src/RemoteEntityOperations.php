<?php

namespace Drupal\remote_content_sync;

use Drupal\content_sync\ContentSyncManager;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\remote_content_sync\Entity\RemoteSiteInterface;
use Drupal\remote_content_sync\Exception\RemoteContentSyncException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Serializer\Serializer;

/**
 * Remote entity sync service class.
 */
class RemoteEntityOperations {

  const FORMAT = 'json';
  const ENTITY_EMPTY_CHECKSUM = 1;
  const ENTITY_NOT_FOUND = 0;

  protected $contentSyncManager;
  protected $httpClient;
  protected $entityTypeManager;
  protected $serializer;
  protected $supportedEntityTypes;

  /**
   * {@inheritdoc}
   */
  public function __construct(ContentSyncManager $content_sync_manager, Client $http_client, EntityTypeManagerInterface $entity_type_manager, Serializer $serializer) {
    $this->contentSyncManager = $content_sync_manager;
    $this->contentSyncManager->setFormat(self::FORMAT);
    $this->httpClient = $http_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->serializer = $serializer;
    $this->supportedEntityTypes = ['node', 'file', 'taxonomy_term'];
  }

  /**
   * Sends local copy of an updated node to remote provider.
   */
  public function updateRemote(ContentEntityInterface $entity, RemoteSiteInterface $remote_site) {
    $entity_references = $this->contentSyncManager->getEntityReferences($entity);
    try {
      foreach ($entity_references as $referenced_entity) {
        $this->updateRemoteEntity($referenced_entity, $remote_site);
      }
      $this->updateRemoteEntity($entity, $remote_site);
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * Get the entity refs contained in a given entity.
   */
  public function getEntityReferences(ContentEntityInterface $entity) {
    return $this->contentSyncManager->getEntityReferences($entity);
  }

  /**
   * Sends local copy of an updated entity to remote provider.
   */
  public function updateRemoteEntity(ContentEntityInterface $entity, RemoteSiteInterface $remote_site) {
    $context = [
      'expose_image' => TRUE,
      'field_map' => $remote_site->getFieldMap(),
    ];
    $encoded_entity = $this->contentSyncManager->exportEntity($entity, $context);
    try {
      $uri = (string) $remote_site->uri();
      $uri .= "content_sync/{$encoded_entity['entity_type_id']}";
      $format = $this->contentSyncManager->getFormat();
      if (!in_array($encoded_entity['entity_type_id'], $this->supportedEntityTypes)) {
        return;
      }
      $checksum = $this->remoteEntityChecksum($encoded_entity['original_entity'], $remote_site);
      if ($checksum === self::ENTITY_NOT_FOUND) {
        $this->httpClient->post($uri, [
          'json' => $this->serializer->decode($encoded_entity['entity'], $format),
          'query' => ['_format' => $format],
          'auth' => [$remote_site->username(), $remote_site->password()],
          'http_errors' => TRUE,
        ]);
      }
      elseif ($checksum === self::ENTITY_EMPTY_CHECKSUM || $checksum != $this->contentSyncManager->generateChecksum($entity)) {
        $uri .= "/{$encoded_entity['original_entity']->uuid()}";
        $this->httpClient->patch($uri, [
          'json' => $this->serializer->decode($encoded_entity['entity'], $format),
          'query' => ['_format' => $format],
          'auth' => [$remote_site->username(), $remote_site->password()],
          'http_errors' => TRUE,
        ]);
      }
    }
    catch (ConnectException $e) {
      drupal_set_message(t('There was a problem establishing a connection to the remote site during the insert/update operation on @entity_uuid.', ['@entity_uuid' => $entity->uuid()]), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
    catch (ClientException $e) {
      if ($e->getCode() !== 400) {
        drupal_set_message(t('There was a problem executing the insert/update operation on @entity_uuid: Remote site returned status code @code', [
          '@entity_uuid' => $entity->uuid(),
          '@code' => $e->getCode()
        ]), 'warning');
      }
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
    catch (RequestException $e) {
      drupal_set_message(t('There was a problem with the request executing the insert/update operation on @entity_uuid. Response code @code', ['@entity_uuid' => $entity->uuid(), '@code' => $e->getResponse()->getStatusCode()]), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
    catch (\Exception $e) {
      drupal_set_message(t('There was a problem executing the insert/update operation on @entity_uuid', ['@entity_uuid' => $entity->uuid()]), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
  }

  /**
   * Deletes entity from remote.
   */
  public function deleteRemote($entity_type_id, $entity_uuid, RemoteSiteInterface $remote_site) {
    try {
      $uri = (string) $remote_site->uri();
      $uri .= "content_sync/{$entity_type_id}";
      $format = $this->contentSyncManager->getFormat();
      if (!in_array($entity_type_id, $this->supportedEntityTypes)) {
        return;
      }
      if ($this->remoteEntityExistsByIds($entity_type_id, $entity_uuid, $remote_site)) {
        $uri .= "/{$entity_uuid}";
        $this->httpClient->delete($uri, [
          'query' => ['_format' => $format],
          'auth' => [$remote_site->username(), $remote_site->password()],
          'http_errors' => TRUE,
        ]);
      }
    }
    catch (ConnectException $e) {
      drupal_set_message(t('There was a problem establishing a connection to the remote site during the delete operation on @entity_uuid.', ['@entity_uuid' => $entity_uuid]), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
    catch (ClientException $e) {
      drupal_set_message(t('There was a problem executing the delete operation on @entity_uuid: Remote site returned status code @code', ['@entity_uuid' => $entity_uuid, '@code' => $e->getCode()]), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
    catch (RequestException $e) {
      drupal_set_message(t('There was a problem with the request during the delete operation on @entity_uuid. Response code @code', ['@entity_uuid' => $entity_uuid, '@code' => $e->getResponse()->getStatusCode()]), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
    catch (\Exception $e) {
      drupal_set_message(t('There was a problem executing the delete operation on @entity_uuid', ['@entity_uuid' => $entity_uuid]), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw new RemoteContentSyncException();
    }
  }

  /**
   * Checks if remote entity exists.
   */
  public function remoteEntityExists(ContentEntityInterface $entity, RemoteSiteInterface $remote_site) {
    return $this->remoteEntityExistsByIds($entity->getEntityTypeId(), $entity->uuid(), $remote_site);
  }

  /**
   * Gets the Remote Entity Checksum.
   */
  public function remoteEntityChecksum(ContentEntityInterface $entity, RemoteSiteInterface $remote_site) {
    return $this->remoteEntityChecksumByIds($entity->getEntityTypeId(), $entity->uuid(), $remote_site);
  }

  /**
   * Checks if remote entity exists by id.
   */
  protected function remoteEntityExistsByIds($entity_type_id, $entity_uuid, RemoteSiteInterface $remote_site) {
    $uri = (string) $remote_site->uri();
    $uri .= "content_sync/{$entity_type_id}/{$entity_uuid}";
    try {
      $response = $this->httpClient->head($uri, [
        'query' => ['_format' => $this->contentSyncManager->getFormat()],
        'auth' => [$remote_site->username(), $remote_site->password()],
      ]);
      return $response->getStatusCode() === 200;
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return FALSE;
      }
      watchdog_exception('remote_entity_sync', $e);
      throw $e;
    }
    catch (\Exception $e) {
      drupal_set_message(t('There was a problem executing the operation'), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw $e;
    }
  }

  /**
   * Gets the Remote Entity Checksum.
   */
  protected function remoteEntityChecksumByIds($entity_type_id, $entity_uuid, RemoteSiteInterface $remote_site) {
    $uri = (string) $remote_site->uri();
    $uri .= "content_sync/{$entity_type_id}/{$entity_uuid}";
    try {
      $response = $this->httpClient->head($uri, [
        'query' => ['_format' => $this->contentSyncManager->getFormat()],
        'auth' => [$remote_site->username(), $remote_site->password()],
      ]);
      if ($response->getStatusCode() === 200) {
        if ($response->getHeader('Content-MD5')) {
          $md5 = $response->getHeader('Content-MD5');
          $md5 = reset($md5);
          return base64_decode($md5);
        }
        else {
          return self::ENTITY_EMPTY_CHECKSUM;
        }
      }
      else {
        return self::ENTITY_NOT_FOUND;
      }
    }
    catch (RequestException $e) {
      if ($e->getCode() === 404) {
        return self::ENTITY_NOT_FOUND;
      }
      watchdog_exception('remote_entity_sync', $e);
      throw $e;
    }
    catch (\Exception $e) {
      drupal_set_message(t('There was a problem executing the operation'), 'warning');
      watchdog_exception('remote_entity_sync', $e);
      throw $e;
    }
  }

}
