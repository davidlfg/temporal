<?php

namespace Drupal\deploy_hpb;

use Drupal\multiversion\Entity\WorkspaceInterface;
use Drupal\replication\ChangesFactory as ChangesFactoryBase;
use Symfony\Component\HttpFoundation\ParameterBag;

class ChangesFactory extends ChangesFactoryBase
{
    /**
     * @inheritDoc
     */
    public function get(WorkspaceInterface $workspace)
    {

        if (!isset($this->instances[$workspace->id()])) {
            parent::get($workspace);
            $view_id = $this->getViewId($workspace);
            if (!empty($view_id)) {
                $this->instances[$workspace->id()]->filter('views');
                $parameters = new ParameterBag(['view_id' => $view_id]);
                $this->instances[$workspace->id()]->parameters($parameters);
            }
        }
        return $this->instances[$workspace->id()];
    }

    /**
     * Returns the view ID to the given workspace
     * @param WorkspaceInterface $workspace
     * @return string
     */
    protected function getViewId(WorkspaceInterface $workspace)
    {
        return $workspace->field_view_id->target_id;
    }

}