<?php

namespace Drupal\oauth_service_connector\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class OAuthUserLoginForm.
 *
 * @package Drupal\oauth_service_connector\Form
 */
class OAuthUserLoginForm extends FormBase {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs a new OAuthUserLoginForm.
   *
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   */
  public function __construct(UserStorageInterface $user_storage) {
    $this->userStorage = $user_storage;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oauth_user_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mail'] = [
      '#type' => 'email',
      '#placeholder' => $this->t('E-mail'),
      '#required' => TRUE,
      '#attributes' => array(
        'autocorrect' => 'none',
        'autocapitalize' => 'none',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
      ),
    ];
    $form['pass'] = [
      '#type' => 'password',
      '#maxlength' => 64,
      '#size' => 64,
      '#placeholder' => $this->t('Password'),
      '#required' => TRUE,
    ];

    $form['links']['#markup'] = t('<div class="item-list form-item-forgetpassword-link"><a href=":url">Forgot your password ?</a></div>', array(
      ':url' => '/user/password'
    ));

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Log In'),
    ];
    $form['#validate'][] = '::validateAuthentication';
    return $form;
  }

  /**
   *  Send login detail to micro service and check for valid user
   *  Check local user exist, if not create a local user and create a session.
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uuid = $form_state->get('uuid');

    $account = \Drupal::entityManager()->loadEntityByUuid('user', $uuid);
    if (!$account) {
      //Local User does not Exist.Create a user with same UUID.
      $account = \Drupal::service('oauth_service.connect')
        ->oauthCreateLocalUser($form_state);
    }

    user_login_finalize($account);
    $form_state->setRedirect('<front>');
  }

  /**
   * Validate.
   *  If a valid user in remote website, check local user exist.
   *  If local user does not exit create a local user.
   */
  public function validateAuthentication(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) { // If there are errors in the form, don't proceed
      return;
    }
    $response = \Drupal::service('oauth_service.connect')
      ->oauthUserLogin($form_state);

    if (isset($response[0]->errorMessage)) {
      $form_state->setErrorByName('mail', $this->t($response[0]->errorMessage));
    }
    else {
      $form_state->set('uuid', $response['uuid']);
    }

  }

}
