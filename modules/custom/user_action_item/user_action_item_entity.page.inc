<?php

/**
 * @file
 * Contains user_action_item_entity.page.inc.
 *
 * Page callback for User action item entity entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for User action item entity templates.
 *
 * Default template: user_action_item_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_user_action_item_entity(array &$variables) {
  // Fetch UserActionItemEntity Entity Object.
  $user_action_item_entity = $variables['elements']['#user_action_item_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
