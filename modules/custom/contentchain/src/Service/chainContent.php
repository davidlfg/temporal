<?php
/**
 *  This file will pull content chain content for a given chain Id
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 *  This file contains trade secrets of Johnson & Johnson, Inc.
 *  No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 *  permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 *
 */

namespace Drupal\contentchain\Service;


use Drupal\views\Views;
use Drupal\contentchain\Service\ListSubChainIds;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Datetime\DateTimePlus;

/**
 * Class chainContent
 * @package Drupal\contentchain\Service
 */
class chainContent {

  /**
   * Function to return required or not required chain count
   * @param $before_surgery
   * @param $jointReplacementType
   * @return array
   */
  public function get_chainCount($before_surgery, $jointReplacementType) {
    $view = views_embed_view('chain_count_before_after_surgery', 'rest_export_1', $jointReplacementType, $before_surgery);
    $viewResult = \Drupal::service('renderer')->render($view);
    $chainJson = (string) $viewResult;
    $chainArray = json_decode($chainJson);

    $items['totalchainCount'] = count($chainArray);
    $items['totalRequired'] = 0;
    $items['totalNotRequired'] = 0;
    $i = 1;
    $j = 1;
    if (count($chainArray) > 0) {
      foreach ($chainArray as $key => $values) {
        if ($values->field_required) {
          $items['totalRequired'] = $i;
          $i++;
        }
        else {
          $items['totalNotRequired'] = $j;
          $j++;
        }
      }
    }
    return $items;
  }

  /**
   * Function to retrieve Starred Content .
   * starred content from previous weeks is moved into subsequent weeks
   * @param $items
   * @param $user
   * @param $week_diff
   * @param $before_surgery
   * @return mixed
   */
  function getPreviousWeeksContent($items, $user, $week_diff, $before_surgery, $jointReplacementType) {
    //Calculating registration week
    $registrationDate = date('Y-m-d', $user->created->value);
    $date = DateTimePlus::createFromFormat('m-d-Y', $user->field_surgery_date->value);
    $field_surgery_date = $date->format('Y-m-d');
    $registrationWeek = self::weekDifference($registrationDate, $field_surgery_date);
    $currentWeek = $week_diff;

    //Get completed chainId pre-surgery
    $chainId = \Drupal::database()->select('chain_progression', 'cp')
      ->fields('cp', array('chain_id'))
      ->condition('cp.uid', $user->id())
      ->condition('cp.progress_percent', '000000000001')
      ->execute();
    $completedNodeArray = array();
    $completedNode = '';
    foreach ($chainId as $row) {
      $completedNodeArray[] = $row->chain_id;
    }
    if (count($completedNodeArray) > 0) {
      $completedNode = implode('+', $completedNodeArray);
    }


    //pre-Surgery Starred Content Chains carried over to post-surgery week.
    $config = \Drupal::configFactory()->getEditable('user_management.settings');
    $preSurgeryNode = $config->get('preSurgeryNode');
    if ($preSurgeryNode) {
      $preSurgeryNodeArray = explode('+', $preSurgeryNode);
      $preSurgeryNodeArray = array_diff($preSurgeryNodeArray, $completedNodeArray);
      $preSurgeryNode = implode('+', $preSurgeryNodeArray);
    }
    else {
      $preSurgeryNode = '';
    }

    //End

    if ($before_surgery) { //Pre-Surgery
      //View @arg :Week/week/before or after surgery/ required/ skip node id
      if ($completedNode) {
        $view1 = views_embed_view('starred_content_chains', 'rest_export_2', 1, 1, $registrationWeek, $currentWeek, $jointReplacementType, $completedNode);
      }
      else {
        $view1 = views_embed_view('starred_content_chains', 'rest_export_2', 1, 1, $registrationWeek, $currentWeek, $jointReplacementType);
      }
      //Add required chains from pre-surgery
      $preSurgeryChain = array();
    }
    else {
      //PostSurgery
      if ($completedNode) {
        $view1 = views_embed_view('starred_content_chains_postsurgery', 'rest_export_2', 1, 0, $currentWeek, $jointReplacementType, $completedNode);
      }
      else {
        $view1 = views_embed_view('starred_content_chains_postsurgery', 'rest_export_2', 1, 0, $currentWeek, $jointReplacementType);
      }
      //Add required chains from pre-surgery
      $preSurgeryChain = array();
      if ($preSurgeryNode) {
        $view = views_embed_view('content_chain', 'pre_surgery_starred', $preSurgeryNode, $jointReplacementType);
        $view_result = \Drupal::service('renderer')->render($view);
        $chain_json = (string) $view_result;
        $preSurgeryChain = json_decode($chain_json);
      }
    }

    $order = isset($items['activities']) ? count($items['activities']) : 0;

    $view_result1 = \Drupal::service('renderer')->render($view1);
    $chain_json1 = (string) $view_result1;
    $chain_array1 = json_decode($chain_json1);
    $newArray = array_merge($chain_array1, $preSurgeryChain);

    foreach ($newArray as $values) {
      $values->field_weight = $order;
      $order++;
    }
    if (count($newArray) > 0) {
      return $items = self::getChainContent($items, $newArray, $user->id(), count($items['activities']));
    }
    else {
      return $items;
    }
  }

  /**
   * Function to create content chain array
   * @param $items
   * @param $chain_array
   * @param $user
   * @param int $keys
   * @return mixed
   */
  public function getChainContent($items, $chain_array, $user, $keys = 0, $before_surgery = 0, $week_diff = 1) {
    if (isset($user['uid'])) {
      $uid = $user['uid'];
    }
    else {
      $headers = getallheaders();
      $uuid = $headers['uuid'];
      $headlessAccount = \Drupal::entityManager()
        ->loadEntityByUuid('user', $uuid);
      $uid = $headlessAccount->id();
    }
    $items['listSubChainId'] = array();
    foreach ($chain_array as $key => $values) {
      //Start: Whole Content Chain Include /Exclude
      $tid = isset($values->field_whole_chain_rules) ? $values->field_whole_chain_rules : '';
      $condition = isset($values->field_chain_include_exclude) ? $values->field_chain_include_exclude : '';
      if ($condition != '' and $tid != '') {
        $res = self::wholeChainIncludeExclude($condition, $tid, $user, $before_surgery, $week_diff);
        //\Drupal::logger('Sub Chain')->notice(print_r($res, TRUE));
        $ruleCount = $res['ruleCount'];
        $includeCount = $res['includeCount'];
        $excludeCount = $res['excludeCount'];

        if ($condition == 'Include This Content Chain') {
          //if condition not match then call 'continue' to make the content chain to exclude
          if ($includeCount > 0) {
            continue;
          }
        }
        else {
          if ($condition == 'Exclude This Content Chain') {
            //if condition  match then call 'continue' to make the content chain to exclude
            if ($ruleCount == $excludeCount) {
              continue;
            }
          }
        }
      }
      //End: Whole Content Chain Include /Exclude
      $items['activities'][$keys]['chainId'] = (int) $values->nid;
      $items['activities'][$keys]['order'] = (int) $values->field_weight;
      $items['activities'][$keys]['progress'] = 0;
      $items['activities'][$keys]['lastSeenContentId'] = 0;
      $items['activities'][$keys]['beforeSurgery'] = (int) $values->field_before_surgery_date;
      if ($values->field_required == 1) {
        $required = TRUE;
      }
      else {
        $required = FALSE;
      }
      $items['activities'][$keys]['required'] = $required;
      $progress = \Drupal::database()->select('chain_progression', 'cp')
        ->fields('cp', array('progress_percent', 'link_id'))
        ->condition('cp.uid', $uid)
        ->condition('cp.chain_id', $values->nid)
        ->execute();

      foreach ($progress as $row) {
        $progress_percent = isset($row->progress_percent) ? $row->progress_percent : 0;
        $link_id = isset($row->link_id) ? $row->link_id : FALSE;
        $items['activities'][$keys]['progress'] = (double) $progress_percent;
        $items['activities'][$keys]['lastSeenContentId'] = (int) $link_id;
      }
      $items['activities'][$keys]['title'] = htmlspecialchars_decode($values->field_display_title, ENT_QUOTES);
      $items['activities'][$keys]['week'] = (int) $values->field_week;
      $items['activities'][$keys]['JointReplacementType'] = isset($values->field_joint_replacement_type) ? $values->field_joint_replacement_type : '';
      //Get all sub-chain ID
      $listSubChainId = new ListSubChainIds();
      if ($listSubChainId->getSubChainId($values->nid)) {
        $firstSubChain = $listSubChainId->getSubChainId($values->nid);
        //\Drupal::logger('firstSubChain')->notice(print_r($firstSubChain, TRUE));
        $subChain = array();
        $listSubChain = self::subChainRecursive($firstSubChain, $subChain);
        //\Drupal::logger('Sub Chain')->notice(print_r($listSubChain, TRUE));
        //$items['activities'][$keys]['listSubChainIds1'] = $listSubChain;
        if (count($listSubChain) > 0) {
          $items['listSubChainId'][] = array_unique(array_merge($listSubChain, $firstSubChain), SORT_REGULAR);;
        }
        else {
          $items['listSubChainId'][] = $firstSubChain;
        }
      }
      //End
      $keys++;
    }

    //Re-arrange sub chain array
    $listSubChain1 = array();
    foreach ($items['listSubChainId'] as $key => $value) {
      $listSubChain1 = array_unique(array_merge($listSubChain1, $value), SORT_REGULAR);
    }
    $items['listSubChainId'] = $listSubChain1;
    //end

    return $items;
  }

  /**
   * Recursive function to get subChain id
   * @param $firstSubChain
   * @param $subChain
   * @return array
   */
  function subChainRecursive($firstSubChain, $subChain) {
    $listSubChainId = new ListSubChainIds();
    foreach ($firstSubChain as $key => $chainId) { //2nd
      $list = $listSubChainId->getSubChainId($chainId);
      if (count($list) > 0) {
        $subChain1 = $list;
        $subChain2 = self::subChainRecursive($list, $list);
        if (count($subChain2) > 0) {
          $subChain = array_merge($subChain1, $subChain2);
        }
        else {
          $subChain = $subChain1;
        }
      }
    }
    return $subChain;
  }

  /**
   * Update content chain array , add selected chain items
   * @param $selectedChain
   * @param $items
   * @param $counter
   * @return mixed
   */
  public
  function getDecisionTreeLinks($selectedChain, $items, $counter) {
    global $base_url;
    $headers = getallheaders();
    $contentChainUrl = $base_url . '/content/' . $selectedChain;
    try {
      $response = \Drupal::httpClient()->get($contentChainUrl,
        array(
          'headers' => array(
            'uuid' => $headers['uuid'],
            'internal' => 'Yes',
            'elementsCount' => $counter
          )
        ));
      //unset selected content chain id
      unset($items['selectedChain']);
      $data = json_decode($response->getBody()->getContents(), TRUE);
      //unset decision tree chain items
      unset($data['Title']);
      unset($data['chainId']);
      unset($data['backgroundImage']);
      unset($data['selectedChain']);
      $j = $counter;
      foreach ($data['contentElements'] as $key => $value) {
        $items['contentElements'][$j] = $value;
        $j++;
      }
      return $items;

    } catch (ClientException $e) {
      if ($e->getResponse()->getStatusCode() == 400) {
        \Drupal::logger('getDecisionTreeLinks')
          ->notice('Error: ' .
            Json::decode($e->getResponse()->getBody()->getContents()));
      }
      else {
        \Drupal::logger('getDecisionTreeLinks')
          ->error('An error occurred"' .
            $selectedChain . '"');
      }
    }
  }

  /**
   * Function to calculate week difference
   * @param $dateTime1
   * @param $dateTime2
   * @return float
   */
  public
  function weekDifference($dateTime1, $dateTime2) {
    $datetime1 = date_create($dateTime1);
    $datetime2 = date_create($dateTime2);
    $interval = date_diff($datetime1, $datetime2);
    $day_diff = $interval->format('%a');
    $weekDiff = ceil($day_diff / 7);
    return $weekDiff;
  }

  /**
   * @param $a
   * @param $operator
   * @param $b
   * @return bool
   * @throws \Drupal\contentchain\Service\Exception
   */
  private
  function doComparison($a, $operator, $b) {
    switch ($operator) {
      case '<':
        if ($a < $b) {
          return TRUE;
        }
        else {
          return FALSE;
        }
        break;
      case '<=':
        if ($a <= $b) {
          return TRUE;
        }
        else {
          return FALSE;
        }
        break;

      case '=':
        if ($a == $b) {
          return TRUE;
        }
        else {
          return FALSE;
        }
        break;

      case '==':
        if ($a == $b) {
          return TRUE;
        }
        else {
          return FALSE;
        }
        break;

      case '!=':
        if ($a != $b) {
          return TRUE;
        }
        else {
          return FALSE;
        }
        break;

      case '>=':
        if ($a >= $b) {
          return TRUE;
        }
        else {
          return FALSE;
        }
        break;

      case '>':
        if ($a > $b) {
          return TRUE;
        }
        else {
          return FALSE;
        }
        break;
    }

    throw new Exception("The {$operator} operator does not exists", 1);
  }

  /**
   * @param $condition
   * @param $tid
   * @param $user
   * @return array
   * @throws \Drupal\contentchain\Service\Exception
   */
  private function wholeChainIncludeExclude($condition, $tid, $user, $before_surgery, $week_diff) {
    //Whole Chain Include /Exclude
    $userAge = isset($user['field_age']) ? (int) $user['field_age'] : 0;
    $userGender = isset($user['field_gender']) ? $user['field_gender'] : '';
    $userRace = isset($user['field_race']) ? $user['field_race'] : '';

    if ($condition != '' and $tid != '') {
      $taxonomy = \Drupal\taxonomy\Entity\Term::load($tid);
      $ruleAge = isset($taxonomy->field_age->value) ? (int) $taxonomy->field_age->value : 0;

      $ruleAgeOperator = isset($taxonomy->field_age_operator->value) ? $taxonomy->field_age_operator->value : 0;
      $ruleGender = isset($taxonomy->field_gender->value) ? $taxonomy->field_gender->value : '';
      $ruleRace = isset($taxonomy->field_race->value) ? $taxonomy->field_race->value : '';

      $ruleBeforeAfterSurgery = isset($taxonomy->field_before_or_after_surgery->value) ? (int) $taxonomy->field_before_or_after_surgery->value : 0;
      $ruleSurgeryWeekOperator = isset($taxonomy->field_surgery_week_operator->value) ? $taxonomy->field_surgery_week_operator->value : 0;
      $ruleWeek = isset($taxonomy->field_week->value) ? $taxonomy->field_week->value : 0;

      $include = 0;
      $exclude = 0;
      $rule = 0;
      //AGE
      if ($ruleAge) {
        $rule++;
        $check = self::doComparison($userAge, $ruleAgeOperator, $ruleAge);
        if ($check == FALSE) {
          $include++;
        }
        else {
          $exclude++;
        }
      }
      //GENDER
      if ($ruleGender) {
        $rule++;
        $check = self::doComparison($userGender, '==', $ruleGender);
        if ($check == FALSE) {
          $include++;
        }
        else {
          $exclude++;
        }
      }
      //RACE
      if ($ruleRace) {
        $rule++;
        $check = self::doComparison($userRace, '==', $ruleRace);
        if ($check == FALSE) {
          $include++;
        }
        else {
          $exclude++;
        }
      }

      //Surgery Week
      if ($ruleWeek) {
        $rule++;
        if ($before_surgery == $ruleBeforeAfterSurgery and $week_diff == $ruleSurgeryWeekOperator) {
          $include++;
        }
        else {
          $exclude++;
        }
      }
      return array(
        'ruleCount' => $rule,
        'includeCount' => $include,
        'excludeCount' => $exclude,
      );
    }
  }

}
