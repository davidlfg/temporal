<?php
/**
 * mailchimpSubscriptionController file
 *
 *
 * @copyright 2016 Johnson & Johnson, Inc
 * This file contains trade secrets of Johnson & Johnson, Inc.
 * No part may be reproduced or transmitted in any form by any means or for any purpose without the express written
 * permission of Johnson & Johnson, Inc..
 * @since File available since Release 1.0.0
 */

namespace Drupal\contentchain\Controller;

use Drupal\contentchain\Service\ChainWebForms;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;
use Drupal\Component\Datetime\DateTimePlus;


class mailchimpSubscriptionController {
  /*
   * Function to Subscribe/Update/Remove caretaker email
   *
   */
  public function mailChimpSubscription(Request $request) {
    $headers = getallheaders();
    $user = \Drupal::service('user_management.user')
      ->validate_user_id($headers);
    if (isset($user['Error'])) {
      return new JsonResponse(array('errorMessage' => $user['Error']), 400);
    }
    $uuid = $headers['uuid'];
    $headlessAccount = \Drupal::entityManager()
      ->loadEntityByUuid('user', $uuid);
    $userId = $headlessAccount->id();

    $content = json_decode($request->getContent(), TRUE);
    $firstName = $content['caretakerFirstName'];
    $lastName = $content['caretakerLastName'];
    $newCaretakerEmail = $content['caretakerEmail'];
    $jointReplacementType = $user['field_joint_replacement_type'];
    $subscribe = new ChainWebForms();
    if ($userId) { //Existing user User
      $date = DateTimePlus::createFromFormat('m-d-Y', $user['field_surgery_date']);
      $referenceDate = $date->format('m/d/Y');
      $oldCaretakerEmail = $user['field_caretaker_email'];
      if (isset($oldCaretakerEmail)) {//Update
        if ($oldCaretakerEmail != $newCaretakerEmail) {
          //TODO ;1st check new email is already used or not
          $res = $subscribe->CareTakerSubscription(array(
            'caretakerEmail' => $newCaretakerEmail,
            'jointReplacementType' => $jointReplacementType
          ), 'Status');
          if ($res['status'] != 404) {
            //Email is already in use by other user
            $response = array(
              'ErrorMessage' => t('Caretaker Email is already used by other user.'),
            );
            return new JsonResponse($response, 400);
          }
          //unSubscribe old caretaker
          $oldCareTaker = array(
            'userId' => $userId,
            'caretakerFirstName' => $user['field_caretaker_first_name'],
            'caretakerLastName' => $user['field_caretaker_last_name'],
            'caretakerEmail' => $user['field_caretaker_email'],
            'jointReplacementType' => $user['field_joint_replacement_type']
          );
          $result = $subscribe->CareTakerSubscription($oldCareTaker, 'Status');
          if ($result['status'] != 404) {
            $subscribe->CareTakerSubscription($oldCareTaker, 'Remove');
          }
        }
      }
      else {
        //New
        //TODO ;1st check new email is already used or not
        $res = self::CareTakerSubscription(array(
          'caretakerEmail' => $newCaretakerEmail,
          'jointReplacementType' => $jointReplacementType
        ), 'Status');
        if ($res['status'] != 404) {
          //Email is already in use by other user
          $response = array(
            'ErrorMessage' => t('Caretaker Email is already used by other user.'),
          );
          return new JsonResponse($response, 400);
        }
      }

      $userProfile = array(
        'userId' => $userId,
        'caretakerFirstName' => $firstName,
        'caretakerLastName' => $lastName,
        'caretakerEmail' => $newCaretakerEmail,
        'ReferenceDate' => $referenceDate,
        'jointReplacementType' => $jointReplacementType
      );
      //check subscription status and subscribe to the list
      $response = $subscribe->mailChimpApiCall($userProfile);
      if (isset($response['ErrorMessage'])) {
        return new JsonResponse(array('errorMessage' => $response['ErrorMessage']), 400);
      }
      return new JsonResponse(array('Message' => $response['Message']), $response['code']);
    }
    else {
      //Error , userId not fount in the header
      $message = array(
        'errorMessage' => t('Error.'),
      );
      return new JsonResponse($message, 400);
    }
  }
}