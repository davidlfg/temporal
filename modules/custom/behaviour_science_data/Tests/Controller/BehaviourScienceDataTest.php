<?php

namespace Drupal\behaviour_science_data\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the behaviour_science_data module.
 */
class BehaviourScienceDataTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "behaviour_science_data BehaviourScienceData's controller functionality",
      'description' => 'Test Unit for module behaviour_science_data and controller BehaviourScienceData.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests behaviour_science_data functionality.
   */
  public function testBehaviourScienceData() {
    // Check that the basic functions of module behaviour_science_data.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
