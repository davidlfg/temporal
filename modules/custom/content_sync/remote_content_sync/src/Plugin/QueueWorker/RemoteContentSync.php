<?php

namespace Drupal\remote_content_sync\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\remote_content_sync\EntityOperations;
use Drupal\remote_content_sync\Exception\RemoteContentSyncException;
use Drupal\remote_content_sync\RemoteEntityOperations;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "remote_content_sync",
 *   title = @Translation("Remote entity sync"),
 *   cron = {"time" = 60}
 * )
 */
class RemoteContentSync extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  protected $exportedEntities = [];

  protected $entityTypeManager;

  protected $remoteEntityOperations;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RemoteEntityOperations $remote_entity_operations) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->remoteEntityOperations = $remote_entity_operations;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('remote_content_sync.remote_entity_operations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $item_token = serialize([
      $data->operation,
      $data->remote_site_id,
      $data->entity_type_id,
      $data->entity_id,
    ]);

    if (!in_array($item_token, $this->exportedEntities)) {
      $remote_site = $this->entityTypeManager
        ->getStorage('remote_site')
        ->load($data->remote_site_id);
      if (!$remote_site->isAutoSyncEnabled()) {
        throw new SuspendQueueException();
      }
      try {
        switch ($data->operation) {
          case EntityOperations::OPERATION_DELETE:
            $this->remoteEntityOperations->deleteRemote($data->entity_type_id, $data->entity_uuid, $remote_site);
            break;

          case EntityOperations::OPERATION_INSERT:
          case EntityOperations::OPERATION_UPDATE:
            $entity = $this->entityTypeManager
              ->getStorage($data->entity_type_id)
              ->load($data->entity_id);
            if (!empty($entity)) {
              $this->remoteEntityOperations->updateRemote($entity, $remote_site);
            }
            break;

          default:
            break;
        }
      }
      catch (RemoteContentSyncException $e) {
        throw new SuspendQueueException();
      }
      catch (\Exception $e) {
        watchdog_exception('remote_content_sync', $e);
      }

      $this->exportedEntities[] = $item_token;
      drupal_set_message(t('Processed @operation operation on @entity_uuid', ['@operation' => $data->operation, '@entity_uuid' => $data->entity_uuid]));
    }
  }

}
